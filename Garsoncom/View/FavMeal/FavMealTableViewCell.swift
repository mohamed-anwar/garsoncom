//
//  FavMealTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/28/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class FavMealTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        minusButton.layer.cornerRadius = 15
        
    }
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var mealNameLabel: UILabel!
    @IBOutlet weak var restImageView: UIImageView!
    @IBOutlet weak var minusButton: UIButton!
    
    func ConfigureCell(image : String , name : String , category :String , sizes : [SizeModel] ) {
        if sizes.count != 0 {
           priceLabel.text = sizes[0]._offer_price + "" + EGP
            
        }else{
            priceLabel.text =  "" + EGP
            
        }
        restImageView.kf.indicatorType = .activity
        restImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        mealNameLabel.text = name
        categoryLabel.text = category
        
    }
    
}
