//
//  EditMeal.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher
class EditMeal: UIView {

    @IBOutlet weak var foodimageView: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var co1Label: UILabel!
    @IBOutlet weak var co2Label: UILabel!
    @IBOutlet weak var co3Label: UILabel!
    @IBOutlet weak var co4Label: UILabel!
    @IBOutlet weak var co1Segmented: UISegmentedControl!
    @IBOutlet weak var co2Segmented: UISegmentedControl!
    @IBOutlet weak var co3Segmented: UISegmentedControl!
    @IBOutlet weak var co4Segmented: UISegmentedControl!
    @IBOutlet weak var co4Stack: UIStackView!
    @IBOutlet weak var co3Stack: UIStackView!
    @IBOutlet weak var co2Stack: UIStackView!
    @IBOutlet weak var co1Stack: UIStackView!
   
    func getSegmentValue(cov : String) -> Int {
        switch cov {
        case "ex":
            return 0
        case "m":
            return 1
        default:
            return 2
        }
    }
    func getCov(selected : Int) -> String {
        switch selected {
        case 0:
            return "ex"
        case 1:
            return "m"
        default:
            return "f"
        }
    }
    func configureView(cov1 : String , co1 : String ,cov2 : String  , co2 : String , cov3 :String  , co3 : String , cov4 : String  , co4 : String , image : String) {
        foodimageView.kf.indicatorType = .activity
        foodimageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        
        if co1 != ""{
            co1Label.text = co1
            co1Segmented.selectedSegmentIndex = getSegmentValue(cov : cov1)
            co1Segmented.setImage(#imageLiteral(resourceName: "selectedHumperger"), forSegmentAt: getSegmentValue(cov : cov1))

        }else{
            co1Stack.removeFromSuperview()
        }
        if co2 != ""{
            co2Label.text = co2
            co2Segmented.selectedSegmentIndex = getSegmentValue(cov : cov2)
            co2Segmented.setImage(#imageLiteral(resourceName: "selectedHumperger"), forSegmentAt: getSegmentValue(cov : cov2))

        }else{
            co2Stack.removeFromSuperview()
        }
        if co3 != ""{
            co3Label.text = co3
            co3Segmented.selectedSegmentIndex = getSegmentValue(cov : cov3)
            co3Segmented.setImage(#imageLiteral(resourceName: "selectedHumperger"), forSegmentAt: getSegmentValue(cov : cov3))

        }else{
            co3Stack.removeFromSuperview()
        }
        if co4 != ""{
            co4Label.text = co4
            co4Segmented.selectedSegmentIndex = getSegmentValue(cov : cov4)
            co4Segmented.setImage(#imageLiteral(resourceName: "selectedHumperger"), forSegmentAt: getSegmentValue(cov : cov4))


        }else{
            co4Stack.removeFromSuperview()
        }
    }
    
}
