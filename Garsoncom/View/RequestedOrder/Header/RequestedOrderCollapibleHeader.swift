//
//  RequestedOrderCollapibleHeader.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/14/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SRCountdownTimer
import Kingfisher

class RequestedOrderCollapibleHeader: UITableViewHeaderFooterView,UIGestureRecognizerDelegate {
    var delegate: CollapsibleTableViewHeaderDelegate?
    var section: Int = 0

    @IBOutlet weak var restImage: UIImageView!
    @IBOutlet weak var counterView: SRCountdownTimer!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var deliveryCoustLabel: UILabel!
    @IBOutlet weak var TotalCoustLabel: UILabel!
    @IBOutlet weak var cancelOrderButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupGestureRecognizer()
    }
    @IBOutlet weak var restNameLabel: UILabel!
    @IBAction func CancelOrderButton(){
    
        delegate?.toggleCancel(section: section)
    }
    
    func configure(id : String , deliveryCoust : String , totalCoust : String, status : String , date : String , remaing : Int , image :String , restName : String , orderNo : String) {
        if status != "new"{
            createdAtLabel.isHidden = false
            counterView.isHidden = false
            cancelOrderButton.isHidden = true
        }else{
            cancelOrderButton.isHidden = false
            createdAtLabel.isHidden = true
            counterView.isHidden = true


        }
        restNameLabel.text = restName
        counterView.useMinutesAndSecondsRepresentation = true
        counterView.start(beginingValue: remaing)
        createdAtLabel.text = NSLocalizedString("Order Num", comment: "" + orderNo)
//        idLabel.text = "#"  + id
        deliveryCoustLabel.text = NSLocalizedString("Delivery Cost : ", comment: "") + deliveryCoust
        TotalCoustLabel.text = NSLocalizedString("Total Cost : ", comment: "") + totalCoust
        cancelOrderButton.fullyRounded()
//        titleLabel.text = sectionName
//        arrowLabel.text = ">"
        
        restImage.kf.indicatorType = .activity
        restImage.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
    
    }
    
    func setupGestureRecognizer() {
        let tapR = UITapGestureRecognizer(target: self, action: #selector(tapHeader(_:)))
        tapR.delegate = self
        tapR.numberOfTapsRequired = 1
        tapR.numberOfTouchesRequired = 1
        addGestureRecognizer(tapR)
    }
    
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        
        guard let cell = gestureRecognizer.view as? RequestedOrderCollapibleHeader else {
            return
        }
        
        delegate?.toggleSection(self, section: cell.section)
    }
    
    func setCollapsed(_ collapsed: Bool) {
//        arrowLabel.rotate(collapsed ? 0.0 : .pi / 2)
    }
    

    
}
