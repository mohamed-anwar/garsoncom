//
//  collapsibleTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class collapsibleTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemCostLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(image : String ,name : String , cost : String) {
        itemNameLabel.text = name
        itemCostLabel.text = NSLocalizedString("Item Cost : ", comment: "") + cost
        itemImageView.fullyRounded()
        itemImageView.kf.indicatorType = .activity
        itemImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        
    }

    
}
