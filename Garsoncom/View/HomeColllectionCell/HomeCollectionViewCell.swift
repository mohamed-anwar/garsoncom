//
//  HomeCollectionViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/11/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class HomeCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 10
    }

    @IBOutlet weak var restNameLabel: UILabel!
    @IBOutlet weak var restImageView: UIImageView!
    
    func SetCell(name : String ,image : String)  {
        restNameLabel.text = name
        restImageView.kf.indicatorType = .activity
        restImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
        })
    }
    
}
