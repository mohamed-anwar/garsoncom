//
//  ProductHomeCollectionViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/12/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos
import FaveButton

class ProductHomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var favButton: FaveButton!
    @IBOutlet weak var productImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 10
    }
    func configure(product : ProductModel){
        rateView.rating = Double(product._rate)
        rateView.text =  "(" + String(product._rate) + ")"
        productNameLabel.text =  product._name
        favButton.isSelected = product._isFavourite
        favButton.imageView?.image = product._isFavourite ? #imageLiteral(resourceName: "heart") : #imageLiteral(resourceName: "heartWhite")
        descLabel.text = product._description
        productPriceLabel.text = (product._sizes.first?._price ?? "0.00") + EGP
        productImageView.kf.indicatorType = .activity
        productImageView.kf.setImage(with: URL(string: photoUrl + "/"  + product._image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
        })
        
        
    }

}
