//
//  MealDetails.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/21/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class MealDetails: UIView {
 
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var foodImageView: UIImageView!
    @IBOutlet weak var addRateButton: UIButton!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    
    var viewIsSetup = false
    var counter = 1
    var price  :Float = 0.0
    @IBAction func plusButton(_ sender: UIButton) {
        counter += 1
        countLabel.text = "\(counter)"
        totalLabel.text = "\(Float(counter) * price)" + EGP


    }
    @IBAction func  minusButton(_ sender: UIButton) {
        if counter > 1 {
            counter -= 1
        }
        countLabel.text = "\(counter)"
        totalLabel.text = "\(Float(counter) * price)" + EGP

    }
    func configure(image : String , desc : String)  {
        descLabel.text = desc
        
        foodImageView.kf.indicatorType = .activity
        foodImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                   placeholder:nil,
                                   options: [.transition(ImageTransition.fade(1))],
                                   progressBlock: { receivedSize, totalSize in
        },
                                   completionHandler: { image, error, cacheType, imageURL in
                                    
        })
    }
    func configure(Totlat Price : Float){
        price = Price
        totalLabel.text = "\(Float(counter) * Price)" + EGP
    }
    
    
    override func layoutSubviews() {
        if !viewIsSetup {
            viewIsSetup = true
            plusButton.fullyRounded()
            minusButton.fullyRounded()
            makeBorderToView(descLabel, boderWidth: 1, cornerRadius: 10, borderColor: .lightGray, maskToBounds: true)
            makeBorderToView(containerView, boderWidth: 1, cornerRadius: 10, borderColor: .lightGray, maskToBounds: true)
            
            

        }
    }
    
}
