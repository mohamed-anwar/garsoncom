//
//  SelectSizeTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/21/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit

class SelectSizeTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    func configureCell (size : String , discount : String , price : String , offer  : Int){
        if offer != 0 {
            discountLabel.text = discount + EGP
            priceLabel.addStrikedthrough(text: price )

        }else{
            priceLabel.text = price + EGP

        }
        sizeLabel.text = size
        
    }
    func configureCell(selected : Bool) {
        if selected{
            selectedImageView.image = #imageLiteral(resourceName: "dot-inside-a-circle")
        }else{
            selectedImageView.image = #imageLiteral(resourceName: "unselected")
        }
    }
    
}
