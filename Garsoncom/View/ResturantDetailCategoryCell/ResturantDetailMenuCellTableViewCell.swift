//
//  ResturantDetailCellTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/12/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class ResturantDetailMenuCellTableViewCell: UITableViewCell {

    @IBOutlet weak var FoodImageView: UIImageView!
    @IBOutlet weak var FoodNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func SetCell(name : String , image : String) {
        FoodNameLabel.text = name
        FoodImageView.kf.indicatorType = .activity
        FoodImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
        })
        
    }
    
}
