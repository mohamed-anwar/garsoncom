//
//  ResturantMenuTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Cosmos
import FaveButton
import Kingfisher

class ResturantMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var RestimageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var RateView: CosmosView!
    @IBOutlet weak var PriceLabe: UILabel!
    @IBOutlet weak var favBut: FaveButton!
    var isViewSetup = false
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if !isViewSetup {
            RateView.isUserInteractionEnabled = false
            mainView.layer.cornerRadius = 15
            makeBorderToView(RestimageView, boderWidth: 1, cornerRadius: 15, borderColor: .clear, maskToBounds: true)
            makeShadowToView(mainView, shadowOpacity: 0.5, shadowRadius: 1, shadowColor: .gray)
        }
        isViewSetup = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(name : String , desc : String , rate : Int ,sizes : [SizeModel] , fav : Bool , image : String , isLogin : Bool ) {
        favBut.isHidden = !isLogin
        RestimageView.kf.indicatorType = .activity
        RestimageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in

        })
        if sizes.count != 0 {
            PriceLabe.text = sizes[0]._offer_price + " " + EGP

        }else{
            PriceLabe.text =  "" + EGP

        }
        nameLabel.text = name
        descLabel.text = desc
        RateView.rating = Double(rate)
        RateView.text = "(" + String(rate) + ")"
        favBut.isSelected = fav
        
    }
    
}
