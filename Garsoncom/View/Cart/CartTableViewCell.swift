//
//  CartTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class CartTableViewCell: UITableViewCell {
    @IBOutlet weak var foodImageView: UIImageView!
    @IBOutlet weak var foodNameLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    
    @IBOutlet weak var counterLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    var counter = 0
    var price  :Float = 0.0
    @IBAction func plusButton(_ sender: UIButton) {
        counter += 1
        counterLabel.text = "\(counter)"
        totalLabel.text = "\(Float(counter) * price)" + EGP
        
        
    }
    @IBAction func  minusButton(_ sender: UIButton) {
        if counter > 1 {
            counter -= 1
        }
        counterLabel.text = "\(counter)"
        totalLabel.text = "\(Float(counter) * price)" + EGP
    }
    func configure(image : String  , name : String , price : String , Counter : Int)  {
        foodNameLabel.text = name
        priceLabel.text = price
        counter = Counter
        counterLabel.text = "\(counter)"
        configure(Totlat: Float(price)!)

        foodImageView.kf.indicatorType = .activity
        foodImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
    }
    func configure(Totlat Price : Float!){
        price = Price
        totalLabel.text = "\(Float(counter) * price)" + EGP
        counterLabel.text = "\(counter)"
    }

    
}
