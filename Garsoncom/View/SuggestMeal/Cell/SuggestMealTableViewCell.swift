//
//  SuggestMealTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/1/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class SuggestMealTableViewCell: UITableViewCell {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var mealNameLabel: UILabel!
    @IBOutlet weak var mealImageView: UIImageView!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var addComments: UIButton!
    @IBOutlet weak var voteButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var deletePostButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        deletePostButton.fullyRounded()
        makeBorderToView(containerView, boderWidth: 1, cornerRadius: 10, borderColor: .clear, maskToBounds: true)
        makeShadowToView(containerView, shadowOpacity: 1, shadowRadius: 2, shadowColor: .lightGray)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(post : Post){
        deletePostButton.isHidden = post._is_my_post ? false : true
        userImageView.kf.indicatorType = .activity
        userImageView.kf.setImage(with: URL(string: photoUrl + "/"  + post._client._image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        userImageView.fullyRounded()
        nameLabel.text = post._client._name
        hoursLabel.text = post._time
        mealNameLabel.text = post._title
        mealImageView.kf.indicatorType = .activity
        mealImageView.kf.setImage(with: URL(string: photoUrl + "/" + post._image), placeholder: nil, options: [.transition(ImageTransition.fade(1))], progressBlock: { receivedSize, totalSize in
        }, completionHandler:  {image, error, cacheType, imageURL in
        
    })
        postLabel.text = post._description
        addComments.setTitle(NSLocalizedString("Comments (\(post._comments_count))", comment: ""), for: .normal)
        voteButton.setTitle(NSLocalizedString("Votes (\(post._votes_count))", comment: ""), for: .normal)
        voteButton.setImage(post._is_vote ? #imageLiteral(resourceName: "starFilled") : #imageLiteral(resourceName: "star"), for: .normal)

    }
    
    
    
}
