//
//  SuggestMealHeader.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/30/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftMessages
class SuggestMealHeader: UITableViewHeaderFooterView {
    weak var delegate : AddPostDelegate?
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameLabel.text = userName
        userImageView.fullyRounded()
        userImageView.kf.indicatorType = .activity
        userImageView.kf.setImage(with: URL(string: photoUrl + "/"  + userImage!) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        
        
    }



    @IBOutlet weak var captureImageButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mealNameTextF: UITextField!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var postTextF: UITextField!
    @IBAction func addPost(_ sender : UIButton){
        if (mealNameTextF.text?.count)! > 0{
            if (postTextF.text?.count)! > 0 {
                
                delegate?.didAddPost(mealName: mealNameTextF.text!, post: postTextF.text!)
                
            }else{
                // add post msg
                SwiftMessages.showMessage(title: "", body: NSLocalizedString("plese add post", comment: ""), type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

            }
        }else{
           // add mealName msg
            SwiftMessages.showMessage(title: "", body: NSLocalizedString("plese add meal name", comment: ""), type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        }
        
    }
    
    
}


