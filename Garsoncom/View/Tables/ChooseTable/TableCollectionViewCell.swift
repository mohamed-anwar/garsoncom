//
//  TableCollectionViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/2/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher
import AMPopTip
let greenColor = UIColor(red: 83/255, green: 181/255, blue: 103/255, alpha: 1)
class TableCollectionViewCell: UICollectionViewCell {
    let popTip = PopTip()
    override func awakeFromNib() {
        super.awakeFromNib()
        InfoButton.layer.cornerRadius = 11

    }
    @IBOutlet weak var InfoButton: UIButton!

    @IBOutlet weak var tableImageView: UIImageView!
    @IBOutlet weak var TableNumLabel: UILabel!
    @IBOutlet weak var GuestsLabel: UILabel!
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        popTip.text = ""
    }
    
    
    func Configre(image : String, tableNum : Int , gusetsAllowes : Int , disc : String , selected : Bool) {
        if selected{
            backgroundColor = greenColor
        }else{
             backgroundColor = .white
        }
        popTip.text = disc
        popTip.bubbleColor = greenColor
        TableNumLabel.text = TableNumString + "\(tableNum)"
        GuestsLabel.text =   GuestsAllowedString +  "\(gusetsAllowes)"
        tableImageView.kf.indicatorType = .activity
        tableImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
        })
    }
    

}
