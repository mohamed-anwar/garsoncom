//
//  TableTimePickerView.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/2/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit

class TableTimePickerView: UIView {

    @IBOutlet weak var ChooseDateButton: UIButton!
    
    @IBOutlet weak var FromTimePicker: UIDatePicker!
    @IBOutlet weak var ToTimePicker: UIDatePicker!
    
    @IBOutlet weak var SubmitButton: UIButton!
    
}
