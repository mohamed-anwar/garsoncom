//
//  DefualtTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/13/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


import UIKit

class DefualtTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var defualtImageView : UIImageView!
    func Configure (image : UIImage){
        defualtImageView.image = image
    }
    
}
