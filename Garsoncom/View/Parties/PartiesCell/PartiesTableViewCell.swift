//
//  PartiesTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/4/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher
class PartiesTableViewCell: UITableViewCell {

    @IBOutlet weak var foodTypeLabel: UILabel!
    @IBOutlet weak var BranshNumberLabel: UILabel!
    @IBOutlet weak var restImageView: UIImageView!
    @IBOutlet weak var restNameLabel: UILabel!
    
    @IBOutlet weak var selectedImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func configureCell(image : String , name :String , branshNum : String , type : String , selected : Bool){
        restNameLabel.text = name
        BranshNumberLabel.text = NSLocalizedString("Num of Branshs : ", comment: "") + branshNum
        foodTypeLabel.text = NSLocalizedString("Food Type : ", comment: "") + type
        selectedImageView.image = (selected ?  #imageLiteral(resourceName: "checkBox") : #imageLiteral(resourceName: "uncheckbox"))
        restImageView.kf.indicatorType = .activity
        restImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
    
        
    }
    
}
