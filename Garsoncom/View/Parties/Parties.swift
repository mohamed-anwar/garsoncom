//
//  Parties.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/4/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import PMSuperButton
import SkyFloatingLabelTextField
import Kingfisher
import DropDown

class Parties: UIView {
    let setPartyType = DropDown()
    weak var delegete : ChoosePartyDelegate!
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.setPartyType
        ]
    }()
    @IBOutlet weak var chooseDate: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var chooseTypeButton: PMSuperButton!
    @IBOutlet weak var outsideRestButton: UIButton!
    @IBOutlet weak var intsideRestButton: UIButton!
    @IBOutlet weak var markAllCategoriesButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var markAllRestaurantsButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendNotificationButton: UIButton!
    @IBOutlet weak var contentTextF: SkyFloatingLabelTextField!
    @IBOutlet weak var addressTextF: SkyFloatingLabelTextField!
    @IBOutlet weak var numberOfGuestsTextF: SkyFloatingLabelTextField!
    override func awakeFromNib() {
        userNameLabel.text = userName!
    makeBorderToView(userImageView, boderWidth: 1, cornerRadius: userImageView.frame.size.width / 2, borderColor: .gray, maskToBounds: true)
        makeBorderToView(chooseTypeButton, boderWidth: 1, cornerRadius: 15, borderColor: .gray, maskToBounds: true)
        makeBorderToView(chooseDate, boderWidth: 1, cornerRadius: 15, borderColor: .gray, maskToBounds: true)

        userImageView.kf.indicatorType = .activity
        userImageView.kf.setImage(with: URL(string: photoUrl + "/"  + userImage!) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
    }
    @IBAction func ChoosePartyType(_ sender : UIButton){
        setPartyType.show()
     
    }
    
    
    
    
    
    
    
}
extension Parties{
    func setupSetCityDropDown() {
        setPartyType.anchorView =  chooseTypeButton
        setPartyType.bottomOffset = CGPoint(x: 0, y:  chooseTypeButton.bounds.height)
        setPartyType.selectionAction = { [unowned self] (index, item) in
            self.chooseTypeButton.setTitle(item, for: .normal)
            self.delegete.didTapPartyType(Sender: index)
        }
    }
}
extension Parties : UITextFieldDelegate{
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
            switch(textField) {
            case contentTextF :
                if  text.count == 0{
                    contentTextF.errorMessage = NSLocalizedString("Invalid Content", comment: "")
                } else if text.count != 0 {
                    contentTextF.errorMessage = ""
                }
                break
                
            case addressTextF :
                if text.count == 0 {
                    addressTextF.errorMessage =  NSLocalizedString("Invalid Address", comment: "")
                } else if text.count != 0 {
                    addressTextF.errorMessage = ""
                }
                break
            case numberOfGuestsTextF :
                if !text.isNumber{
                    numberOfGuestsTextF.errorMessage = NSLocalizedString("Invalid Number", comment: "")
                } else if text.count != 0{
                    numberOfGuestsTextF.errorMessage = ""
                }
                break

                
            default :
                break
            }
            return true
        }
    
    
    
}
