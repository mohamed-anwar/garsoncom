//
//  CommentTableViewHeader.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class CommentTableViewHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var mealNameLabel: UILabel!
    @IBOutlet weak var mealImageView: UIImageView!
    @IBOutlet weak var postLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
 
    func configure(post : Post){
        userImageView.kf.indicatorType = .activity
        userImageView.kf.setImage(with: URL(string: photoUrl + "/"  + post._client._image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        userImageView.fullyRounded()
        nameLabel.text = post._client._name
        hoursLabel.text = post._time
        mealNameLabel.text = post._title
        mealImageView.kf.indicatorType = .activity
        mealImageView.kf.setImage(with: URL(string: photoUrl + "/" + post._image), placeholder: nil, options: [.transition(ImageTransition.fade(1))], progressBlock: { receivedSize, totalSize in
        }, completionHandler:  {image, error, cacheType, imageURL in
            
        })
        postLabel.text = post._description

        
    }

 
    
}
