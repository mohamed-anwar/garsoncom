//
//  CommentTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class CommentTableViewCell: UITableViewCell {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var deleteCommentButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        deleteCommentButton.fullyRounded()
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(comment : CommentModel){
        deleteCommentButton.isHidden = comment._is_my_comment ? false : true
        userImageView.kf.indicatorType = .activity
        userImageView.kf.setImage(with: URL(string: photoUrl + "/"  + comment._user._image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        userImageView.fullyRounded()
        nameLabel.text = comment._user._name
        timeLabel.text = comment._time
        commentLabel.text = comment._comment
     

        
    }
    
}
