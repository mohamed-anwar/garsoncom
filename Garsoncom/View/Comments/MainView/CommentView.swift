//
//  CommentView.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit

class CommentView: UIView {

    @IBOutlet weak var commentTextF : UITextField!
    @IBOutlet weak var sendButton : UIButton!
    @IBOutlet weak var tableView : UITableView!
    

}
extension CommentView : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        switch textField {
        case commentTextF :
            if text.count == 0{
                sendButton.isEnabled = false
            }else{
                sendButton.isEnabled = true
            }
            
            default:
            break
        }
        return true
    }
}
