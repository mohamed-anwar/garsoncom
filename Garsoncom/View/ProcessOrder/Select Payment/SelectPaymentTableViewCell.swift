//
//  SelectPaymentTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/26/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit

class SelectPaymentTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var paymentTypeLabel: UILabel!
    func configureCell (PaymentType : String  ){
        paymentTypeLabel.text = PaymentType
        
    }
    func configureCell(selected : Bool) {
        if selected{
            selectedImageView.image = #imageLiteral(resourceName: "dot-inside-a-circle")
        }else{
            selectedImageView.image = #imageLiteral(resourceName: "unselected")
        }
    }

    
    
}
