//
//  ProcessOrder.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/25/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ProcessOrder: UIView {
    
    @IBOutlet weak var restNameLabel: UILabel!
    @IBOutlet weak var deliveryInLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var addressTxtF: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneTextF: SkyFloatingLabelTextField!
    @IBOutlet weak var NameTextF: SkyFloatingLabelTextField!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var deliveryFeeLabel: UILabel!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    
    @IBOutlet weak var addPromoCodeButton: UIButton!
    @IBOutlet weak var PlaceOrderButton: UIButton!
    @IBOutlet var circileView: [UIView]!
    
    
    
    
    override func awakeFromNib() {
        circileView.forEach({
            makeBorderToView($0, boderWidth: 1, cornerRadius: 12.5, borderColor: .clear, maskToBounds: true)
        })
        NameTextF.text = userName
        phoneTextF.text = userPhone
        addressTxtF.text = userAddress
    }
    func configureView(deliveryTime : String , restName : String , subTotal : String , deliveryMin : String , deliveryMax : String){
        subtotalLabel.text = subTotal
        deliveryFeeLabel.text = deliveryMin + " - " + deliveryMax
        
        
      
        deliveryInLabel.text = NSLocalizedString("Delivery in ", comment: "") + "\(deliveryTime)" +  NSLocalizedString("m", comment: "")
        
    }

}
extension ProcessOrder : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        switch(textField) {
        case addressTxtF :
            if !Validator.name(text) && text.count != 0 {
                addressTxtF.errorMessage = "Invalid Address"
            }else if Validator.name(text) || text.count == 0{
                addressTxtF.errorMessage = ""
            }
        case phoneTextF :
            if !Validator.validatePhoneNumber(text: text) && text.count != 0 {
                phoneTextF.errorMessage = "Invalid Phone Number"
            }else if Validator.validatePhoneNumber(text: text) || text.count == 0  || text.isNumber{
                phoneTextF.errorMessage = ""
            }
        case NameTextF :
            if !Validator.name(text) && text.count != 0{
                NameTextF.errorMessage = "Invalid name"
            } else if Validator.name(text) || text.count == 0 {
                NameTextF.errorMessage = ""
            }
            break
    
            
        default :
            break
        }
        return true
    }
    
}
