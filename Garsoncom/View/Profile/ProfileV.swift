//
//  ProfileV.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/11/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileV: UIView {


    @IBOutlet weak var ProfileImageView: UIImageView!
    
    
    @IBOutlet weak var tabelView: UITableView!
    
    func Configure(image : String)  {
        
        ProfileImageView.kf.indicatorType = .activity
        ProfileImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:#imageLiteral(resourceName: "profile-1"),
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        
        
    }
    
}
