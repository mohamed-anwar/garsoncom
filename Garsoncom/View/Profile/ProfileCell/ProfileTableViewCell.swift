//
//  ProfileTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/15/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    func configureCell(name : String , icon : UIImage)  {
        nameLabel.text = name
        iconImageView.image = icon
    }
    
    
}
