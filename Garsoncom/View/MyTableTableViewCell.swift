//
//  MyTableTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/29/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher
import AMPopTip

class MyTableTableViewCell: UITableViewCell {
    let popTip = PopTip()

    override func awakeFromNib() {
        super.awakeFromNib()

    }
    var descTip = ""
    override func prepareForReuse() {
        super.prepareForReuse()
        popTip.text = ""
    }
    

    @IBAction func ShowTip(){
        popTip.bubbleColor = greenColor

        popTip.show(text: descTip, direction: .down, maxWidth: 200, in: self, from: infoButton.frame, duration: 3)
        
    }
    @IBOutlet weak var canselButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var tableimageView: UIImageView!
    @IBOutlet weak var numberOfGustLabel: UILabel!
    @IBOutlet weak var tableNumLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    
    func configureCell(image : String , numberOfGust : String , tableNum : String , from : String , to : String , desc  : String , status : String)  {
        canselButton.isHidden = status == "accepted" ? true : false
        infoButton.layer.cornerRadius = 15
        canselButton.layer.cornerRadius = 15
        infoButton.clipsToBounds = true
        canselButton.clipsToBounds = true
        toLabel.text = to
        fromLabel.text = from
        numberOfGustLabel.text = numberOfGust
        tableNumLabel.text = tableNum
        tableimageView.kf.indicatorType = .activity
        tableimageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        descTip = desc

        
        
    }
}
