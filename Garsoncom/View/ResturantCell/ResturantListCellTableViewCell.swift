//
//  ResturantListCellTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/11/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import FaveButton
import Cosmos
import Kingfisher


class ResturantListCellTableViewCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var viewLayer: UIView!
    @IBOutlet weak var RateButton: CosmosView!
    @IBOutlet weak var heartButton: FaveButton!
    @IBOutlet weak var RestDeliveryImageView: UIImageView!
    @IBOutlet weak var RestDeliveryLabel: UILabel!
    @IBOutlet weak var RestTabelImageView: UIImageView!
    @IBOutlet weak var RestDescLabel: UILabel!
    @IBOutlet weak var RestNameLabe: UILabel!
    @IBOutlet weak var RestImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func setCellState(rest : ResturantModel){
        if rest._active == false || rest._isOrder == false || rest._haveDelivery == false{
            viewLayer.isHidden = false
            statusLabel.text = currentlyUnavilable
        }else if rest._isOpen == false || rest._isOnline == false{
            viewLayer.isHidden = false
            statusLabel.text = closedNow
        }else{
            viewLayer.isHidden = true
            
        }
    }
    
    func SetCell(rest : ResturantModel){
//        if (restaurant.getIsActive() == 0 || restaurant.getIs_order() == 0||restaurant.getHaveDelivery()==0) {
//            holder.imgClosed.setVisibility(View.VISIBLE);
//            tvRestStatus.setText(R.string.currently_unavilable);
//
//        } else if (restaurant.getIsOpen() == 0||restaurant.getIs_online()==0) {
//            holder.imgClosed.setVisibility(View.VISIBLE);
//            tvRestStatus.setText(R.string.closed_now);
//        }

        

        if userLoginToken != nil{
            heartButton.isHidden = false
            heartButton.isSelected = rest._isFavourite
        }else{
            heartButton.isHidden = true
        }
        if rest._isDeleviry == true{
            RestDeliveryImageView.isHidden = false
            RestDeliveryLabel.isHidden = false
            RestDeliveryLabel.text = NSLocalizedString("Delivery in ", comment: "") + "\(rest._deliveryTime)" +  NSLocalizedString("m", comment: "")
        }else{
            RestDeliveryImageView.isHidden = true
            RestDeliveryLabel.isHidden = true
        }
        if rest._haveTables == true{
            RestTabelImageView.isHidden = false
            RestTabelImageView.fullyRounded()
        }else{
            RestTabelImageView.isHidden = true

        }
        RestImageView.kf.indicatorType = .activity
        RestImageView.kf.setImage(with: URL(string: photoUrl + "/"  + rest._image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        RateButton.rating = Double(rest._rate)
        RateButton.text = "(\(rest._rate))"
        RestNameLabe.text = rest._name
        RestDescLabel.text = rest._categoriesHuman
        
        
    }
}

extension ResturantListCellTableViewCell : FaveButtonDelegate{
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
    
    }

    
    
    
}
