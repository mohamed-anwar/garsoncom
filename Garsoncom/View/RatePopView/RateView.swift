//
//  RateView.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/14/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Cosmos
import SwiftMessages
class RateView: UIViewController {
    var id : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        makeBorderToView(sendRateButton, boderWidth: 1, cornerRadius: 15, borderColor: .gray, maskToBounds: true)
        makeBorderToView(notNowButton, boderWidth: 1, cornerRadius: 15, borderColor: .gray, maskToBounds: true)
        backEnd = RateProductBackEnd()
        backEnd?.delegate = self
    }
    var backEnd : RateProductBackEnd?
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var rateView: CosmosView!
    
    @IBOutlet weak var sendRateButton: UIButton!
    @IBOutlet weak var notNowButton: UIButton!
    
    @IBAction func sendRateButton(_ sender: UIButton) {
        backEnd?.rateRequest(parameters: ["api_token" : userLoginToken! , "id" : id! , "type" : "product" , "rate" : Int(rateView.rating)])
        SwiftLoading().showLoading()
        
    }
    @IBAction func notNowButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
extension RateView : RateProductProtocol{
    func RateProduct(didFaild error: Error?) {
         SwiftLoading().hideLoading()
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

    }
    
    func RateProduct(didGetUnexpectedResponse: String) {
        SwiftLoading().hideLoading()

        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

    }
    
    func RateProduct(Success msg: String) {
        SwiftLoading().hideLoading()
        dismiss(animated: true, completion: nil)
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

    }
    
    
}
