//
//  RateProductBackEnd.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/14/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
class  RateProductBackEnd {
    
    private var network : Networking!
    weak var delegate  : RateProductProtocol?
    
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    
    
    func rateRequest(parameters : [String:Any]) {
        network.manager.request( RateUrl,method : .post ,parameters: parameters,headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].string! == StatusMsgs.sucess {
                    strongSelf.delegate?.RateProduct(Success: json["message"].string!)
                } else {
                    strongSelf.delegate?.RateProduct(didGetUnexpectedResponse: json["message"].string!)
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.RateProduct(didFaild: error as NSError)
                break
            }
        }
    }
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
