//
//  CategoryCollectionViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/11/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var CategoryNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var isViewSetup = false

    override func layoutSubviews() {
        super.layoutSubviews()
        
        if !isViewSetup {
            layer.cornerRadius = 15
        }
        isViewSetup = true
    }
 func SetCell(name : String){
    CategoryNameLabel.text = name
    }
    func SetSelectedCell(sender : Bool) {
        print(sender)
        if sender{
            CategoryNameLabel.backgroundColor = greenColor
        }else{
            CategoryNameLabel.backgroundColor = .gray
        }
    }

}
