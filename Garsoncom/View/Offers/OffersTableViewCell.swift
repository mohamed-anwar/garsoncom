//
//  OffersTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/17/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class OffersTableViewCell: UITableViewCell {

  
    @IBOutlet weak var offerPriceLabel: UILabel!
    @IBOutlet weak var offerNumber: UILabel!
    @IBOutlet weak var offerImageView : UIImageView!
    @IBOutlet weak var restNameLabel : UILabel!
    @IBOutlet weak var foodNameLabel : UILabel!
    override func layoutSubviews() {
        offerImageView.layer.cornerRadius = 45
        offerImageView.clipsToBounds = true

    }
    
    func configureCell(image : String , restName : String , foodName :  String , offer : String ,offerPrice : String) {

        restNameLabel.text = restName
        foodNameLabel.text = foodName
        offerImageView.kf.indicatorType = .activity
        offerImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                   placeholder:nil,
                                   options: [.transition(ImageTransition.fade(1))],
                                   progressBlock: { receivedSize, totalSize in
        },
                                   completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        offerNumber.text = offer + "%"
        
    }
    
}
