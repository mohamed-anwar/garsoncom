//
//  FavResturantCollectionViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/28/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher
class FavResturantCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    var isViewSetup = false
    override func layoutSubviews() {
        super.layoutSubviews()
        if !isViewSetup {
                minusButton.layer.cornerRadius = 15
            isViewSetup = true
        }
        
    }
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var restrantNameLabel: UILabel!
    @IBOutlet weak var restImageView: UIImageView!
    @IBOutlet weak var minusButton: UIButton!
    
    func ConfigureCell(image : String , name : String , category :String) {
        restImageView.kf.indicatorType = .activity
        restImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                   placeholder:nil,
                                   options: [.transition(ImageTransition.fade(1))],
                                   progressBlock: { receivedSize, totalSize in
        },
                                   completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        restrantNameLabel.text = name
        categoryLabel.text = category
        
    }
    
}
