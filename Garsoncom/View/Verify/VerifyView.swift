//
//  VerifyView.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/9/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class VerifyView: UIView {
    @IBOutlet weak var verifyTextF : SkyFloatingLabelTextField!
    @IBOutlet weak var sendCodeButton : UIButton!
    @IBOutlet weak var resendCodeButton : UIButton!

}
extension VerifyView : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        switch(textField) {
        case verifyTextF :
            if !Validator.validateVerificationCode(text: text) && text.count != 0{
                verifyTextF.errorMessage = "Invalid Verification Code"
            }else if Validator.validateVerificationCode(text: text) || text.count == 0 || text.isNumber{
                verifyTextF.errorMessage = ""
            }
            
        default :
            break
        }
        return true
    }
}
