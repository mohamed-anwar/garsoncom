//
//  ResturantHomeCollectionViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/11/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import FaveButton
import Cosmos
import Kingfisher

class ResturantHomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var deliveryTimeLabel: UILabel!
    @IBOutlet weak var restNameLabel: UILabel!
    @IBOutlet weak var favButton: FaveButton!
    @IBOutlet weak var restImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 10
    }
    func configure(rest : ResturantModel){
        rateLabel.text = String(rest._rate)
        rateView.rating = Double(rest._rate)
        rateView.text =  "(" + String(rest._rateCount) + ")"
        restNameLabel.text =  rest._name
        favButton.isSelected = rest._isFavourite

        favButton.imageView?.image = rest._isFavourite ? #imageLiteral(resourceName: "heart") : #imageLiteral(resourceName: "heartWhite")
        descLabel.text = rest._categoriesHuman
        deliveryTimeLabel.text = NSLocalizedString("Delivery in ", comment: "") + "\(rest._deliveryTime)" +  NSLocalizedString("m", comment: "")
        restImageView.kf.indicatorType = .activity
        restImageView.kf.setImage(with: URL(string: photoUrl + "/"  + rest._image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
        })
        
        
    }

}
