//
//  BestClientTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/16/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class BestClientTableViewCell: UITableViewCell {

  
    override func layoutSubviews() {
        clientImage.layer.cornerRadius = 40
           clientImage.clipsToBounds = true
    }
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var numberOfOrdersLabel : UILabel!
    @IBOutlet weak var clientImage : UIImageView!
    func ConFigure(name : String , numberOfOrders  : String , image : String)  {
        nameLabel.text = name
        numberOfOrdersLabel.text = numberOfOrders
        clientImage.kf.indicatorType = .activity
        clientImage.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:#imageLiteral(resourceName: "logo"),
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        
        
    }


    
}
