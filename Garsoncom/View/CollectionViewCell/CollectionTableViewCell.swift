//
//  CollectionTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/11/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit

class CollectionTableViewCell: UITableViewCell  {

    @IBOutlet weak var collectionView: UICollectionView!
    var data : Any!
    var isRatesCollectionView = false
    var isProductCategoryCollectionView = false
    var collectionViewitemSize : CGSize?
    var scrollDirection : UICollectionViewScrollDirection?
    var isViewSetup = false
    var dataManger : HomeBackEndManger!
    
    weak var delegate : TapDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        dataManger = HomeBackEndManger()

        collectionView.dataSource = self
        collectionView.delegate = self

        let categoryNib = UINib(nibName: "CategoryCollectionViewCell", bundle: nil)
        collectionView.register(categoryNib, forCellWithReuseIdentifier: categoryCellId)
        var homeNib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
        collectionView.register(homeNib, forCellWithReuseIdentifier: HomeCollectionId)
        homeNib = UINib(nibName: "ResturantHomeCollectionViewCell", bundle: nil)
        collectionView.register(homeNib, forCellWithReuseIdentifier: "ResturantHomeCollectionViewCellID")
        homeNib = UINib(nibName: "ProductHomeCollectionViewCell", bundle: nil)
        collectionView.register(homeNib, forCellWithReuseIdentifier: "ProductHomeCollectionViewCellID")
        
        
        

        
       
        
        
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        scrollDirection = nil
    }

    override func layoutSubviews() {
        super.layoutSubviews()
//        if !isViewSetup {
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
//                layout.itemSize = CGSize(width: frame.width / 1.2 , height: 210)
//                layout.itemSize.height = frame.height - 20
                layout.scrollDirection = scrollDirection!
                layout.itemSize = collectionViewitemSize!
                
//            }
//            isViewSetup = true
        }
        
    }
    func configure(resturants : [ResturantModel]) {
        data = resturants
        isRatesCollectionView = false
        collectionView.reloadData()
    }
    
    func configure(rates : [ResturantModel]) {
        data = rates
        isRatesCollectionView = true
        collectionView.reloadData()
    }
    
    func configure(products : [ProductModel]) {
        data = products
        isRatesCollectionView = false
        collectionView.reloadData()
    }
    
    func configureProducts(productsByCategoy : [ProductModel]) {
        data = productsByCategoy
        isRatesCollectionView = false
        isProductCategoryCollectionView = true
        collectionView.reloadData()
    }
    func configure(categories : [CategoryModel]) {
        data = categories
        isRatesCollectionView = false
        collectionView.reloadData()
    }


    
    
}
extension CollectionTableViewCell : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let model = data as? [ResturantModel], isRatesCollectionView {
            return model.count
        } else if let model = data as? [ResturantModel], !isRatesCollectionView {
            return model.count
        } else if let model = data as? [ProductModel], !isRatesCollectionView {
            return model.count
        }else if let model = data as? [CategoryModel], !isRatesCollectionView {
            return model.count
        }else if let model = data as? [ProductModel], isProductCategoryCollectionView {
            return model.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCollectionId, for:indexPath ) as! HomeCollectionViewCell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResturantHomeCollectionViewCellID", for:indexPath ) as! ResturantHomeCollectionViewCell
        
        if let model = data as? [ResturantModel], isRatesCollectionView {
            
//            cell.SetCell(name: model[indexPath.row]._name, image: model[indexPath.row]._image)
            cell.configure(rest: model[indexPath.row])
        } else if let model = data as? [ResturantModel], !isRatesCollectionView {

//            cell.SetCell(name: model[indexPath.row]._name, image: model[indexPath.row]._image)
            cell.configure(rest: model[indexPath.row])

        } else if let model = data as? [ProductModel], !isRatesCollectionView , !isProductCategoryCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductHomeCollectionViewCellID", for:indexPath ) as! ProductHomeCollectionViewCell
            cell.configure(product: model[indexPath.row])
            return cell

//            cell.SetCell(name: model[indexPath.row]._name, image: model[indexPath.row]._image)

        }else if let model = data as? [ProductModel], isProductCategoryCollectionView ,!isRatesCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductHomeCollectionViewCellID", for:indexPath ) as! ProductHomeCollectionViewCell
            cell.configure(product: model[indexPath.row])
            return cell
//            cell.SetCell(name: model[indexPath.row]._name, image: model[indexPath.row]._image)
        } else if let model = data as? [CategoryModel], !isRatesCollectionView {

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoryCellId, for:indexPath ) as! CategoryCollectionViewCell
            cell.SetCell(name: model[indexPath.row]._name)
            cell.SetSelectedCell(sender: model[indexPath.row]._selected)
            return cell
            
        }
        
        return cell
    }
    
}
extension CollectionTableViewCell : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      if let model = data as? [CategoryModel], !isRatesCollectionView {
            dataManger.DownloadPrudcutCategoryData(id: model[indexPath.row]._id)
            model.forEach({
                $0._selected = false
            })
            model[indexPath.row]._selected = true
            collectionView.reloadData()
      }else if let model = data as? [ResturantModel] {
        self.delegate?.Tap(DidTabOfResturant: model[indexPath.row])
      }else if let model =  data as? [ProductModel] {
        self.delegate?.Tap(DidTapPrudct: model[indexPath.row])

        
        }
    }
    
}


