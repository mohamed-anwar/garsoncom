//
//  RatingViewController.swift
//  PopupDialog
//
//  Created by Martin Wildfeuer on 11.07.16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import UIKit



class ChooseTableTypePopUp: UIViewController {
    weak var delegate : ChooseTableTypeDelegate!
    @IBOutlet weak var cafeButton: UIButton!
    @IBOutlet weak var resturantButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        makeBorderToView(resturantButton, boderWidth: 1, cornerRadius: 30, borderColor: .black, maskToBounds: true)
        makeBorderToView(cafeButton, boderWidth: 1, cornerRadius: 30, borderColor: .black, maskToBounds: true)
    }
    @IBAction func resturantButton(_ sender: UIButton) {
        delegate.ChooseTableType(type: false)
dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func cafeButton(_ sender: UIButton) {
        delegate.ChooseTableType(type: true)
        dismiss(animated: true, completion: nil)

    }
    

}

