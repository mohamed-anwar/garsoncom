//
//  SlideMenuTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/13/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit

class SlideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var slideImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(image : UIImage, name : String) {
        nameLabel.text = name
        slideImage.image = image
        
        
    }
    
}
