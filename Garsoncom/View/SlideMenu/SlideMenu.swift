//
//  SlideMenu.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/13/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class SlideMenu: UIView {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var mainStacView: UIStackView!
    
    @IBOutlet weak var stackHightConstraint: NSLayoutConstraint!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var EmailLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    func ConfigureView(name : String , image : String , email : String)  {
   
        NameLabel.text = name
        EmailLabel.text = email
        userImageView.kf.indicatorType = .activity
        userImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    
        })
        
        
    }
    
}
