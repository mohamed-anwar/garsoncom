//
//  ChooseGovernmentV.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/10/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import PMSuperButton
import DropDown


class ChooseGovernmentV: UIView {

    @IBOutlet weak var ChooseGovernmentButton: PMSuperButton!
    @IBOutlet weak var ChooseCityButton: PMSuperButton!
    @IBOutlet weak var SubmitButton: PMSuperButton!
    
    
    let setGovernment = DropDown()
    let setCity = DropDown()
    
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.setGovernment,
            self.setCity
            
        ]
    }()

    

}
