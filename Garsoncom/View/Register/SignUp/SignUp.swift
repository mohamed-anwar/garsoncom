//
//  SignUp.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/5/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


import UIKit
import SkyFloatingLabelTextField
import PMSuperButton

class SingUp: UIView {
    
    @IBOutlet weak var pickUpImageButton: UIButton!
    @IBOutlet weak var computerNameTxtF: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumberTxtF: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTxtF: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTxtF: SkyFloatingLabelTextField!
    @IBOutlet weak var signUpButton: PMSuperButton!
    
}
extension SingUp : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        switch(textField) {
        case computerNameTxtF :
            if !Validator.name(text) && text.count != 0 {
                computerNameTxtF.errorMessage = "Invalid Computer Name"
            }else if Validator.name(text) || text.count == 0{
                computerNameTxtF.errorMessage = ""
            }
        case phoneNumberTxtF :
            if !Validator.validatePhoneNumber(text: text) && text.count != 0 {
                phoneNumberTxtF.errorMessage = "Invalid Phone Number"
            }else if Validator.validatePhoneNumber(text: text) || text.count == 0  || text.isNumber{
                phoneNumberTxtF.errorMessage = ""
            }
        case emailTxtF :
            if !Validator.validateEmail(text) && text.count != 0{
                emailTxtF.errorMessage = "Invalid email"
            } else if Validator.validateEmail(text) || text.count == 0 {
                emailTxtF.errorMessage = ""
            }
            break
            
        case passwordTxtF :
            if !Validator.validatePassword(text: text) {
                passwordTxtF.errorMessage = "Invalid Password"
            } else if Validator.validatePassword(text: text) {
                passwordTxtF.errorMessage = ""
            }
            break
            
        default :
            break
        }
        return true
    }
}
