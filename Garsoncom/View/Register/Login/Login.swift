//
//  Login.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/5/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import PMSuperButton

class Login: UIView {
    @IBOutlet weak var emailTxtF: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTxtF: SkyFloatingLabelTextField!
    @IBOutlet weak var loginButton: PMSuperButton!
    @IBOutlet weak var signUpButton: UIButton!
    
}
extension Login : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        switch(textField) {
        case emailTxtF :
            if !Validator.validateEmail(text) && text.count != 0{
                emailTxtF.errorMessage = "Invalid email"
            } else if Validator.validateEmail(text) || text.count == 0 {
                emailTxtF.errorMessage = ""
            }
            break
            
        case passwordTxtF :
            if !Validator.validatePassword(text: text) {
                passwordTxtF.errorMessage = "Invalid Password"
            } else if Validator.validatePassword(text: text) {
                passwordTxtF.errorMessage = ""
            }
            break
            
        default :
            break
        }
        return true
    }
}
