//
//  ResturantDetailTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/13/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Kingfisher

class ResturantDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var DescreptionTextF: UITextView!
    @IBOutlet weak var ClientNameLabel: UILabel!
    @IBOutlet weak var clientImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func ConfigureCell (name : String , image : String , desc : String){
        ClientNameLabel.text = name
        DescreptionTextF.text = desc
        clientImageView.kf.indicatorType = .activity
        clientImageView.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
        })
        makeBorderToView(clientImageView, boderWidth: 1, cornerRadius: clientImageView.frame.width / 2, borderColor: .clear, maskToBounds: true)
        
        
        
    }
    
}
