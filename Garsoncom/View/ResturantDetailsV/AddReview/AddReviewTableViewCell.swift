//
//  AddReviewTableViewCell.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit

class AddReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var addReviewButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
