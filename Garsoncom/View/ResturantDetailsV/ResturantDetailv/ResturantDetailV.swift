//
//  ResturantDetailV.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/13/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher

class ResturantDetailV: UIView {
    @IBOutlet weak var contanerView: UIView!
    
    @IBOutlet weak var OpenToLabel: UILabel!
    @IBOutlet weak var OpenFromLabel: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ResturantImage: UIImageView!
    @IBOutlet weak var untilTimeLabel: UILabel!
    
    @IBOutlet weak var dileveryTimeLabel: UILabel!
    @IBOutlet weak var ResturantName: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    
    override func awakeFromNib() {
    }
    
    
    @IBOutlet weak var RateView: CosmosView!
    func ConfigureView(image : String , name : String , rate : Int , from : String , to : String , deliveryCoust : String , rateCount :String , deliveryTime : String , untilTime : String, isDelivery : Bool) {
        ResturantImage.kf.indicatorType = .activity
        ResturantImage.kf.setImage(with: URL(string: photoUrl + "/"  + image) ,
                                  placeholder:nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
        })
        ResturantName.text = name
        RateView.rating = Double(rate)
        RateView.text = "(" + rateCount + ")"
        OpenToLabel.text = convertDateformate_24_to12_andReverse(str_date: to, strdateFormat: "h:mm a",inputFormat:"HH:mm:ss")
        OpenFromLabel.text = convertDateformate_24_to12_andReverse(str_date: from, strdateFormat: "h:mm a",inputFormat:"HH:mm:ss")
        if isDelivery{
            deliveryLabel.text = deliveryCoust +  EGP
        }else{
            stackView.subviews.last?.removeFromSuperview()
        }
        dileveryTimeLabel.text = convertDateformate_24_to12_andReverse(str_date: deliveryTime, strdateFormat: "h:mm a",inputFormat:"HH:mm:ss")
        untilTimeLabel.text = convertDateformate_24_to12_andReverse(str_date: untilTime, strdateFormat: "h:mm a",inputFormat:"HH:mm:ss")
        makeBorderToView(contanerView, boderWidth: 1, cornerRadius: 15, borderColor: .gray, maskToBounds: true)
//        contanerView.round(corners: [.topLeft,.topRight,.bottomLeft,.bottomRight], radius: 15, borderColor: .gray, borderWidth: 1)

    }
}
