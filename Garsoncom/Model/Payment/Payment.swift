//
//  Payment.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/26/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import ObjectMapper
class PaymentMethod : Mappable{
    
    private var psymentMethod_id : String!
    private var psymentMethod_name : String!
    var _psymentMethod_id : String{
        get{
            if psymentMethod_id == nil{
                psymentMethod_id = ""
            }
            return psymentMethod_id
        }set{
            psymentMethod_id = newValue
        }
    }
    var _psymentMethod_name : String{
        get{
            if psymentMethod_name == nil{
                psymentMethod_name = ""
            }
            return psymentMethod_name
        }set{
            psymentMethod_name = newValue
        }
    }
    
    init() {
    }
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _psymentMethod_id <- map["_psymentMethod_id"]
        _psymentMethod_name <- map["_psymentMethod_name"]
        
    }
    
    func debugClass() {
        
        print("_psymentMethod_name : ", _psymentMethod_name , "\n","image : ", _psymentMethod_id , "\n","_psymentMethod_id ")
        
    }
}
