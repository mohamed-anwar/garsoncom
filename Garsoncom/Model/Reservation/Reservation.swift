//
//  Reservation.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/29/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import ObjectMapper
class Reservation: Mappable {
    init() {
    }
    private var id : Int!
    private var tableId : Int!
    private var clientId : Int!
    private var from : String!
    private var to : String!
    private var status : String!
    private var restaurantName : String!
    private var table : TablesModel!
    var _table : TablesModel {
        get {
            if table == nil {
                table = TablesModel()
            }
            return table
        }
        set {
            table = newValue
        }
    }
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _table_id : Int {
        get {
            if tableId == nil {
                tableId = 0
            }
            return tableId
        }
        set {
            tableId = newValue
        }
    }
    
    var _client_id : Int {
        get {
            if clientId == nil {
                clientId = 0
            }
            return clientId
        }
        set {
            clientId = newValue
        }
    }
    
    var _from : String {
        get {
            if from == nil {
                from = ""
            }
            return from
        }
        set {
            from = newValue
        }
    }
    
    var _to : String {
        get {
            if to == nil {
                to = ""
            }
            return to
        }
        set {
            to = newValue
        }
    }
    
    var _status : String {
        get {
            if status == nil {
                status = ""
            }
            return status
        }
        set {
            status = newValue
        }
    }
    
    var _restaurant_name : String {
        get {
            if restaurantName == nil {
                restaurantName = ""
            }
            return restaurantName
        }
        set {
            restaurantName = newValue
        }
    }
    
    
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _table_id <- map["table_id"]
        _client_id <- map["client_id"]
        _from <- map["from"]
        _to <- map["to"]
        _status <- map["status"]
        _restaurant_name <- map["restaurant_name"]
        _table <- map["table"]
        
    }
    
    func debugClass() {
        
        print("id : ", _id , "\n","table_id : ", _table_id , "\n","client_id : ", _client_id , "\n","from : ", _from , "\n","to : ", _to , "\n","status : ", _status , "\n","restaurant_name : ", _restaurant_name , "\n")
        
    }
    
}
