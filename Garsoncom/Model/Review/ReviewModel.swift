//
//  ReviewModel.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/25/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import ObjectMapper



class ReviewModel: Mappable {
    private var id : Int!
    private var desc : String!
    private var restId : Int!
    private var clientId : Int!
    private var rate : Int!
    private var client : ClientMode!
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _desc : String {
        get {
            if desc == nil {
                desc = ""
            }
            return desc
        }
        set {
            desc = newValue
        }
    }
    
    var _restId : Int {
        get {
            if restId == nil {
                restId = 0
            }
            return restId
        }
        set {
            restId = newValue
        }
    }
    
    var _clientId : Int {
        get {
            if clientId == nil {
                clientId = 0
            }
            return clientId
        }
        set {
            clientId = newValue
        }
    }
    
    var _rate : Int {
        get {
            if rate == nil {
                rate = 0
            }
            return rate
        }
        set {
            rate = newValue
        }
    }
 
    
    var _client : ClientMode {
        get {
            if client == nil {
                client = ClientMode()
            }
            return client
        } set {
            client = newValue
        }
    }
    
    
    init() {
    }
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _desc <- map["description"]
        _restId <- map["restaurant_id"]
        _clientId <- map["client_id"]
        _rate <- map["rate"]
        _client <- map["client"]
        
    }

    func debugClass() {
        
        print("id : ", _id , "\n","desc : ", _desc , "\n","restId : ", _restId , "\n","clientId : ", _clientId , "\n","rate : ", _rate , "\n")
        _client.debugClass()
        
    }

    

}
