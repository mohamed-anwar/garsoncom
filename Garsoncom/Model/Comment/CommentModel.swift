//
//  Comment.swift
//  Garsoncom
//  Created by Mohamed anwar on 7/1/18.
//  Copyright © 2018 Rivers. All rights reserved.

import ObjectMapper
class CommentModel: Mappable {
    private var id : Int!
    private var comment : String!
    private var post_id : Int!
    private var time : String!
    private var is_my_comment : Bool!
    private var user : Client!
    init() {
    }
    var _user : Client {
        get {
            if user == nil {
                user = Client()
            }
            return user
        }
        set {
            user = newValue
        }
    }
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _comment : String {
        get {
            if comment == nil {
                comment = ""
            }
            return comment
        }
        set {
            comment = newValue
        }
    }
    
    var _post_id : Int {
        get {
            if post_id == nil {
                post_id = 0
            }
            return post_id
        }
        set {
            post_id = newValue
        }
    }
    
    var _time : String {
        get {
            if time == nil {
                time = ""
            }
            return time
        }
        set {
            time = newValue
        }
    }
    
    var _is_my_comment : Bool {
        get {
            if is_my_comment == nil {
                is_my_comment = false
            }
            return is_my_comment
        }
        set {
            is_my_comment = newValue
        }
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _comment <- map["comment"]
        _post_id <- map["post_id"]
        _time <- map["time"]
        _is_my_comment <- map["is_my_comment"]
        _user <- map["user"]
        
    }
    
    func debugClass() {
        
        print("id : ", _id , "\n","comment : ", _comment , "\n","post_id : ", _post_id , "\n","time : ", _time , "\n","is_my_comment : ", _is_my_comment , "\n")
        
    }

}
