//
//  ResturantModel.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/18/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import ObjectMapper

class ResturantModel: Mappable {
    
    private var id : Int!
    private var city_id : Int!
    private var email : String!
    private var mobile : String!
    private var phone : String!
    private var image : String!
    private var about : String!
    private var numBranches : Int!
    private var mainAddress : String!
    private var haveDelivery : Bool!
    private var type : String!
    private var deliveryTime : Int!
    private var deliveryCost : String!
    private var lat : Double!
    private var lng : Double!
    private var deliveryFrom : String!
    private var deliveryTo : String!
    private var openFrom : String!
    private var openTo : String!
    private var orderBalance : String!
    private var tableBalance : String!
    private var partyBalance : String!
    private var betyBalance : String!
    private var tableCancelTime : Int!
    private var orderCancelTime : Int!
    private var distance : Int!
    private var name : String!
    private var haveTables : Bool!
    private var categoriesHuman : String!
    private var rate : Int!
    private var rateCount : Int!
    private var isFavourite : Bool!
    private var isOpen : Bool!
    private var isUser : Bool!
    private var isBety : Bool!
    private var active : Bool!
    private var isOnline : Bool!
    private var isDeleviery : Bool!
    private var selected : Bool!
    private var orderCount : Int!
    private var rates : [RateModel]!
    private var isOrder : Bool!
   
    private var min_delivery_cost : String!
    private var max_delivery_cost : String!
    var _min_delivery_cost : String{
        get{
            if min_delivery_cost == nil {
                min_delivery_cost = ""
            }
            return min_delivery_cost
            
        }set{
            min_delivery_cost = newValue
            
        }
    }
    var _max_delivery_cost : String{
        get{
            if max_delivery_cost == nil {
                max_delivery_cost = ""
            }
            return max_delivery_cost
            
        }set{
            max_delivery_cost = newValue
            
        }
    }
    
    var _isOrder : Bool{
        get{
            if isOrder == nil{
                isOrder =  false
            }
            return isOrder
        }set{
            isOrder = newValue
        }
    }
    var _orderCount : Int{
        get{
            if orderCount == nil{
                orderCount = 0
            }
            return orderCount
        }set{
            orderCount = newValue
        }
    }
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _city_id : Int {
        get {
            if city_id == nil {
                city_id = 0
            }
            return city_id
        }
        set {
            city_id = newValue
        }
    }
    
    var _email : String {
        get {
            if email == nil {
                email = ""
            }
            return email
        }
        set {
            email = newValue
        }
    }
    
    var _mobile : String {
        get {
            if mobile == nil {
                mobile = ""
            }
            return mobile
        }
        set {
            mobile = newValue
        }
    }
    
    var _phone : String {
        get {
            if phone == nil {
                phone = ""
            }
            return phone
        }
        set {
            phone = newValue
        }
    }
    
    var _image : String {
        get {
            if image == nil {
                image = ""
            }
            return image
        }
        set {
            image = newValue
        }
    }
    
    var _about : String {
        get {
            if about == nil {
                about = ""
            }
            return about
        }
        set {
            about = newValue
        }
    }
    
    var _numBranches : Int {
        get {
            if numBranches == nil {
                numBranches = 0
            }
            return numBranches
        }
        set {
            numBranches = newValue
        }
    }
    
    var _mainAddress : String {
        get {
            if mainAddress == nil {
                mainAddress = ""
            }
            return mainAddress
        }
        set {
            mainAddress = newValue
        }
    }
    
    var _haveDelivery : Bool {
        get {
            if haveDelivery == nil {
                haveDelivery = false
            }
            return haveDelivery
        }
        set {
            haveDelivery = newValue
        }
    }
    
    var _type : String {
        get {
            if type == nil {
                type = ""
            }
            return type
        }
        set {
            type = newValue
        }
    }
    
    var _deliveryTime : Int {
        get {
            if deliveryTime == nil {
                deliveryTime = 0
            }
            return deliveryTime
        }
        set {
            deliveryTime = newValue
        }
    }
    
    var _deliveryCost : String {
        get {
            if deliveryCost == nil {
                deliveryCost = "0.0"
            }
            return deliveryCost
        }
        set {
            deliveryCost = newValue
        }
    }
    
    var _lat : Double {
        get {
            if lat == nil {
                lat = 0.0
            }
            return lat
        }
        set {
            lat = newValue
        }
    }
    
    var _lng : Double {
        get {
            if lng == nil {
                lng = 0.0
            }
            return lng
        }
        set {
            lng = newValue
        }
    }
    
    var _deliveryFrom : String {
        get {
            if deliveryFrom == nil {
                deliveryFrom = ""
            }
            return deliveryFrom
        }
        set {
            deliveryFrom = newValue
        }
    }
    
    var _deliveryTo : String {
        get {
            if deliveryTo == nil {
                deliveryTo = ""
            }
            return deliveryTo
        }
        set {
            deliveryTo = newValue
        }
    }
    
    var _openFrom : String {
        get {
            if openFrom == nil {
                openFrom = ""
            }
            return openFrom
        }
        set {
            openFrom = newValue
        }
    }
    
    var _openTo : String {
        get {
            if openTo == nil {
                openTo = ""
            }
            return openTo
        }
        set {
            openTo = newValue
        }
    }
    
    var _orderBalance : String {
        get {
            if orderBalance == nil {
                orderBalance = ""
            }
            return orderBalance
        }
        set {
            orderBalance = newValue
        }
    }
    
    var _tableBalance : String {
        get {
            if tableBalance == nil {
                tableBalance = ""
            }
            return tableBalance
        }
        set {
            tableBalance = newValue
        }
    }
    
    var _partyBalance : String {
        get {
            if partyBalance == nil {
                partyBalance = ""
            }
            return partyBalance
        }
        set {
            partyBalance = newValue
        }
    }
    
    var _betyBalance : String {
        get {
            if betyBalance == nil {
                betyBalance = ""
            }
            return betyBalance
        }
        set {
            betyBalance = newValue
        }
    }
    
    var _tableCancelTime : Int {
        get {
            if tableCancelTime == nil {
                tableCancelTime = 0
            }
            return tableCancelTime
        }
        set {
            tableCancelTime = newValue
        }
    }
    
    var _orderCancelTime : Int {
        get {
            if orderCancelTime == nil {
                orderCancelTime = 0
            }
            return orderCancelTime
        }
        set {
            orderCancelTime = newValue
        }
    }
    
    var _distance : Int {
        get {
            if distance == nil {
                distance = 0
            }
            return distance
        }
        set {
            distance = newValue
        }
    }
    
    var _name : String {
        get {
            if name == nil {
                name = ""
            }
            return name
        }
        set {
            name = newValue
        }
    }
    
    var _haveTables : Bool {
        get {
            if haveTables == nil {
                haveTables = false
            }
            return haveTables
        }
        set {
            haveTables = newValue
        }
    }
    
    var _categoriesHuman : String {
        get {
            if categoriesHuman == nil {
                categoriesHuman = ""
            }
            return categoriesHuman
        }
        set {
            categoriesHuman = newValue
        }
    }
    
    var _rate : Int {
        get {
            if rate == nil {
                rate = 0
            }
            return rate
        }
        set {
            rate = newValue
        }
    }
    
    var _rateCount : Int {
        get {
            if rateCount == nil {
                rateCount = 0
            }
            return rateCount
        }
        set {
            rateCount = newValue
        }
    }
    
    var _isFavourite : Bool {
        get {
            if isFavourite == nil {
                isFavourite = false
            }
            return isFavourite
        }
        set {
            isFavourite = newValue
        }
    }
    var _selected : Bool {
        get {
            if selected == nil {
                selected = false
            }
            return selected
        }
        set {
            selected = newValue
        }
    }
    var _isBety : Bool {
        get {
            if isBety == nil {
                isBety = false
            }
            return isBety
        }
        set {
            isBety = newValue
        }
    }
    
    var _isOpen : Bool {
        get {
            if isOpen == nil {
                isOpen = false
            }
            return isOpen
        }
        set {
            isOpen = newValue
        }
    }
    var _active : Bool {
        get {
            if active == nil {
                active = false
            }
            return active
        }
        set {
            active = newValue
        }
    }
    var _isUser : Bool {
        get {
            if isUser == nil {
                isUser = false
            }
            return isUser
        }
        set {
            isUser = newValue
        }
    }
    var _isOnline : Bool {
        get {
            if isOnline == nil {
                isOnline = false
            }
            return isOnline
        }
        set {
            isOnline = newValue
        }
    }
    var _isDeleviry: Bool {
        get {
            if isDeleviery == nil {
                isDeleviery = false
            }
            return isDeleviery
        }
        set {
            isDeleviery = newValue
        }
    }
    
    

    var _rates : [RateModel] {
        get {
            if rates == nil {
                rates = [RateModel]()
            }
            return rates
        } set {
            rates = newValue
        }
    }
    
    
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
  
    
    init() {
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _city_id <- map["city_id"]
        _email <- map["email"]
        _mobile <- map["mobile"]
        _phone <- map["phone"]
        _image <- map["image"]
        _about <- map["about"]
        _numBranches <- map["num_branches"]
        _mainAddress <- map["main_address"]
        _haveDelivery <- map["have_delivery"]
        _type <- map["type"]
        _deliveryTime <- map["delivery_time"]
        _deliveryCost <- map["delivery_cost"]
        _lat <- map["lat"]
        _lng <- map["lng"]
        _deliveryFrom <- map["delivery_from"]
        _deliveryTo <- map["delivery_to"]
        _openFrom <- map["open_from"]
        _openTo <- map["open_to"]
        _orderBalance <- map["order_balance"]
        _tableBalance <- map["table_balance"]
        _partyBalance <- map["party_balance"]
        _betyBalance <- map["bety_balance"]
        _tableCancelTime <- map["table_cancel_time"]
        _orderCancelTime <- map["order_cancel_time"]
        _distance <- map["distance"]
        _name <- map["name"]
        _haveTables <- map["have_tables"]
        _categoriesHuman <- map["categories_human"]
        _rate <- map["rate"]
        _rateCount <- map["rate_count"]
        _isFavourite <- map["is_favourite"]
        _isOpen <- map["is_open"]
        _isUser <- map["is_user"]
        _isBety <- map["is_bety"]
        _rates <- map["rates"]
        _orderCount <- map["orders_count"]
        _active <- map["is_active"]
        _isOnline <- map["is_online"]
        _isDeleviry <- map["is_delivery"]
        _isOrder <- map["is_order"]
        _max_delivery_cost <- map["max_delivery_cost"]
        _min_delivery_cost <- map["min_delivery_cost"]

    }
    
    func debugClass() {
        print("id : ", _id , "\n","city_id : ", _city_id , "\n","email : ", _email , "\n","mobile : ", _mobile , "\n","phone : ", _phone , "\n","image : ", _image , "\n","about : ", _about , "\n","num_branches : ", _numBranches , "\n","main_address : ", _mainAddress , "\n","have_delivery : ", _haveDelivery , "\n","type : ", _type , "\n","delivery_time : ", _deliveryTime , "\n","delivery_cost : ", _deliveryCost , "\n","lat : ", _lat , "\n","lng : ", _lng , "\n","delivery_from : ", _deliveryFrom , "\n","delivery_to : ", _deliveryTo , "\n","open_from : ", _openFrom , "\n","open_to : ", _openTo , "\n","order_balance : ", _orderBalance , "\n","table_balance : ", _tableBalance , "\n","party_balance : ", _partyBalance , "\n","bety_balance : ", _betyBalance , "\n","table_cancel_time : ", _tableCancelTime , "\n","order_cancel_time : ", _orderCancelTime , "\n","distance : ", _distance , "\n","name : ", _name , "\n","have_tables : ", _haveTables , "\n","categories_human : ", _categoriesHuman , "\n","rate : ", _rate , "\n","rate_count : ", _rateCount , "\n","is_favourite : ", _isFavourite , "\n","is_open : ", _isOpen , "\n")
        _rates.forEach({
            $0.debugClass()
        })
    }

//
//
    












    

}

