//
//  PartyType.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/13/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


import ObjectMapper
class PartyType: Mappable {
    init() {
    }
    private var id : Int!

    private var name : String!

   
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    
    var _name : String {
        get {
            if name == nil {
                name = ""
            }
            return name
        }
        set {
            name = newValue
        }
    }
    
   
    
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _name <- map["name"]
       
        
    }
    
    
}
