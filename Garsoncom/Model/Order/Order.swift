//
//  Order.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import RealmSwift
 @objc  class Order : Object{
    @objc dynamic var price : Double = 0
    @objc dynamic var total  = ""
    @objc dynamic var deliveryTime  = ""
    @objc dynamic var deliveryCost  = ""
    @objc dynamic var address  = ""
    @objc dynamic var name  = ""
    @objc dynamic var phone  = ""
    @objc dynamic var future_date_time = ""
    @objc dynamic var lat  = ""
    @objc dynamic var lng  = ""
    @objc dynamic var psymentMethod_id  = ""
    @objc dynamic var rest_id  = ""
    @objc dynamic var restName  = ""
    @objc dynamic var deliverMin  = ""
    @objc dynamic var deliverMax  = ""



    var foods  = List<Food>()
}
