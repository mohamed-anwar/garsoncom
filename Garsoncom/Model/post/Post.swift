//
//  Post.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/1/18.
//  Copyright © 2018 Rivers. All rights reserved.
//
//"id": 45,
//"title": "sssssdfsdf",
//"image": "uploads/posts/153044162231132.jpg",
//"description": "sssssssssss",
//"client_id": 61,
//"votes_count": 0,
//"comments_count": 2,
//"time": "7 Hour ago",
//"is_my_post": 0,
//"is_vote": 0,
import ObjectMapper
class Post: Mappable {
    private var id : Int!
    private var title : String!
    private var image : String!
    private var description : String!
    private var client_id : Int!
    private var votes_count : Int!
    private var comments_count : Int!
    private var time : String!
    private var is_my_post : Bool!
    private var is_vote : Bool!
    private var client : Client!
    init() {
    }
    
    var _client : Client {
        get {
            if client == nil {
                client = Client()
            }
            return client
        }
        set {
            client = newValue
        }
    }
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _title : String {
        get {
            if title == nil {
                title = ""
            }
            return title
        }
        set {
            title = newValue
        }
    }
    
    var _image : String {
        get {
            if image == nil {
                image = ""
            }
            return image
        }
        set {
            image = newValue
        }
    }
    
    var _description : String {
        get {
            if description == nil {
                description = ""
            }
            return description
        }
        set {
            description = newValue
        }
    }
    
    var _client_id : Int {
        get {
            if client_id == nil {
                client_id = 0
            }
            return client_id
        }
        set {
            client_id = newValue
        }
    }
    
    var _votes_count : Int {
        get {
            if votes_count == nil {
                votes_count = 0
            }
            return votes_count
        }
        set {
            votes_count = newValue
        }
    }
    
    var _comments_count : Int {
        get {
            if comments_count == nil {
                comments_count = 0
            }
            return comments_count
        }
        set {
            comments_count = newValue
        }
    }
    
    var _time : String {
        get {
            if time == nil {
                time = ""
            }
            return time
        }
        set {
            time = newValue
        }
    }
    
    var _is_my_post : Bool {
        get {
            if is_my_post == nil {
                is_my_post = false
            }
            return is_my_post
        }
        set {
            is_my_post = newValue
        }
    }
    
    var _is_vote : Bool {
        get {
            if is_vote == nil {
                is_vote = false
            }
            return is_vote
        }
        set {
            is_vote = newValue
        }
    }
    
    
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _title <- map["title"]
        _image <- map["image"]
        _description <- map["description"]
        _client_id <- map["client_id"]
        _votes_count <- map["votes_count"]
        _comments_count <- map["comments_count"]
        _time <- map["time"]
        _is_my_post <- map["is_my_post"]
        _is_vote <- map["is_vote"]
        _client <- map["client"]

        
    }
    
    func debugClass() {
        
        print("id : ", _id , "\n","title : ", _title , "\n","image : ", _image , "\n","description : ", _description , "\n","client_id : ", _client_id , "\n","votes_count : ", _votes_count , "\n","comments_count : ", _comments_count , "\n","time : ", _time , "\n","is_my_post : ", _is_my_post , "\n","is_vote : ", _is_vote , "\n")
        
    }
    
    
    
    
}
