//
//  SizeModel.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import ObjectMapper

//id": 49,
//"name": "large",
//"price": "10.00",
//"offer": "0",
//"offer_price": "10"

class SizeModel: Mappable {

    private var id : Int!
    private var name : String!
    private var price : String!
    private var offer : Int!
    private var offerPrice : String!
    private var selected : Bool!
    var _selected : Bool {
        get {
            if selected == nil {
                selected = false
            }
            return selected
        }
        set {
            selected = newValue
        }
    }
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _name : String {
        get {
            if name == nil {
                name = ""
            }
            return name
        }
        set {
            name = newValue
        }
    }
    
    var _price : String {
        get {
            if price == nil {
                price = ""
            }
            return price
        }
        set {
            price = newValue
        }
    }
    
    var _offer : Int {
        get {
            if offer == nil {
                offer = 0
            }
            return offer
        }
        set {
            offer = newValue
        }
    }
    
    var _offer_price : String {
        get {
            if offerPrice == nil {
                offerPrice = ""
            }
            return offerPrice
        }
        set {
            offerPrice = newValue
        }
    }
    

    init(){
        
    }
    
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _name <- map["name"]
        _price <- map["price"]
        _offer <- map["offer"]
        _offer_price <- map["offer_price"]
        
    }
    
    func debugClass() {
        
        print("id : ", _id , "\n","name : ", _name , "\n","price : ", _price , "\n","offer : ", _offer , "\n","offer_price : ", _offer_price , "\n"," : ", "\n")
        
    }


    
    
}
