//
//  UserModel.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/5/18.
//  Copyright © 2018 Rivers. All rights reserved.
import UIKit
import ObjectMapper
 class UserModel:Mappable{
    private var id : Int!
       private var name : String!
       private var phone : String!
       private var address : String!
       private var email : String!
       private var image : String!
       private var facebook_image_link : String!
       private var lat : String!
       private var lng : String!
       private var numberOfOrders : Int!
       private var city_id  : Int!
       private var apiToken : String!

    
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _name : String {
        get {
            if name == nil {
                name = ""
            }
            return name
        }
        set {
            name = newValue
            userName = name
        }
    }
    
    var _phone : String {
        get {
            if phone == nil {
                phone = ""
            }
            return phone
        }
        set {
            phone = newValue
            userPhone = phone
        }
    }
    
    var _address : String {
        get {
            if address == nil {
                address = ""
            }
            return address
        }
        set {
            address = newValue
            userAddress = address
        }
    }
    
    var _email : String {
        get {
            if email == nil {
                email = ""
            }
            return email
        }
        set {
            email = newValue
           userEmail = email
        }
    }
    
    var _image : String {
        get {
            if image == nil {
                image = ""
            }
            return image
        }
        set {
            image = newValue
            userImage = image
        }
    }
    
    var _facebook_image_link : String {
        get {
            if facebook_image_link == nil {
                facebook_image_link = ""
            }
            return facebook_image_link
        }
        set {
            facebook_image_link = newValue
        }
    }
    
    var _lat : String {
        get {
            if lat == nil {
                lat = ""
            }
            return lat
        }
        set {
            lat = newValue
        }
    }
    
    var _lng : String {
        get {
            if  lng == nil {
                lng = ""
            }
            return lng
        }
        set {
            lng = newValue
        }
    }
    
    var _numberOfOrder : Int {
        get {
            if numberOfOrders == nil {
                numberOfOrders = 0
            }
            return numberOfOrders
        }
        set {
            numberOfOrders = newValue
        }
    }
    var _apiToken : String {
        get {
            if apiToken == nil {
                apiToken = ""
            }
            return apiToken
        }
        set {
            apiToken = newValue
        }
    }
    
    var _city_id : Int {
        get {
            if city_id == nil {
                city_id = 0
            }
            return city_id
        }
        set {
            city_id = newValue
        }
    }
    
    init() {
    }
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    func mapping(map: Map) {
        
        _id <- map["id"]
        _name <- map["name"]
        _phone <- map["phone"]
        _address <- map["address"]
        _email <- map["email"]
        _image <- map["image"]
        _facebook_image_link <- map["facebook_image_link"]
        _lat <- map["lat"]
        _lng <- map["lng"]
        _numberOfOrder <- map["number_of_orders"]
        _city_id <- map["city_id"]
        _apiToken <- map["api_token"]
        
    }
    
    func debugClass() {
        print("id : ", _id , "\n","name : ", _name , "\n","phone : ", _phone , "\n","address : ", _address , "\n","email : ", _email , "\n","image : ", _image , "\n","facebook_image_link : ", _facebook_image_link , "\n","lat : ", _lat , "\n"," : ", lng, "\n","balance : ", _numberOfOrder , "\n","city_id : ", _city_id , "\n")
        
    }
    
    
    
}
