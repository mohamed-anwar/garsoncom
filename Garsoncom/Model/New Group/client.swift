//
//  client.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/1/18.
//  Copyright © 2018 Rivers. All rights reserved.


import ObjectMapper
class Client : Mappable{
    init() {
    }
    
    private var id : Int!
    private var name : String!
    private var phone : Int!
    private var address : String!
    private var email : String!
    private var image : String!
    private var lat : Double!
    private var lng : Double!
    private var balance : String!
    private var city_id : Int!
    private var number_of_orders : Int!
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _name : String {
        get {
            if name == nil {
                name = ""
            }
            return name
        }
        set {
            name = newValue
        }
    }
    
    var _phone : Int {
        get {
            if phone == nil {
                phone = 0
            }
            return phone
        }
        set {
            phone = newValue
        }
    }
    
    var _address : String {
        get {
            if address == nil {
                address = ""
            }
            return address
        }
        set {
            address = newValue
        }
    }
    
    var _email : String {
        get {
            if email == nil {
                email = ""
            }
            return email
        }
        set {
            email = newValue
        }
    }
    
    var _image : String {
        get {
            if image == nil {
                image = ""
            }
            return image
        }
        set {
            image = newValue
        }
    }
    
    var _lat : Double {
        get {
            if lat == nil {
                lat = 0.0
            }
            return lat
        }
        set {
            lat = newValue
        }
    }
    
    var _lng : Double {
        get {
            if lng == nil {
                lng = 0.0
            }
            return lng
        }
        set {
            lng = newValue
        }
    }
    
    var _balance : String {
        get {
            if balance == nil {
                balance = ""
            }
            return balance
        }
        set {
            balance = newValue
        }
    }
    
    var _city_id : Int {
        get {
            if city_id == nil {
                city_id = 0
            }
            return city_id
        }
        set {
            city_id = newValue
        }
    }
    
    var _number_of_orders : Int {
        get {
            if number_of_orders == nil {
                number_of_orders = 0
            }
            return number_of_orders
        }
        set {
            number_of_orders = newValue
        }
    }
    
    
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _name <- map["name"]
        _phone <- map["phone"]
        _address <- map["address"]
        _email <- map["email"]
        _image <- map["image"]
        _lat <- map["lat"]
        _lng <- map["lng"]
        _balance <- map["balance"]
        _city_id <- map["city_id"]
        _number_of_orders <- map["number_of_orders"]
        
    }
    
    func debugClass() {
        
        print("id : ", _id , "\n","name : ", _name , "\n","phone : ", _phone , "\n","address : ", _address , "\n","email : ", _email , "\n","image : ", _image , "\n","lat : ", _lat , "\n","lng : ", _lng , "\n","balance : ", _balance , "\n","city_id : ", _city_id , "\n","number_of_orders : ", _number_of_orders , "\n")
        
    }
}
