//
//  OrderHeader.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import ObjectMapper
class OrderHeader : Mappable {
    
    private var restaurantImage : String!
    private var collapsed: Bool!
    private var id : Int!
    private var clientId : Int!
    private var restaurantId : Int!
    private var paymentMethodId : Int!
    private var lat : Double!
    private var lng : Double!
    private var status : String!
    private var deliveryCost : String!
    private var orderNo : Int!
    private var percent : Int!
    private var total : String!
    private var delivery : String!
    private var remaining : Int!
    private var totalCost : Int!
    private var remainingBySecond : Int!
    private var orderDetails : [OrderDetails]!
    private var created_at : String!
    private var restname : String!
    var _restName : String{
        get{
            if restname == nil{
                restname = ""
            }
            return restname
        }set{
            restname = newValue
        }
    }
    var _restaurantImage : String{
        get{
            if restaurantImage == nil{
                restaurantImage = ""
            }
        return restaurantImage
        }set{
            restaurantImage = newValue
        }
    }
    var _created_at : String {
        get {
            if created_at == nil {
                created_at = ""
            }
            return created_at
        }
        set {
            created_at = newValue
        }
    }
    
    var _collapsed : Bool {
        get {
            if collapsed == nil {
                collapsed = true
            }
            return collapsed
        }
        set {
            collapsed = newValue
        }
    }
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _clientId : Int {
        get {
            if clientId == nil {
                clientId = 0
            }
            return clientId
        }
        set {
            clientId = newValue
        }
    }
    
    var _restaurantId : Int {
        get {
            if restaurantId == nil {
                restaurantId = 0
            }
            return restaurantId
        }
        set {
            restaurantId = newValue
        }
    }
    
    var _paymentMethodId : Int {
        get {
            if paymentMethodId == nil {
                paymentMethodId = 0
            }
            return paymentMethodId
        }
        set {
            paymentMethodId = newValue
        }
    }
    
    var _lat : Double {
        get {
            if lat == nil {
                lat = 0.0
            }
            return lat
        }
        set {
            lat = newValue
        }
    }
    
    var _lng : Double {
        get {
            if lng == nil {
                lng = 0.0
            }
            return lng
        }
        set {
            lng = newValue
        }
    }
    
    var _status : String {
        get {
            if status == nil {
                status = ""
            }
            return status
        }
        set {
            status = newValue
        }
    }
    
    var _deliveryCost : String {
        get {
            if deliveryCost == nil {
                deliveryCost = ""
            }
            return deliveryCost
        }
        set {
            deliveryCost = newValue
        }
    }
    
    var _orderNo : Int {
        get {
            if orderNo == nil {
                orderNo = 0
            }
            return orderNo
        }
        set {
            orderNo = newValue
        }
    }
    
    var _percent : Int {
        get {
            if percent == nil {
                percent = 0
            }
            return percent
        }
        set {
            percent = newValue
        }
    }
    
    var _total : String {
        get {
            if total == nil {
                total = ""
            }
            return total
        }
        set {
            total = newValue
        }
    }
    
    var _delivery : String {
        get {
            if delivery == nil {
                delivery = ""
            }
            return delivery
        }
        set {
            delivery = newValue
        }
    }
    
    var _remaining : Int {
        get {
            if remaining == nil {
                remaining = 0
            }
            return remaining
        }
        set {
            remaining = newValue
        }
    }
    
    var _totalCost : Int {
        get {
            if totalCost == nil {
                totalCost = 0
            }
            return totalCost
        }
        set {
            totalCost = newValue
        }
    }
    
    var _remainingBySecond : Int {
        get {
            if remainingBySecond == nil {
                remainingBySecond = 0
            }
            return remainingBySecond
        }
        set {
            remainingBySecond = newValue
        }
    }
    
    var _orderDetails : [OrderDetails] {
        get {
            if orderDetails == nil {
                orderDetails = [OrderDetails]()
            }
            return orderDetails
        }
        set {
            orderDetails = newValue
        }
    }
    init() {
        
    }
    
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        _created_at <- map["created_at"]
        _id <- map["id"]
        _clientId <- map["client_id"]
        _restaurantId <- map["restaurant_id"]
        _paymentMethodId <- map["payment_method_id"]
        _lat <- map["lat"]
        _lng <- map["lng"]
        _status <- map["status"]
        _deliveryCost <- map["delivery_cost"]
        _orderNo <- map["order_no"]
        _percent <- map["percent"]
        _total <- map["total"]
        _delivery <- map["delivery"]
        _remaining <- map["remaining"]
        _totalCost <- map["total_cost"]
        _remainingBySecond <- map["remaining_by_second"]
        _orderDetails <- map["order_details"]
        _restaurantImage <- map["restaurant_image"]
        _restName <- map["restaurant_name"]
        
    }
    
    func debugClass() {
        
        print("id : ", _id , "\n","client_id : ", _clientId , "\n","restaurant_id : ", _restaurantId , "\n","payment_method_id : ", _paymentMethodId , "\n","lat : ", _lat , "\n","lng : ", _lng , "\n","status : ", _status , "\n","delivery_cost : ", _deliveryCost , "\n","order_no : ", _orderNo , "\n"," percent : ", _percent , "\n","total : ", _total , "\n","delivery : ", _delivery , "\n","remaining : ", _remaining , "\n","total_cost : ", _totalCost , "\n","remaining_by_second : ", _remainingBySecond , "\n","order_details : ", _orderDetails , "\n")
        
    }
    
    
    
    
    
    
}
