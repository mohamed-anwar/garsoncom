//
//  OrderDetails.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import ObjectMapper
class OrderDetails: Mappable {

    private var id : Int!
    private var productId : Int!
    private var orderId : Int!
    private var price :String!
    private var quantity : String!
    private var sizeName : String!
    private var special : Int!
    private var name : String!
    private var image : String!
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _productId : Int {
        get {
            if productId == nil {
                productId = 0
            }
            return productId
        }
        set {
            productId = newValue
        }
    }
    
    var _orderId : Int {
        get {
            if orderId == nil {
                orderId = 0
            }
            return orderId
        }
        set {
            orderId = newValue
        }
    }
    
    var _price : String {
        get {
            if price == nil {
                price = ""
            }
            return price
        }
        set {
            price = newValue
        }
    }
    
    var _quantity : String {
        get {
            if quantity == nil {
                quantity = ""
            }
            return quantity
        }
        set {
            quantity = newValue
        }
    }
    
    var _sizeName : String {
        get {
            if sizeName == nil {
                sizeName = ""
            }
            return sizeName
        }
        set {
            sizeName = newValue
        }
    }
    
    var _special : Int {
        get {
            if special == nil {
                special = 0
            }
            return special
        }
        set {
            special = newValue
        }
    }
    
    var _name : String {
        get {
            if name == nil {
                name = ""
            }
            return name
        }
        set {
            name = newValue
        }
    }
    
    var _image : String {
        get {
            if image == nil {
                image = ""
            }
            return image
        }
        set {
            image = newValue
        }
    }
    
    
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _productId <- map["product_id"]
        _orderId <- map["order_id"]
        _price <- map["price"]
        _quantity <- map["quantity"]
        _sizeName <- map["size_name"]
        _special <- map["special"]
        _name <- map["name"]
        _image <- map["image"]
        
    }
    
    func debugClass() {
        
        print("id : ", _id , "\n","product_id : ", _productId , "\n","order_id : ", _orderId , "\n","price : ", _price , "\n","quantity : ", _quantity , "\n","size_name : ", _sizeName , "\n","special : ", _special , "\n","name : ", _name , "\n","image : ", _image , "\n")
        
    }
    
}
