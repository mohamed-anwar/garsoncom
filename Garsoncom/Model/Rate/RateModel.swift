//
//  RateModel.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/18/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import ObjectMapper

class RateModel: Mappable {
    private var id : Int!
    private var client_id : Int!
    private var rateValue : Int!
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _client_id : Int {
        get {
            if client_id == nil {
                client_id = 0
            }
            return client_id
        }
        set {
            client_id = newValue
        }
    }
    
    var _rateValue : Int {
        get {
            if rateValue == nil {
                rateValue = 0
            }
            return rateValue
        }
        set {
            rateValue = newValue
        }
    }
    
    
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _client_id <- map["client_id"]
        _rateValue <- map["rateValue"]
        
    }
    
    init() {}
    
    func debugClass() {
        
        print("id : ", _id , "\n","client_id : ", _client_id , "\n","rateValue : ", _rateValue , "\n")
        
    }



}
