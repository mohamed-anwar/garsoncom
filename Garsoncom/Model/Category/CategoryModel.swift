//
//  CategoryModel.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

//"id": 2,
//"image": "uploads/grill.jpg",
//"name": "grillش"
import UIKit
import ObjectMapper
class CategoryModel: Mappable {
    
    private var id : Int!
    private var name : String!
    private var image : String!
    private var selected : Bool!
    
    
    
    var _selected : Bool {
        get {
            if selected == nil {
                selected = false
            }
            return selected
        }
        set {
            selected = newValue
        }
    }
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _name : String {
        get {
            if name == nil {
                name = ""
            }
            return name
        }
        set {
            name = newValue
        }
    }
    
    var _image : String {
        get {
            if image == nil {
                image = ""
            }
            return image
        }
        set {
            image = newValue
        }
    }

    
    init(){
        
    }
    
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _name <- map["name"]
        _image <- map["image"]
 
        
    }
    
    func debugClass() {
        
        print("id : ", _id , "\n","name : ", _name , "\n","image : ", _image , "\n")
        
    }
    
    
    
    
}
