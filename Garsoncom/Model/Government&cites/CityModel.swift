//
//  CityModel.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/10/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import ObjectMapper

class CityModel: Mappable {
    private var name : String!
    private var id : Int!
    private var lat : String!
    private var lng : String!


    
    
    var _name : String{
        get{
            if name == nil {
                name = ""
            }
            return name
        }set{
            name = newValue
            
        }
    }
    var _lat : String{
        get{
            if lat == nil {
                lat = ""
            }
            return lat
        }set{
            lat = newValue
            
        }
    }
    var _lng : String{
        get{
            if lng == nil {
                lng = ""
            }
            return lng
        }set{
            lng = newValue
            
        }
    }
    var _id : Int{
        get{
            if id == nil {
                id = 0
            }
            return id
        }set{
            id = newValue
            
        }
    }
    required init?(map: Map) {
        
    }
    
    init() {
    }
    func mapping(map: Map) {
        _id <- map["id"]
        _name <- map["name"]
        _lat <- map["lat"]
        _lng <- map["lng"]
    }
    
    
    
}
