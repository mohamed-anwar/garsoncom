//
//  GovernmentModel.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/10/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import ObjectMapper

class GovernmentModel: Mappable {
    private var name : String!
    private var id : Int!
    var _name : String{
        get{
            if name == nil {
                name = ""
            }
            return name
        }set{
            name = newValue
            
        }
    }
    var _id : Int{
        get{
            if id == nil {
                id = 0
            }
            return id
        }set{
            id = newValue
            
        }
    }
    required init?(map: Map) {
        
    }
    
    init() {
    }
    func mapping(map: Map) {
        _id <- map["id"]
        _name <- map["name"]
    }
    
    
    
}
