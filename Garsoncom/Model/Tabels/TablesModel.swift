//
//  TabelsModel.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/2/18.
//  Copyright © 2018 Rivers. All rights reserved.

import UIKit
import ObjectMapper

class TablesModel: Mappable {
    private var id : Int!
    private var num : Int!
    private var image : String!
    private var description : String!
    private var num_guests : Int!
    private var isSelected : Bool!
    
    var _isSelected : Bool {
        get {
            if isSelected == nil {
                isSelected = false
            }
            return isSelected
        }
        set {
            isSelected = newValue
        }
    }
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _num : Int {
        get {
            if num == nil {
                num = 0
            }
            return num
        }
        set {
            num = newValue
        }
    }
    
    var _image : String {
        get {
            if image == nil {
                image = ""
            }
            return image
        }
        set {
            image = newValue
        }
    }
    
    var _description : String {
        get {
            if description == nil {
                description = ""
            }
            return description
        }
        set {
            description = newValue
        }
    }
    
    var _num_guests : Int {
        get {
            if num_guests == nil {
                num_guests = 0
            }
            return num_guests
        }
        set {
            num_guests = newValue
        }
    }
    
    
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _id <- map["id"]
        _num <- map["num"]
        _image <- map["image"]
        _description <- map["description"]
        _num_guests <- map["num_guests"]
        
    }
    init() {
    }
    
    func debugClass() {
        
        print("id : ", _id , "\n","num : ", _num , "\n","image : ", _image , "\n","description : ", _description , "\n","num_guests : ", _num_guests , "\n")
        
    }

}
