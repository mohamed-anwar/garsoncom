//
//  BestClient.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/17/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import ObjectMapper
class BestClient: Mappable {
    private var name :String!
    private var image : String!
    private var ordersCount : Int!
    
    var _name : String {
        get {
            if name == nil {
                name = ""
            }
            return name
        }
        set {
            name = newValue
        }
    }
    
    var _image : String {
        get {
            if image == nil {
                image = ""
            }
            return image
        }
        set {
            image = newValue
        }
    }
    
    var _orders_count : Int {
        get {
            if ordersCount == nil {
                ordersCount = 0
            }
            return ordersCount
        }
        set {
            ordersCount = newValue
        }
    }
    
    init() {
    }
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _name <- map["name"]
        _image <- map["image"]
        _orders_count <- map["orders_count"]
        
    }
    
    func debugClass() {
        
        print("name : ", _name , "\n","image : ", _image , "\n","orders_count : ", _orders_count , "\n")
        
    }
}
