//
//  Food.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import RealmSwift
 @objc  class Food: Object {
    @objc dynamic var deliveryTime  = ""
    @objc dynamic var deliveryCost  = ""
    @objc dynamic var productId  = ""
    @objc dynamic var productImage  = ""
    @objc dynamic var productName  = ""
    @objc dynamic var productPrice  = ""
    @objc dynamic var quantity  = ""
    @objc dynamic var total  = ""
    @objc dynamic var cov1  = ""
    @objc dynamic var cov2  = ""
    @objc dynamic var cov3  = ""
    @objc dynamic var cov4  = ""
    @objc dynamic var sizeId  = ""    
}
