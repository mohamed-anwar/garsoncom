//
//  ProductModel.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/15/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import ObjectMapper


//"is_bety": 0,
//"is_online": 0

class ProductModel : Mappable {
    
    private var name : String!
    private var id : Int!
    private var image : String!
    private var co1 : String!
    private var co2 : String!
    private var co3 : String!
    private var co4 : String!
    private var cov1 : String!
    private var cov2 : String!
    private var cov3 : String!
    private var cov4 : String!
    private var categoryId : Int!
    private var restaurantId : Int!
    private var ratesCount : Int!
    private var description : String!
    private var rate : Int!
    private var rateCount : Int!
    private var isFavourite : Bool!
    private var sizes : [SizeModel]!
    private var isUser : Bool!
    private var isOpen : Bool!
    private var isOpenFuture : Bool!
    private var isDelivery : Bool!
    private var isDeliveryFuture : Bool!
    private var isOrder : Bool!
    private var isTable : Bool!
    private var isParty : Bool!
    private var isBety : Bool!
    private var isOnline : Bool!
    private var resturant : ResturantModel!
    
 
    
    var _resturant : ResturantModel {
        get {
            if resturant == nil {
                resturant = ResturantModel()
            }
            return resturant
        }
        set {
            resturant = newValue
        }
    }
    
    var _is_user : Bool {
        get {
            if isUser == nil {
                isUser = false
            }
            return isUser
        }
        set {
            isUser = newValue
        }
    }
    
    var _is_open : Bool {
        get {
            if isOpen == nil {
                isOpen = false
            }
            return isOpen
        }
        set {
            isOpen = newValue
        }
    }
    
    var _is_open_future : Bool {
        get {
            if isOpenFuture == nil {
                isOpenFuture = false
            }
            return isOpenFuture
        }
        set {
            isOpenFuture = newValue
        }
    }
    
    var _is_delivery : Bool {
        get {
            if isDelivery == nil {
                isDelivery = false
            }
            return isDelivery
        }
        set {
            isDelivery = newValue
        }
    }
    
    var _is_delivery_future : Bool {
        get {
            if isDeliveryFuture == nil {
                isDeliveryFuture = false
            }
            return isDeliveryFuture
        }
        set {
            isDeliveryFuture = newValue
        }
    }
    
    var _is_order : Bool {
        get {
            if isOrder == nil {
                isOrder = false
            }
            return isOrder
        }
        set {
            isOrder = newValue
        }
    }
    
    var _is_table : Bool {
        get {
            if isTable == nil {
                isTable = false
            }
            return isTable
        }
        set {
            isTable = newValue
        }
    }
    
    var _is_party : Bool {
        get {
            if isParty == nil {
                isParty = Bool()
            }
            return isParty
        }
        set {
            isParty = newValue
        }
    }
    
    var _is_bety : Bool {
        get {
            if isBety == nil {
                isBety = false
            }
            return isBety
        }
        set {
            isBety = newValue
        }
    }
    
    var _is_online : Bool {
        get {
            if isOnline == nil {
                isOnline = false
            }
            return isOnline
        }
        set {
            isOnline = newValue
        }
    }
    
    
    
    var _sizes : [SizeModel] {
        get {
            if sizes == nil {
                sizes = [SizeModel]()
            }
            return sizes
        }
        set {
            sizes = newValue
        }
    }
    
    var _name : String {
        get {
            if name == nil {
                name = ""
            }
            return name
        }
        set {
            name = newValue
        }
    }
    
    var _id : Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        }
        set {
            id = newValue
        }
    }
    
    var _image : String {
        get {
            if image == nil {
                image = ""
            }
            return image
        }
        set {
            image = newValue
        }
    }
    
    var _co1 : String {
        get {
            if co1 == nil {
                co1 = ""
            }
            return co1
        }
        set {
            co1 = newValue
        }
    }
    
    var _co2 : String {
        get {
            if co2 == nil {
                co2 = ""
            }
            return co2
        }
        set {
            co2 = newValue
        }
    }
    
    var _co3 : String {
        get {
            if co3 == nil {
                co3 = ""
            }
            return co3
        }
        set {
            co3 = newValue
        }
    }
    
    var _co4 : String {
        get {
            if co4 == nil {
                co4 = ""
            }
            return co4
        }
        set {
            co4 = newValue
        }
    }
    
    var _cov1 : String {
        get {
            if cov1 == nil {
                cov1 = ""
            }
            return cov1
        }
        set {
            cov1 = newValue
        }
    }
    
    var _cov2 : String {
        get {
            if cov2 == nil {
                cov2 = ""
            }
            return cov2
        }
        set {
            cov2 = newValue
        }
    }
    
    var _cov3 : String {
        get {
            if cov3 == nil {
                cov3 = ""
            }
            return cov3
        }
        set {
            cov3 = newValue
        }
    }
    
    var _cov4 : String {
        get {
            if cov4 == nil {
                cov4 = ""
            }
            return cov4
        }
        set {
            cov4 = newValue
        }
    }
    
    var _categoryId : Int {
        get {
            if categoryId == nil {
                categoryId = 0
            }
            return categoryId
        }
        set {
            categoryId = newValue
        }
    }
    
    var _restaurantId : Int {
        get {
            if restaurantId == nil {
                restaurantId = 0
            }
            return restaurantId
        }
        set {
            restaurantId = newValue
        }
    }
    
    var _ratesCount : Int {
        get {
            if ratesCount == nil {
                ratesCount = 0
            }
            return ratesCount
        }
        set {
            ratesCount = newValue
        }
    }
    
    var _description : String {
        get {
            if description == nil {
                description = ""
            }
            return description
        }
        set {
            description = newValue
        }
    }
    
    var _rate : Int {
        get {
            if rate == nil {
                rate = 0
            }
            return rate
        }
        set {
            rate = newValue
        }
    }
    
    var _rateCount : Int {
        get {
            if rateCount == nil {
                rateCount = 0
            }
            return rateCount
        }
        set {
            rateCount = newValue
        }
    }
    
    var _isFavourite : Bool {
        get {
            if isFavourite == nil {
                isFavourite = false
            }
            return isFavourite
        }
        set {
            isFavourite = newValue
        }
    }
    
    init(){
    }
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _name <- map["name"]
        _id <- map["id"]
        _image <- map["image"]
        _co1 <- map["co1"]
        _co2 <- map["co2"]
        _co3 <- map["co3"]
        _co4 <- map["co4"]
        _cov1 <- map["cov1"]
        _cov2 <- map["cov2"]
        _cov3 <- map["cov3"]
        _cov4 <- map["cov4"]
        _categoryId <- map["category_id"]
        _restaurantId <- map["restaurant_id"]
        _ratesCount <- map["rates_count"]
        _description <- map["description"]
        _rate <- map["rate"]
        _rateCount <- map["rate_count"]
        _isFavourite <- map["is_favourite"]
        _is_user <- map["is_user"]
        _is_open <- map["is_open"]
        _is_open_future <- map["is_open_future"]
        _is_delivery <- map["is_delivery"]
        _is_delivery_future <- map["is_delivery_future"]
        _is_order <- map["is_order"]
        _is_table <- map["is_table"]
        _is_party <- map["is_party"]
        _is_bety <- map["is_bety"]
        _is_online <- map["is_online"]
        _sizes <- map["sizes"]
        _resturant <- map["restaurant"]
        
        
        
    }
    
    func debugClass() {
        
        print("name : ", _name , "\n","id : ", _id , "\n","image : ", _image , "\n","co1 : ", _co1 , "\n","co2 : ", _co2 , "\n","co3 : ", _co3 , "\n","co4 : ", _co4 , "\n","cov1 : ", _cov1 , "\n","cov2 : ", _cov2 , "\n","cov3 : ", _cov3 , "\n","cov4 : ", _cov4 , "\n","categoryId : ", _categoryId , "\n","restaurantId : ", _restaurantId , "\n","ratesCount : ", _ratesCount , "\n","description : ", _description , "\n","rate : ", _rate , "\n","rateCount : ", _rateCount , "\n","isFavourite : ", _isFavourite , "\n")
        print("is_user : ", _is_user , "\n","is_open : ", _is_open , "\n","is_open_future : ", _is_open_future , "\n","is_delivery : ", _is_delivery , "\n","is_delivery_future : ", _is_delivery_future , "\n","is_order : ", _is_order , "\n","is_table : ", _is_table , "\n","is_party : ", _is_party , "\n","is_bety : ", _is_bety , "\n","is_online : ", _is_online , "\n")
        
        
    }
    
    
    
}
