//
//  LabelExtention.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/21/18.
//  Copyright © 2018 Rivers. All rights reserved.
//
import UIKit
extension UILabel {
    func addStrikedthrough (text: String){
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(text)")
        attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        attributedText = attributeString
    }
}
