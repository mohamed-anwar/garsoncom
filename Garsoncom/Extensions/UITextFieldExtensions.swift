//
//  CustomTextFieldAnimationExtensions.swift
//
//

import UIKit

extension UITextField {

    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setImage(image: String, width: Int, height: Int) {
        self.leftViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width , height: height))
        let img = UIImage(named: image)
        imageView.image = img
        self.leftView = imageView
    }
    
    func setButton(target: Any, imageName: String, function: Selector) {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: imageName), for: .normal)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: CGFloat(10), height: CGFloat(25))
        button.addTarget(target, action: function, for: .touchUpInside)
        self.rightView = button
        self.rightViewMode = .always
    }
}

