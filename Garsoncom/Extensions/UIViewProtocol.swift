//
//  UIViewExtensions.swift
//
//

import UIKit

extension UIView {
    
    // Example use: myView.addBorder(toSide: .Left, withColor: UIColor.redColor().CGColor, andThickness: 1.0)
    
    enum ViewSide {
        case Left, Right, Top, Bottom
    }
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        print()
        let border = CALayer()
        border.backgroundColor = color
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: 0, y: frame.size.height
            , width: UIScreen.main.bounds.width, height: thickness); break
        }
        
        layer.addSublayer(border)
    }
    func makeShadowAndBorderForView( _ view:UIView ,  cornerRadius : CGFloat ,borderWidth : CGFloat  , borderColor : UIColor , masksToBounds : Bool ,shadowColor : UIColor , shadowOffset : CGSize! =  CGSize(width: 0, height:1)  ,shadowRadius : CGFloat  , shadowOpacity : Float ){
    
        view.layer.cornerRadius = cornerRadius
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = borderColor.cgColor
        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height:1)
        view.layer.shadowRadius = shadowRadius
        view.layer.shadowOpacity = shadowOpacity
        view.layer.masksToBounds = masksToBounds

    }
    func makeShadowToView( _ view:UIView , shadowOpacity : Float , shadowRadius : CGFloat , shadowColor : UIColor , shadowOffset : CGSize! = CGSize(width: 0, height: 2.0)  ){
        
        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowOffset =  shadowOffset
        view.layer.shadowRadius = shadowRadius
        view.layer.shadowOpacity = shadowOpacity
        view.layer.masksToBounds = false
    }
}


func makeBorderToView( _ view:UIView , boderWidth : CGFloat , cornerRadius : CGFloat , borderColor : UIColor , maskToBounds : Bool  ){
    view.layer.borderWidth = boderWidth
    view.layer.cornerRadius = cornerRadius
    view.layer.borderColor =  borderColor.cgColor
    view.layer.masksToBounds = maskToBounds

}
