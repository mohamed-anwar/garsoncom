//
//  UIViewControllerExtensions.swift
//  
//
//

import UIKit

extension UIViewController {
    
    func showAlert(alertTitle: String, alertMsg: String, buttonTitle: String, action: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: alertTitle, message: alertMsg, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTitle, style: .default, handler: action)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    

    func showAlert(alertTitle : String, alertMsg : String, buttonTitles : [String], actions : [(UIAlertAction) -> ()]) {
        let alert = UIAlertController(title: alertTitle, message: alertMsg, preferredStyle: .alert)
        buttonTitles.enumerated().forEach({ index,title in
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        })
        present(alert, animated: true, completion: nil)
    }
    
    func getAlert(title: String, message: String) -> UIAlertController {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        let messageText = NSMutableAttributedString(
            string: title
//            attributes: [
//                NSAttributedStringKey.paragraphStyle,
//                NSAttributedStringKey.font.preferredFont(forTextStyle: UIFontTextStyle.headline),
//              NSAttributedStringKey.foregroundColor.black
//            ]
        )
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.setValue(messageText, forKey: "attributedTitle")
        return alert
    }
    
    func presentViewController(_ identifier : String) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: identifier)
        present(vc, animated: true, completion: nil)
    }
    func pushViewController( sender : UIViewController , identifier : String){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: identifier)
        sender.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func instantiateViewController(_ name : String, withIdentifier : String) -> UIViewController {
        
        return UIStoryboard(name: name, bundle: nil).instantiateViewController(withIdentifier: withIdentifier)
        
    }
    
    func getInstantiatedViewControllers(_ names : [String]) -> [UIViewController] {
        var viewControllers = [UIViewController]()
        for name in names {
            viewControllers.append(instantiateViewController("Main", withIdentifier: name))
        }
        return viewControllers
    }
    func changeAppLanguge(){
        
        
        let title = NSLocalizedString("To Change the app languge" , comment: "")
        let message = NSLocalizedString("it will need to exite and reopen it again in 1 second open your app", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OkayAlertAction = UIAlertAction(title: NSLocalizedString("I Agree", comment: "make agree to change languge"), style: .default, handler: {(action)-> Void in
            
            
            let arrayOfLang = UserDefaults.standard.object(forKey: "AppleLanguages") as! [String]
            if arrayOfLang.first == "ar"{
                UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
                
                
                
            }else{
                UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
                
            }
            print( UserDefaults.standard.object(forKey: "AppleLanguages") as! [String])
            
            
            //  UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
            //Actual settings of the language in the Application Settings
            UserDefaults.standard.synchronize()
            //Force the Settings to be saved and written to file since we will forcibly crash the app
            
            //crash the app after 500 milli seconds
            //time is a slight delay to make sure the file write has completed
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                exit(0)
            }
            
            
        })
        let CancelAlertAction = UIAlertAction(title: NSLocalizedString("I am not Agree", comment: "I am not agree to change languge"), style: .cancel, handler:nil)
        
        alertController.addAction(OkayAlertAction)
        alertController.addAction(CancelAlertAction)
        present(alertController, animated: true, completion: nil)
        
        
    }
}

