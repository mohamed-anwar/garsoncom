//
//  idintfers.swift
//  Foreera
//
//  Created by Mohamed on 9/8/17.
//  Copyright © 2017 Foreera. All rights reserved.
//

import Foundation
//   ============VC ID ==========

let RegisterNavID = "RegisterNavID"
let ChooseLocationNavID = "ChooseLocationNavID"
let RestTabBarId = "RestTabBarId"
let RestMenuID = "restMenuID"
let homeNavID = "homeNavID"
let HomeTableViewControllerID = "HomeTableViewControllerID"
let ProfileViewControllerID = "ProfileViewControllerID"
let OffersTableViewControllerID = "OffersTableViewControllerID"
let EditProfileViewControllerID = "EditProfileViewControllerID"
let MealDetailsViewControllerID = "MealDetailsViewControllerID"
let EditDetailMealViewControllerID    = "EditDetailMealViewControllerID"
let CartViewControllerID = "CartViewControllerID"
let ProcessOrderViewControllerID = "ProcessOrderViewControllerID"
let FavTabBarControllerID = "FavTabBarControllerID"
let MyOrderTabBarID = "MyOrderTabBarID"
let RequestedTableTableViewControllerID = "RequestedTableTableViewControllerID"
let ResturantListTableViewControllerID = "ResturantListTableViewControllerID"
let TableListViewControllerID = "TableListViewControllerID"
let ChooseTableTypeViewControllerID = "ChooseTableTypeViewControllerID"
let TableTimePickerViewControllerID = "TableTimePickerViewControllerID"
let AddPartyViewControllerID = "AddPartyViewControllerID"
let FilterTabBarID = "FilterTabBarID"
let ResturantCategoryTableViewControllerID = "ResturantCategoryTableViewControllerID"
let CommentTableViewControllerID = "CommentTableViewControllerID"
let SuggestMealTableViewControllerID = "SuggestMealTableViewControllerID"
//=================== Collection Cells Id ===========================
let categoryCellId = "categoryCellId"
let collectionCellId = "collectionTableCellID"
let restListCellId = "restListCellId"
let ResturantDetailMenuCellID = "ResturantDetailMenuCellID"
let HomeCollectionId = "HomeCollectionId"
let RestProductMenuCellID = "RestProductMenuCellID"
let RestDetailCellID = "RestDetailCellID"
let TableCellID = "tableCellID"
let ChooseTableID = "ChooseTableID"
let AddReviewID = "AddReviewID"
let ProfileTableViewCellID = "ProfileTableViewCellID"
let LogOutTableViewCellID = "LogOutTableViewCellID"
let BestClientTableViewCellID = "BestClientTableViewCellID"
let OffersTableViewCellID = "OffersTableViewCellID"
let SelectSizeTableViewCellID = "SelectSizeTableViewCellID"
let CartTableViewCellID = "CartTableViewCellID"
let SelectPaymentTableViewCellID = "SelectPaymentTableViewCellID"
let FavMealTableViewCellID = "FavMealTableViewCellID"
let FavResturantCollectionViewCellID = "FavResturantCollectionViewCellID"
let MyTableTableViewCellID = "MyTableTableViewCellID"
let PartiesTableViewCellID = "PartiesTableViewCellID"
let LocationCellID = "LocationCellID"
//=============================ResponseState===========================================
struct ResponseState {
    static   let defualt = "defualt"
    static  let notSearchedYet = "notnotSearchedYet"
    static   let loading = "loading"
    static   let noResults = "noResults"
    static   let  result = "result"
    static   let  error = "error"
    
}
//------------------------\-------------------------
struct TableViewCelldentifiers {
    static let loadingCell = "LoadingTableViewCell"
    static let defualt = "DefualtTableViewCell"
}
