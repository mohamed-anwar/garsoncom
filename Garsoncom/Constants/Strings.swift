//
//  Strings.swift
//  Foreera
//
//  Created by Mohamed on 9/9/17.
//  Copyright © 2017 Foreera. All rights reserved.
//

import Foundation
let askForLocationString = NSLocalizedString("Do You Want To Turn On Your GPS", comment: "")
let askForDeleteString = NSLocalizedString("Are You Sure to delete?", comment: "")
let askForDeleteCartString = NSLocalizedString("Are You Sure to Empty cart?", comment: "")


let askGoTOSettingString = NSLocalizedString("To Turn On GPS You Need To Go To Settings", comment: "")

let aletAskString = NSLocalizedString("Ask", comment: "")

let OkayButtonTitle = NSLocalizedString("Okay", comment: "")
let yesButtonTitle = NSLocalizedString("Yes", comment: "")
let MoveToCartButtonTitle = NSLocalizedString("Move To Cart", comment: "")
let currentlyUnavilable = NSLocalizedString("Currently unavilable", comment: "")
let closedNow = NSLocalizedString("Closed now", comment: "")








let SuccessTitle = NSLocalizedString("Success", comment: "")
let ChooseCityButtonTitle = NSLocalizedString("Choose City", comment: "")

let CancelButtonTitle = NSLocalizedString("Cancel", comment: "")
let SettingsTitle = NSLocalizedString("Settings", comment: "")
let EGP = NSLocalizedString("EGP", comment: "")
let RestaurantMenuString = NSLocalizedString("Menu", comment: "")
let MenuString = NSLocalizedString("Menu", comment: "")
let MainbranchString = NSLocalizedString("Main branch :", comment: "")
let DatePickerTitleString = NSLocalizedString("Choose Date:", comment: "")
let ErrorAlertString = NSLocalizedString("Error", comment: "")
let NoAddationString = NSLocalizedString("This Meal has no addtions.", comment: "")

let PleaseFillInMsgString = NSLocalizedString("Please Fill In All Requierd Fields", comment: "")
let PleaseSelectTableMsgString = NSLocalizedString("Please Select one table at least", comment: "")
let PleaseSelectDateMsgString = NSLocalizedString("Please Choose Date", comment: "")
let PleaseSelectCityMsgString = NSLocalizedString("Please Choose City", comment: "")
let TableNumString = NSLocalizedString("Table Num : ", comment: "")
let GuestsAllowedString = NSLocalizedString("Guests allowed : ", comment: "")
var titlesOfSlideMenu = [
    NSLocalizedString("Home", comment: "") ,
    NSLocalizedString("Profile", comment: "") ,
    NSLocalizedString("Restaurant List", comment: "") ,
    NSLocalizedString("Tables List", comment: "") ,
    NSLocalizedString("My Tables", comment: "") ,
    NSLocalizedString("Offers", comment: ""),
    NSLocalizedString("Suggested Meals", comment: ""),
    NSLocalizedString("Parties", comment: ""),
    NSLocalizedString("My Order", comment: ""),
    NSLocalizedString("Favourite", comment: ""),
    NSLocalizedString("Best Clients", comment: ""),
    NSLocalizedString("Language", comment: ""),
    NSLocalizedString("Information", comment: ""),
    NSLocalizedString("Logout", comment: "")
]
let buttonArray = [
    OkayButtonTitle,
    CancelButtonTitle
]
let titlesOfSections = [
NSLocalizedString("Resturants", comment: "") ,
NSLocalizedString("Categories", comment: "") ,
NSLocalizedString("", comment: "") ,
NSLocalizedString("Most Rate", comment: "") ,
NSLocalizedString("Most Product Rate", comment: "")
]



