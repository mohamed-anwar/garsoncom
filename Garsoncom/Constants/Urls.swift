//
//  Urls.swift
//  Foreera
//
//  Created by Mohamed on 9/7/17.
//  Copyright © 2017 Foreera. All rights reserved.
//

import Foundation

let baseUrl = "https://develop.garsoncom.com"
let mainUrl = baseUrl + "/api/v1/"
let photoUrl = baseUrl 
let registerUrl = mainUrl + "register"
let loginUrl = mainUrl + "login"
let facebookLoginUrl = mainUrl + "facebook"
let categoriesUrl = mainUrl + "categories"
let category_productsUrl = mainUrl + "category_products"
let governoratesUrl = mainUrl + "governorates"
let citiesUrl = mainUrl + "governorate_cities"
let homeUrl = mainUrl + "home"
let restaurantsUrl = mainUrl + "restaurants"
let getRestaurantCategoryURL = mainUrl + "restaurant_categories"
let getProductsOfCategoryURL = mainUrl + "category_products_for_restaurant"
let GetResturantReviewsURL = mainUrl + "reviews?restaurant_id="
let GetTablesURL = mainUrl + "tables"
let AddTableReservationURL = mainUrl + "add_table_reservation"
let ProfileUrl = mainUrl + "profile"
let BestClientUrl = mainUrl + "best_client"
let OffersUrl = mainUrl + "offers"
let PaymentMethodUrl = mainUrl + "payment_methods"
let AddOrderUrl = mainUrl + "add_order_ios"
let AddFavUrl = mainUrl + "favourite"
let GetFavouratRestirantsUrl = mainUrl + "restaurant_favourite"
let GetFavMealURL = mainUrl +  "product_favourite"
let MyTablesUrl = mainUrl + "my_table_reservations"
let cancelTableUrl = mainUrl + "cancel_table"
let ResturantsUrl = mainUrl + "restaurants"
let GetPartyTypeUrl = mainUrl + "party_types"
let AddPartyUrl = mainUrl + "add_party"
let GetMyOrdersUrl = mainUrl + "my_orders"
let CancelOrderUrl = mainUrl + "cancel_order"
let AddPostUrl = mainUrl + "add_post"
let GetAllPostsUrl = mainUrl + "all_posts"
let VoteUrl = mainUrl + "add_vote"
let DeletePostUrl = mainUrl + "delete_post"
let ShowPostURL = mainUrl + "show_post"
let DeleteCommentURL = mainUrl + "delete_comment"
let AddCommentUrl = mainUrl + "add_comment"
let RateUrl = mainUrl + "rate"
let AddReviewUrl = mainUrl + "review"



