//
//  ParametersKeys.swift
//  Foreera
//
//  Created by Mohamed on 9/9/17.
//  Copyright © 2017 Foreera. All rights reserved.
//

import Foundation
 let acceptLanguage = "Accept-Language"
struct StatusMsgs{
    static let error = "error"
    static let sucess = "done"
    static let dataUpdated = "Update successful"

}

struct SignUpKeys {
    static let name = "name"
    static let phone = "phone"
    static let address = "address"
    static let img = "img"
    static let lat = "lat"
    static let lng = "lng"
    static let city_id = "city_id"
    static let email = "email"
    static let password = "password"
}

struct LoginKeys {
    static let email = "email"
    static let password = "password"
}
struct SocialLoginKeys {
    static let deviceId = "deviceId"
    static let deviceName = "deviceName"
    static let userid = "userid"
    static let authToken = "authToken"
}

