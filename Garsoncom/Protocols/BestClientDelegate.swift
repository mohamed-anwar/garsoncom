//
//  BestClientDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/17/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


import Foundation
protocol BestClientDelegate : class {
    func bestClient( DidFinshDownload Sender : [BestClient])
    func bestClient(didFaild error :NSError?)
    func bestClient(didGetUnexpectedResponse: String)
}
