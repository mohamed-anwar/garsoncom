//
//  ResturantDetailsProtocol.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/25/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol ResturantDetaislDelegate : class {
    func ResturantDetail(didFinishDownload Reviews : [ReviewModel]?)
    func ResturantDetail(didFinishDownload review : ReviewModel?, msg: String)

    func ResturantDetail(didFaild error :NSError?)
    func ResturantDetail(didGetUnexpectedResponse: String)
    func ResturantDetail(Success msg: String)

}
