//
//  ProfileDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/16/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol ProfileDelegate : class {
    func Profile( DidFinshDownload Sender : UserModel , msg : String)
    func Profile(didFaild error :NSError?)
    func Profile(didGetUnexpectedResponse: String)
}
