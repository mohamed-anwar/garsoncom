//
//  FavMealDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/28/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
protocol FavMealDelegate : class {
    func  FavMeal(DidFinshDownload Sender : [ProductModel])
    func  FavMeal(didFaild error :NSError?)
    func  FavMeal(didGetUnexpectedResponse: String)
    func  FavMeal(Success msg: String)
    
}
