//
//  CommentDelelgate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation

protocol CommentDelelgate : class {
    func   Comment(didDownloadData comments : [CommentModel])
    func   Comment(didDeletePost msg : String)
    func   Comment(didDeleteComment msg : String)
    func   Comment(didAddComment msg : String , comment : CommentModel)
    func   Comment(didFaild error :Error?)
    func   Comment(didGetUnexpectedResponse: String)
}
