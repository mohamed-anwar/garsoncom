//
//  VerifyDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/9/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol VerifyDelegate : class  {
    func   Verify(didFaild error :Error?)
    func   Verify(didGetUnexpectedResponse: String)
    func   Verify(Success msg: String)
}
