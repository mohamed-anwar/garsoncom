//
//  ChooseTableTypeDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/1/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol ChooseTableTypeDelegate : class {
    func ChooseTableType(type : Bool)
}
