//
//  CategoryOfCollectionViewDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol CategoryDelegate : class {
    func Category(didFinishDownload Products : [ProductModel]?)
    func Category(didFaild error :NSError?)
    func Category(didGetUnexpectedResponse: String)
}
