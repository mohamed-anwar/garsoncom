//
//  SuggestMealDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol SuggestMealDelegate : class {
    func   SuggestMeal(didaAddPost msg : String , post : Post)
    func   SuggestMeal(didVote msg : String)
    func   SuggestMeal(didDeletePost msg : String)

    func   SuggestMeal(DidFinshDownload Sender : [Post])
    func   SuggestMeal(didFaild error :Error?)
    func   SuggestMeal(didGetUnexpectedResponse: String)
}
