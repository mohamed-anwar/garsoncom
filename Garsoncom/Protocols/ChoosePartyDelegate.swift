//
//  ChoosePartyDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/8/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol ChoosePartyDelegate : class {
    func   ChooseParty(DidFinshDownload Sender : [PartyType])
    func   ChooseParty(DidFinshDownload Sender : [CategoryModel])
    func   ChooseParty(DidFinshDownload Sender : [ResturantModel])
    func   ChooseParty(didFaild error :NSError?)
    func   ChooseParty(didGetUnexpectedResponse: String)
    func   ChooseParty(Success msg: String)
    func   didTapPartyType(Sender : Int)
    func   didTapPartyPlace(Sender : Bool)

}
