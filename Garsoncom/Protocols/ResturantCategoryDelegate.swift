//
//  ResturantDetailDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol ResturantCategoryDelegate : class {
    func ResturantDetail(didFinishDownload Categories : [CategoryModel]?)
    func ResturantDetail(didFaild error :NSError?)
    func ResturantDetail(didGetUnexpectedResponse: String)
}
