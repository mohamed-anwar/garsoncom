//
//  CollapsibleTableViewHeaderDelegate.swift
//  CollapsableTableviewCells
//
//  Created by Vortex on 11/9/17.
//  Copyright © 2017 Vortex. All rights reserved.
//

import Foundation

protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(_ header: RequestedOrderCollapibleHeader, section: Int)
    func toggleCancel(section: Int)

}
