//
//  LoginDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/5/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol LoginDelegate : class {
    func Login( DidFinshDownload Sender : UserModel , msg : String)
    func Login(didFaild error :NSError?)
    func Login(didGetUnexpectedResponse: String)
}
