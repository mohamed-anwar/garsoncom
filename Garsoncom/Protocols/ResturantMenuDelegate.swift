//
//  ResturantMenuDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol MenuDelegate : class {
    func Menu(didFinishDownload Products : [ProductModel]?)
    func  MakeMealFav(DidFinshDownload Sender : String)
    func Menu(didFaild error :NSError?)
    func Menu(didGetUnexpectedResponse: String)
}
