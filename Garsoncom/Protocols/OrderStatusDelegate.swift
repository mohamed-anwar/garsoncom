//
//  OrderStatusDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol OrderStatusDelegate : class {
    func   OrderStatus(DidFinshDownload Sender : [OrderHeader])
    func   OrderStatus(didFaild error :Error?)
    func   OrderStatus(didGetUnexpectedResponse: String)
    func    OrderStatus(Success msg: String)
}
