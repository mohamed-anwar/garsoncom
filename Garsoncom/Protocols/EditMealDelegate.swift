//
//  EditMealDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/25/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

protocol EditMealDelegate:class {
    func didEditMeal(Product : ProductModel)
}
