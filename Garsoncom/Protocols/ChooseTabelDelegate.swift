//
//  ChooseTabelDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol ChooseTabelDelegate : class {
    func ChooseTabelDelegate(didFinishDownload msg : String?)
    func ChooseTabelDelegate(didFaild error :NSError?)
    func ChooseTabelDelegate(didGetUnexpectedResponse: String)
}
