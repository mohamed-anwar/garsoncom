//
//  SignUpDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/7/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


protocol SignUpDelegate : class {
    func SignUp( DidFinshDownload Sender : UserModel , msg : String)
    func SignUp(didFaild error :Error?)
    func SignUp(didGetUnexpectedResponse: String)
}
