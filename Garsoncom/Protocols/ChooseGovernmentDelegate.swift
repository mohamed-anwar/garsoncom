//
//  LocationDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/9/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol ChooseGovernmentDelegate : class {
    
    func GetGovernment(didFinishDownload Sender : [GovernmentModel]?)
    func GetGovernmentCities(didFinishDownload Sender : [CityModel]?)

    func GetGovernment(didFaild error :Error?)
    
    
    func GetGovernment(didGetUnexpectedResponse: String)


}
