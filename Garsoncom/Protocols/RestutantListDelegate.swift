//
//  RestutantListDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/30/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol RestutantListDelegate : class {
    func   RestutantList(DidFinshDownload Sender : [ResturantModel])
    func   RestutantList(didFaild error :NSError?)
    func   RestutantList(didGetUnexpectedResponse: String)
    func   RestutantList(Success msg: String)
}
