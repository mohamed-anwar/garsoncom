//
//  RateProductProtocol.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/14/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol RateProductProtocol : class  {
    func   RateProduct(didFaild error :Error?)
    func   RateProduct(didGetUnexpectedResponse: String)
    func   RateProduct(Success msg: String)
}
