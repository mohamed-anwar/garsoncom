//
//  addPostDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//
import  Foundation
protocol AddPostDelegate : class {
    func didAddPost(mealName : String , post : String)
}
