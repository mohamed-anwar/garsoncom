//
//  HomeProtocol.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/18/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
protocol HomeDelegate : class {
    func Home(didFinishDownload Resturants : [ResturantModel]? , ResturantsRates : [ResturantModel]? , Products : [ProductModel]?)
    func Home(didFinishDownload Categories : [CategoryModel]?)
    func Home(didFinishDownload Products : [ProductModel]?)
    func Home(didFaild error :Error?)
    func Home(didGetUnexpectedResponse: String)
}

