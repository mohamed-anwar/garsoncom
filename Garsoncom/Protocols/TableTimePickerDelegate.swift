//
//  TimePickerDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/2/18.
//  Copyright © 2018 Rivers. All rights reserved.

import Foundation
protocol TableTimePickerDelegate : class {
    func TableTimePicker(didFinishDownload Tables : [TablesModel]?)
    func TableTimePicker(didFaild error :NSError?)
    func TableTimePicker(didGetUnexpectedResponse: String)
}
