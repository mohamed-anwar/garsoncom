//
//  TapDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
protocol TapDelegate : class {
    func Tap(DidTabOfResturant Resturant : ResturantModel)
    func Tap(DidTapPrudct Product : ProductModel)
}

