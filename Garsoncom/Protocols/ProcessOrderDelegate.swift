//
//  ProcessOrderDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/26/18.
//  Copyright © 2018 Rivers. All rights reserved.
//
import UIKit

protocol ProcessOrderDelegate : class {
    func  paymanet( DidFinshDownload Sender : [PaymentMethod])
    func  paymanet(didFaild error :NSError?)
    func  paymanet(didGetUnexpectedResponse: String)
    func order(addSucessfully msg:String)
}
