//
//  SignUpVC.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/5/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftMessages
class SignUpViewController: UIViewController {
    var inputArray : [SkyFloatingLabelTextField]!
    var user  : UserModel?
    var imagePickerController: UIImagePickerController!
    var mainImage : NSData!
    var pimage : UIImage!
    weak var delegate : SignUpDelegate!
    var backEnd : SignUpBackEndManager!
    var parameter = [String : Any]()
    
    @IBOutlet var mainV: SingUp!
    override func viewDidLoad() {
        super.viewDidLoad()
        user = UserModel()
        backEnd = SignUpBackEndManager()
        backEnd.delegate = self
        makeBorderToView(mainV.signUpButton, boderWidth: 1, cornerRadius: 15, borderColor: .clear, maskToBounds: true)
        makeBorderToView(mainV.pickUpImageButton, boderWidth: 1, cornerRadius: mainV.pickUpImageButton.frame.width / 2, borderColor: .clear, maskToBounds: true)
        inputArray = [mainV.emailTxtF,mainV.passwordTxtF , mainV.phoneNumberTxtF , mainV.computerNameTxtF]
        for input in inputArray {
            input.delegate = mainV
        }
    }
    @IBAction func PickupImage(_ sender: UIButton) {
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        self.present(imagePickerController, animated: true, completion: nil)
    }
    @IBAction func Submit(_ sender: UIButton) {
        if Validator.validateInputs(inputs: inputArray) {
            parameter[SignUpKeys.name] = mainV.computerNameTxtF.text!
            parameter[SignUpKeys.password] = mainV.passwordTxtF.text!
            parameter[SignUpKeys.email] = mainV.emailTxtF.text!
            parameter[SignUpKeys.phone] = mainV.phoneNumberTxtF.text!
            if cityID != 0{
                parameter["city_id"] =  cityID!
            }else{
                parameter["lat"] =  lat!
                parameter["lng"] =  lng!
            }
            backEnd.SignUpRequest(parameters: parameter)
            mainV.signUpButton.showLoader()
        }
    }
}
extension SignUpViewController : SignUpDelegate{
    func SignUp(didFaild error: Error?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

    }
    
    func SignUp(didGetUnexpectedResponse: String) {
        mainV.signUpButton.hideLoader()
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)


    }
    func SignUp(DidFinshDownload Sender: UserModel , msg : String) {
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        presentViewController(homeNavID)

        mainV.signUpButton.hideLoader()
    }

    
    
}
extension SignUpViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            mainV.pickUpImageButton.setImage(pickedImage, for: .normal)
            pimage = pickedImage
        }else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            mainV.pickUpImageButton.setImage(image, for: .normal)
            pimage = image
            
        }
        mainImage =  pimage.ResizeImage(targetSize: CGSize(width: 800.0, height: 800.0)) as NSData!
        parameter[SignUpKeys.img] = mainImage
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
