//
//  SignUpBackEnfManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/7/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

//
//  LoginBackEndManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/5/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
class SignUpBackEndManager {
    private var network : Networking!
    weak var delegate : SignUpDelegate?
    init() {
        network = Networking(requestTimeout: 10)
    }
    func SignUpRequest(parameters : [String:Any]) {
       
        network.manager.request(registerUrl, method : .post , parameters: parameters, headers : [ acceptLanguage :  setLang() ]).responseJSON {[weak self] response in
            guard let  strongSelf = self else{return}
            let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                if json["status"].string! == StatusMsgs.sucess{
                if let data = json["data"].dictionaryObject{
                    let userModel = UserModel(map: Map(mappingType: .fromJSON, JSON: data))
                    strongSelf.delegate?.SignUp(DidFinshDownload: userModel!, msg : json["message"].string! )
                } else {
                    strongSelf.delegate?.SignUp(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                    }
                }else{
                    strongSelf.delegate?.SignUp(didGetUnexpectedResponse: json["message"].string!)
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.SignUp(didFaild: error as NSError)
                break
            }
        }
    }
    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
