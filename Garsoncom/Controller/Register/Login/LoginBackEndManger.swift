//
//  LoginBackEndManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/5/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
import RealmSwift
class LoginBackEndManager {
    private var network : Networking!
    weak var delegate : LoginDelegate?
    init() {
        network = Networking(requestTimeout: 10)
    }
    func LoginRequest(email : String , password :String) {
        
        let parameters : [ String: String ] = [
            LoginKeys.email : email ,
            LoginKeys.password : password
        ]
        network.manager.request(loginUrl,method : .post , parameters: parameters,headers : [ acceptLanguage :  setLang() ]).responseJSON {[weak self] response in
            guard let  strongSelf = self else{return}
            let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                if json["status"].string! == StatusMsgs.sucess{
                    if let data = json["data"].dictionaryObject{
                        let userModel = UserModel(map: Map(mappingType: .fromJSON, JSON: data))
                        userLoginToken = userModel?._apiToken
                        strongSelf.delegate?.Login(DidFinshDownload: userModel! , msg : json["message"].string!)
                    } else {
                        strongSelf.delegate?.Login(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                    }
                }else{
                    strongSelf.delegate?.Login(didGetUnexpectedResponse: json["message"].string!)
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.Login(didFaild: error as NSError)
                break
                
            }
        }
    }
    
    
    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
