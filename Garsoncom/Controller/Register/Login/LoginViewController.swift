//
//  LoginViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/5/18.
//  Copyright © 2018 Rivers. All rights reserved.
//
import SwiftMessages
import SideMenu
import UIKit
import SkyFloatingLabelTextField
class LoginViewController: UIViewController {
    var inputArray : [SkyFloatingLabelTextField]!
    var user  : UserModel?
    weak var delegate : LoginDelegate!
    var backEnd  : LoginBackEndManager!
    @IBOutlet var mainV: Login!
    override func viewDidLoad() {
        super.viewDidLoad()
        backEnd = LoginBackEndManager()
        backEnd.delegate = self
        user = UserModel()
        makeBorderToView(mainV.loginButton, boderWidth: 1, cornerRadius: 15, borderColor: .clear, maskToBounds: true)
        makeBorderToView(mainV.signUpButton, boderWidth: 1, cornerRadius: 15, borderColor: .clear, maskToBounds: true)
        
        inputArray = [mainV.emailTxtF,mainV.passwordTxtF]
        for input in inputArray {
            input.delegate = mainV
        }
    }

    @IBAction func Submit(_ sender: UIButton) {
        if Validator.validateInputs(inputs: inputArray) {
            backEnd.LoginRequest(email: mainV.emailTxtF.text!, password: mainV.passwordTxtF.text!)
            mainV.loginButton.showLoader()
        }
    }
    

}
extension LoginViewController : LoginDelegate{
  
    func Login(DidFinshDownload Sender: UserModel, msg: String) {
        dismiss(animated: true, completion: nil)
        
        
    }
    
    func Login(didFaild error: NSError?) {
        mainV.loginButton.hideLoader()
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
    
    func Login(didGetUnexpectedResponse: String) {
        mainV.loginButton.hideLoader()
               SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
    
    
    
}
