//
//  HomeTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/11/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SideMenu
import SwiftMessages


class HomeTableViewController: UITableViewController    {
    var dataManger : HomeBackEndManger!
    var data : (resturants : [ResturantModel],resturantsRates : [ResturantModel], products : [ProductModel]) = ([],[],[])
    var categories = [CategoryModel]()
    var products = [ProductModel]()
    @IBAction func slideMenuBarButton(_ sender: UIBarButtonItem) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    var refreshTVControl: UIRefreshControl!
    @objc func refresh(sender:AnyObject) {
        dataManger.downloadAllData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         let order  = Array(realm.objects(Order.self))
         if order.count > 0 {
            
            let actions : [((UIAlertAction) -> ())] = [
            { [weak self] _ in
                            let vc = self?.storyboard?.instantiateViewController(withIdentifier: CartViewControllerID) as! CartViewController
                self?.navigationController?.pushViewController(vc, animated: true)
                },{ _ in
                    try! realm.write {
                        realm.deleteAll()
                        print("deleted")

                    }
                    
                }
            ]
            
            showAlert(alertTitle: aletAskString, alertMsg: askForDeleteCartString, buttonTitles: [MoveToCartButtonTitle,yesButtonTitle], actions: actions)
//            let vc = storyboard?.instantiateViewController(withIdentifier: CartViewControllerID) as! CartViewController
//            let restId = String(restaurantId)
//            vc.order.rest_id = restId
//            
//            navigationController?.pushViewController(vc, animated: true)
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setSlideMenu(sender: self)
        dataManger = HomeBackEndManger()
        dataManger.delegate = self
        dataManger.delegate2 = self
        dataManger.downloadAllData()
        
        refreshTVControl = UIRefreshControl()
        refreshTVControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "Pull to refresh"))
        refreshTVControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        
        tableView.refreshControl = refreshTVControl
    }
    
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1{
            return tableView.frame.height / 12
        }else if indexPath.section == 4 || indexPath.section == 2 {
            return 480
        }else{
//            return tableView.frame.height / 5
            return 240
        }
    }
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.backgroundColor = .white
        if let textlabel = header.textLabel {
            textlabel.font = textlabel.font.withSize(15)
            textlabel.textColor = UIColor(red: 61/255, green: 178/255, blue: 90/255, alpha: 1)
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return titlesOfSections[section]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: collectionCellId, for: indexPath) as! CollectionTableViewCell
        cell.delegate = self
        switch indexPath.section {
        case 0 :
            cell.collectionViewitemSize = CGSize(width: tableView.frame.width / 1.2 , height: 210)
            cell.scrollDirection = .horizontal
            cell.isProductCategoryCollectionView = false
            cell.delegate = self
            cell.configure(resturants: data.resturants)
            print(data.resturants)


            
        case 1 :
            cell.collectionViewitemSize = CGSize(width: tableView.frame.width / 4 , height: tableView.frame.height / 12)
            cell.scrollDirection = .horizontal
            cell.isProductCategoryCollectionView = false
            cell.dataManger.delegate2 = self
            cell.delegate = self
            cell.configure(categories: categories)


            
            
            
        case 2:
            cell.scrollDirection = .vertical
            cell.collectionViewitemSize = CGSize(width: tableView.frame.width / 2.2 , height: 240)
            cell.isProductCategoryCollectionView = true
            cell.delegate = self
            cell.configureProducts(productsByCategoy: products)


            
        case 3:
            cell.scrollDirection = .horizontal
            cell.collectionViewitemSize = CGSize(width: tableView.frame.width / 1.2 , height: 240)
            cell.isProductCategoryCollectionView = false
            cell.delegate = self
            cell.configure(resturants: data.resturantsRates)



            
            
        case 4:
            cell.scrollDirection = .vertical
            cell.collectionViewitemSize = CGSize(width: tableView.frame.width / 2.2 , height: 260)
            cell.isProductCategoryCollectionView = false
            cell.delegate = self
            cell.configure(products: data.products)



            
            
        default :
            break
        }
        return cell
    }
    
    
    
}
extension HomeTableViewController : HomeDelegate{
    func Home(didFinishDownload Products: [ProductModel]?) {
        products = Products!
        
        
    }
    
    func Home(didFinishDownload Categories: [CategoryModel]?) {
        categories = Categories!
        
    }
    
    func Home(didFinishDownload Resturants: [ResturantModel]?, ResturantsRates: [ResturantModel]?, Products: [ProductModel]?) {
        data = (Resturants!,ResturantsRates!,Products!)
        tableView.reloadData()
        refreshTVControl.endRefreshing()
    }
    
    func Home(didFaild error: Error?) {
        refreshTVControl.endRefreshing()
        
        SwiftMessages.showMessage(title: "", body:(error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
    
    func Home(didGetUnexpectedResponse: String) {
        refreshTVControl.endRefreshing()
        
        SwiftMessages.showMessage(title: "", body:didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
        
    }
    
    
}
extension HomeTableViewController : CategoryDelegate{
    func Category(didFinishDownload Products: [ProductModel]?) {
        products = Products!
        tableView.reloadSections(IndexSet(integer:2), with: .automatic)
    }
    
    func Category(didFaild error: NSError?) {
        refreshTVControl.endRefreshing()
        
        SwiftMessages.showMessage(title: "", body:(error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
    
    func Category(didGetUnexpectedResponse: String) {
        refreshTVControl.endRefreshing()
        
        SwiftMessages.showMessage(title: "", body:didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
    
    
    
}
extension HomeTableViewController : TapDelegate{
    func Tap(DidTapPrudct Product: ProductModel) {
        let vc = storyboard?.instantiateViewController(withIdentifier: MealDetailsViewControllerID) as! MealDetailsViewController
        vc.product = Product
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func Tap(DidTabOfResturant Resturant : ResturantModel) {
        //        if Resturant._active == false{
        
        //        let tab = storyboard?.instantiateViewController(withIdentifier: RestTabBarId) as! UITabBarController
        //        let resturantCategotyTB = tab.viewControllers![0] as! ResturantCategoryTableViewController
        //        resturantCategotyTB.id = Resturant._id
        //        tab.navigationItem.title = Resturant._name
        //        let restDVC = tab.viewControllers![1] as! ResturantDetalsViewController
        //            restDVC.resturant = Resturant
        //        let table = tab.viewControllers![2] as! TableTimePickerViewController
        //        table.paramter["restaurant_id"] = Resturant._id
        //        if !Resturant._haveTables{
        //            tab.viewControllers?.remove(at: 2)
        //        }
        //            navigationController?.pushViewController(tab, animated: true)
        let restDVC = storyboard?.instantiateViewController(withIdentifier: RestTabBarId) as! ResturantDetalsViewController
        restDVC.resturant = Resturant
        navigationController?.pushViewController(restDVC, animated: true)
        
        //        }
    }
    
    
    
    
    
}

