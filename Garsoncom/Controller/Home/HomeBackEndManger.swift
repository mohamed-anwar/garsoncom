//
//  HomeBackEndManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/18/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import SwiftyJSON
import ObjectMapper


 class HomeBackEndManger {
    
    private var network : Networking!
    weak var delegate  : HomeDelegate?
    weak var delegate2  : CategoryDelegate?
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    func downloadAllData(){
        DownloadHomeData()
        DownloadCategoryData()
//        DownloadPrudcutCategoryData()
        
    }
    
    func DownloadHomeData() {
        var resturantModels = [ResturantModel]()
        var restaurantsRates = [ResturantModel]()
        var products = [ProductModel]()
        var parameters = [String : Any]()
        if cityID != 0{
            parameters["city_id"] =  cityID!
        }else{
            parameters["lat"] =  lat!
            parameters["lng"] =  lng!
        }
        if (userLoginToken != nil){
            parameters["api_token"] = userLoginToken!

        }
 
        
        network.manager.request( homeUrl ,method : .post ,parameters: parameters, headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else { return }

            switch result {
                
            case .success(let value):
                let json = JSON(value)
                print(json)
                let contentArray = json["data"].dictionaryValue
                if let resturants = contentArray["restaurants"]?.arrayValue, let resturantRates = contentArray["restaurants_rate"]?.arrayValue, let productsRate = contentArray["products_rate"]?.arrayValue {
                    for resturant in resturants {
                        if let dict = resturant.dictionaryObject {
                            let model = ResturantModel()
                            model.mapping(map: Map(mappingType: .fromJSON, JSON: dict))
                            resturantModels.append(model)
                        }
                    }
                    
                    for rate in resturantRates {
                        if let dict = rate.dictionaryObject {
                            let model = ResturantModel()
                            model.mapping(map: Map(mappingType: .fromJSON, JSON: dict))
                            restaurantsRates.append(model)
                        }
                    }
                    for product in productsRate {
                        if let dict = product.dictionaryObject {
                            let model = ProductModel()
                            model.mapping(map: Map(mappingType: .fromJSON, JSON: dict))
                            products.append(model)
                        }
                    }
                strongSelf.delegate?.Home(didFinishDownload: resturantModels, ResturantsRates: restaurantsRates, Products: products)

                } else {
            strongSelf.delegate?.Home(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
        }
                break
            case .failure(let error) :
                strongSelf.delegate?.Home(didFaild: error)
                break
                
            }
        }
    }
    
    func DownloadCategoryData() {
        var categoriesModels = [CategoryModel]()
     
        
        network.manager.request( categoriesUrl , headers : [ acceptLanguage : setLang() ]).responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else { return }

            switch result {
                
            case .success(let value):
                let json = JSON(value)

                    if let contentArray = json["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                            let model = CategoryModel()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                            categoriesModels.append(model)
                        }
                    strongSelf.DownloadPrudcutCategoryData(id: categoriesModels[0]._id)
                        categoriesModels[0]._selected = true
                    strongSelf.delegate?.Home(didFinishDownload: categoriesModels)
                    
                } else {
                    strongSelf.delegate?.Home(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.Home(didFaild: error )
                break
                
            }
        }
    }
    func DownloadPrudcutCategoryData(id : Int = 1) {
        var productssModels = [ProductModel]()
        var parameters = [String : Any]()
            parameters["id"] = id
        if cityID != 0{
            parameters["city_id"] =  cityID!
        }else{
            parameters["lat"] =  lat!
            parameters["lng"] =  lng!
        }
        network.manager.request( category_productsUrl ,method : .post ,parameters: parameters, headers : [ acceptLanguage : setLang() ]).debugLog().responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else { return }
            switch result {
            case .success(let value):
                let json = JSON(value)
                if let productss = json["data"].array{
                    for resturant in productss {
                        if let dict = resturant.dictionaryObject {
                        let model = ProductModel()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: dict))
                        productssModels.append(model)
                        }
                        
                    }
//                    self.delegate?.Home(didFinishDownload: productssModels)
                    strongSelf.delegate2?.Category(didFinishDownload: productssModels)

                    
                }else{
                    strongSelf.delegate?.Home(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.Home(didFaild: error )
                break
                
            }
        }
    }
    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}

