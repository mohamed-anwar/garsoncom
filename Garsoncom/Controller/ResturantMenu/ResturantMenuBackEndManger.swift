//
//  ResturantMenuBackEndManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
class ResturantMenuBackEndManger {
    
    private var network : Networking!
    weak var delegate  : MenuDelegate?
    
    
    init() {
        network = Networking(requestTimeout: 10)
    }
   
    
    func DownloadData(categoryId : Int ,  restaurantId : Int) {
        var parameters : [String : Any] = [
            "category_id" : categoryId ,
            "restaurant_id" : restaurantId
        ]
        if userLoginToken != nil {
        parameters["api_token"] = userLoginToken!
        }
        network.manager.request( getProductsOfCategoryURL ,method : .post, parameters :  parameters , headers : [ acceptLanguage :  setLang() ]).responseJSON {[weak self]response in
            guard let strongSelf = self else{return}

            var productsModels = [ProductModel]()
            let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                    let contentArray = json["data"].dictionaryValue
                    if let productss = contentArray["data"]?.arrayValue{
                        for resturant in productss {
                            if let dict = resturant.dictionaryObject {
                                let model = ProductModel()
                                model.mapping(map: Map(mappingType: .fromJSON, JSON: dict))
                                productsModels.append(model)
                            }
                        }
                    strongSelf.delegate?.Menu(didFinishDownload: productsModels)
                    
                } else {
                    strongSelf.delegate?.Menu(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.Menu(didFaild: error as NSError)
                break
                
            }
        }
    }
    func addOrRemoveFavRequest(id : Int) {
        let parameters : [String : Any] = [
            "id" : id ,
            "type" : "product" ,
            "api_token" : userLoginToken!
        ]
        network.manager.request( AddFavUrl ,method : .post, parameters :  parameters , headers : [ acceptLanguage :  setLang() ]).responseJSON {[weak self]response in
            guard let strongSelf = self else{return}
            let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                if json["status"].string! == StatusMsgs.sucess{
                    strongSelf.delegate?.MakeMealFav(DidFinshDownload: json["message"].string!)
                }else{
                    strongSelf.delegate?.Menu(didGetUnexpectedResponse: json["message"].string!)
                }
                break
                
            case .failure(let error) :
                strongSelf.delegate?.Menu(didFaild: error as NSError)
                break
                
            }
        }
    }
    
    
    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
