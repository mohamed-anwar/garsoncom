//
//  ResturantMenuVC+ExTableViewDelegate.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
extension ResturantMenuVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RestProductMenuCellID, for: indexPath) as! ResturantMenuTableViewCell

        cell.configure(name: products[indexPath.row]._name, desc: products[indexPath.row]._description, rate: products[indexPath.row]._rate, sizes: products[indexPath.row]._sizes, fav: products[indexPath.row]._isFavourite, image: products[indexPath.row]._image, isLogin: userLoginToken == nil ? false : true)
        cell.favBut.addTarget(self, action: #selector(makeFav(sender:)), for: .touchUpInside)
        return cell
    }
    

@objc func makeFav(sender : Any){
    let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: tableView)
    if let indexPath = tableView?.indexPathForRow(at: position)
    {
        backEndManger?.addOrRemoveFavRequest(id: products[indexPath.row]._id)
        favIndex = indexPath
    }
}
 
}
extension ResturantMenuVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: MealDetailsViewControllerID) as! MealDetailsViewController
        vc.product = products[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
