//
//  ResturantMenuVC.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages

class ResturantMenuVC: UIViewController {
    var backEndManger : ResturantMenuBackEndManger?
    var products = [ProductModel]()
    var categoryId : Int!
    var restaurantId : Int!
    var favIndex : IndexPath!
    @IBOutlet weak var tableView: UITableView!
    @IBAction func openCart(_ sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewController(withIdentifier: CartViewControllerID) as! CartViewController
//        let restId = String(restaurantId)
//        vc.order.rest_id = restId
       
        navigationController?.pushViewController(vc, animated: true)
    }
    var refreshTVControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshTVControl = UIRefreshControl()
        refreshTVControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "Pull to refresh"))
        refreshTVControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.refreshControl = refreshTVControl

        title = RestaurantMenuString
        let menuCellNib = UINib(nibName: "ResturantMenuTableViewCell", bundle: nil)
        tableView.register(menuCellNib, forCellReuseIdentifier: RestProductMenuCellID)
        backEndManger = ResturantMenuBackEndManger()
        backEndManger?.delegate = self
        backEndManger?.DownloadData(categoryId: categoryId, restaurantId: restaurantId)
        SwiftLoading().showLoading()
    }
    @objc func refresh() {
        backEndManger?.DownloadData(categoryId: categoryId, restaurantId: restaurantId)
        SwiftLoading().showLoading()
    }




}
extension ResturantMenuVC : MenuDelegate{
    func MakeMealFav(DidFinshDownload Sender: String) {
        SwiftMessages.showMessage(title: "", body: Sender, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        backEndManger?.DownloadData(categoryId: categoryId, restaurantId: restaurantId)
        SwiftLoading().hideLoading()

    }
    
    func Menu(didFinishDownload Products: [ProductModel]?) {
        products = Products!
        if products.count > 0{
        if favIndex != nil {
            tableView.reloadRows(at: [favIndex], with: .automatic)
        }else{
        tableView.reloadData()
            }
        }else{
            SwiftMessages.showMessage(title: "", body: NSLocalizedString("no products have been added yet", comment: ""), type: .info, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        }
          SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()
    }
    
    
    func Menu(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()


        
    }
    
    func Menu(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()


        
    }
    
    
}
