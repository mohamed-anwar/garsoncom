//
//  ResturantDetalsViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/13/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages
import DropDown


class ResturantDetalsViewController: UIViewController {
    var responseState = ResponseState.loading
    let showMore = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.showMore
        ]
    }()
    @IBAction func ShowMenu(_ sender: UIBarButtonItem) {
        let resturantCategotyVC = storyboard?.instantiateViewController(withIdentifier: ResturantCategoryTableViewControllerID) as! ResturantCategoryTableViewController
        resturantCategotyVC.id = resturant?._id
        resturantCategotyVC.title = resturant?._name
        navigationController?.pushViewController(resturantCategotyVC, animated: true)
    }
    @IBAction func makeFavResturant(_ sender: Any) {
        backEndManger?.addOrRemoveFavRequest(id: (resturant?._id)!)
        SwiftLoading().showLoading()
    }
    @IBAction func showMore(_ sender: UIBarButtonItem) {
        showMore.show()
    }
    @IBOutlet weak var showMoreButton: UIBarButtonItem!
    @IBOutlet weak var makFavButton: UIBarButtonItem!
    var resturant : ResturantModel?
    var reviews = [ReviewModel]()
    var backEndManger : ResturantDetailBackENdManger?
    var mainView: ResturantDetailV!{
        return  view  as! ResturantDetailV
    }
    
    
    func NavigationTrackerPressedPressed(){
        
        if let UrlNavigation = URL.init(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(UrlNavigation){
                if resturant?._lat != nil && resturant?._lng != nil {
                    
                    let lat =      resturant?._lat
                    let longi = resturant?._lng
                    
                    if let urlDestination = URL.init(string: "comgooglemaps://?saddr=&daddr=\(String(describing: lat)),\(String(describing: longi))&directionsmode=driving") {
                        UIApplication.shared.openURL(urlDestination)
                    }
                }
            }
            else {
                NSLog("Can't use comgooglemaps://");
                self.openTrackerInBrowser()
                
            }
        }
        else
        {
            NSLog("Can't use comgooglemaps://");
            self.openTrackerInBrowser()
        }
        
        
        
    }
    func openTrackerInBrowser(){
        if resturant?._lat != nil && resturant?._lng != nil {
            
            let lat =      resturant?._lat
            let longi = resturant?._lng
            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(lat),\(longi)&directionsmode=driving") {
                UIApplication.shared.openURL(urlDestination)
            }
        }
    }
    
    @IBAction func viewMoreButton(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "ReviewsViewControllerID") as! ReviewsViewController
        vc.restaurant = resturant!
        
        vc.title = resturant?._name
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backEndManger = ResturantDetailBackENdManger()
        backEndManger?.delegate = self
        backEndManger?.DownloadData(restaurantId: (resturant?._id)!)
        setupSetShowMore()
        mainView.ConfigureView(image: (resturant?._image)!, name: (resturant?._name)!, rate: (resturant?._rate)!, from: (resturant?._openFrom)!, to: (resturant?._openTo)!, deliveryCoust:String(describing:(resturant?._deliveryCost)!), rateCount: String(describing:(resturant?._rateCount)!), deliveryTime: (resturant?._deliveryFrom)! , untilTime: (resturant?._deliveryTo)!, isDelivery: (resturant?._isDeleviry)!)

        let restDetailCellNib = UINib(nibName: "ResturantDetailTableViewCell", bundle: nil)
        mainView.tableView.register(restDetailCellNib, forCellReuseIdentifier: RestDetailCellID)
        let addReviewCellNib = UINib(nibName: "AddReviewTableViewCell", bundle: nil)
        mainView.tableView.register(addReviewCellNib, forCellReuseIdentifier: AddReviewID)
        let deafultCellNib = UINib(nibName: TableViewCelldentifiers.defualt, bundle: nil)
        mainView.tableView.register(deafultCellNib, forCellReuseIdentifier: TableViewCelldentifiers.defualt)
        let loadingCellNib = UINib(nibName: TableViewCelldentifiers.loadingCell, bundle: nil)
        mainView.tableView.register(loadingCellNib, forCellReuseIdentifier: TableViewCelldentifiers.loadingCell)
        makFavButton.image = (resturant?._isFavourite)! ? #imageLiteral(resourceName: "likeFull") : #imageLiteral(resourceName: "like")
        showMore.dataSource = ["Location" , "Review" , "Tables"]
        if !(resturant?._haveTables)!{
            showMore.dataSource.removeLast()
        }
    }

}
extension ResturantDetalsViewController : ResturantDetaislDelegate{
    func ResturantDetail(didFinishDownload review: ReviewModel?, msg: String) {
    
    }
    
  
    
    func ResturantDetail(didFinishDownload Reviews: [ReviewModel]?) {
        reviews = Reviews!
        if reviews.count > 0{
            responseState = ResponseState.result
        }else{
            responseState = ResponseState.defualt
        }
        mainView.tableView.reloadData()
    }
    func ResturantDetail(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        SwiftLoading().hideLoading()
    }
    func ResturantDetail(didGetUnexpectedResponse: String) {
        SwiftLoading().hideLoading()
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
    }
    func ResturantDetail(Success msg: String) {
        SwiftLoading().hideLoading()
        resturant?._isFavourite = !(resturant?._isFavourite)!
        makFavButton.image = (resturant?._isFavourite)! ? #imageLiteral(resourceName: "likeFull") : #imageLiteral(resourceName: "like")
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
    }
}
extension ResturantDetalsViewController : UITableViewDelegate{
    
}
extension ResturantDetalsViewController  : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
      return 1
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch responseState {
        case ResponseState.defualt , ResponseState.loading:
            return 1
        case ResponseState.result :
            return (reviews.count)
        default :
            return 0
            
        }

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch responseState {
        case ResponseState.defualt:
            let cell =  tableView.dequeueReusableCell(withIdentifier: TableViewCelldentifiers.defualt) as! DefualtTableViewCell
            cell.Configure(image:#imageLiteral(resourceName: "selectedHumperger"))
            return cell
        case ResponseState.result :
            let cell = tableView.dequeueReusableCell(withIdentifier: RestDetailCellID) as! ResturantDetailTableViewCell
            cell.ConfigureCell(name: reviews[indexPath.row]._client._name , image: reviews[indexPath.row]._client._image, desc: reviews[indexPath.row]._desc)
            return cell
        default :
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCelldentifiers.loadingCell,for: indexPath)
            let spinner = cell.viewWithTag(100) as! UIActivityIndicatorView
            spinner.startAnimating()
            return cell
        }
   
    }
}
extension ResturantDetalsViewController{
    func setupSetShowMore() {
        showMore.anchorView =  showMoreButton
        showMore.bottomOffset = CGPoint(x: 0, y:  32)
        showMore.selectionAction = { [unowned self] (index, item) in
            switch index {
            case 0:
                self.NavigationTrackerPressedPressed()
//            case 1 :
            case 2 :
                let tableVC = self.storyboard?.instantiateViewController(withIdentifier: TableTimePickerViewControllerID) as! TableTimePickerViewController
                tableVC.paramter["restaurant_id"] = self.resturant?._id
                self.navigationController?.pushViewController(tableVC, animated: true)
            default:
                break
            }
        }
    }
}

