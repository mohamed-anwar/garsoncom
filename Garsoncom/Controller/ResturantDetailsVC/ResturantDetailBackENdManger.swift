//
//  ResturantDetailBavkENdManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/25/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
class  ResturantDetailBackENdManger {
    
    private var network : Networking!
    weak var delegate  : ResturantDetaislDelegate?
    
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    func addOrRemoveFavRequest(id : Int) {
        let parameters : [String : Any] = [
            "id" : id ,
            "type" : "restaurant" ,
            "api_token" : userLoginToken!
        ]
        network.manager.request( AddFavUrl ,method : .post, parameters :  parameters , headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self]response in
            guard let strongSelf = self else{return}
            let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                
                if json["status"].string! == StatusMsgs.sucess{
                    strongSelf.delegate?.ResturantDetail(Success: json["message"].string!)
                }else{
                    strongSelf.delegate?.ResturantDetail(didGetUnexpectedResponse: json["message"].string!)
                }
                break
                
            case .failure(let error) :
                strongSelf.delegate?.ResturantDetail(didFaild: error as NSError)
                break
                
            }
        }
    }
    
    func DownloadData(restaurantId : Int) {
        network.manager.request( GetResturantReviewsURL + "\(restaurantId)" ,headers : [ acceptLanguage :  setLang() ]).responseJSON {[weak self] response in
            var reviewsModels = [ReviewModel]()
            let result = response.result
            guard let strongSelf = self else{return}

            switch result {
            case .success(let value):
                let json = JSON(value)
                let contentArray = json["data"].dictionaryValue
                if let reviews = contentArray["data"]?.arrayValue{
                    for resturant in reviews {
                        if let dict = resturant.dictionaryObject {
                            let model = ReviewModel()
                            model.mapping(map: Map(mappingType: .fromJSON, JSON: dict))
                            reviewsModels.append(model)
                            
                        }
                        
                    }
                    strongSelf.delegate?.ResturantDetail(didFinishDownload: reviewsModels)
                    
                } else {
                    strongSelf.delegate?.ResturantDetail(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.ResturantDetail(didFaild: error as NSError)
                break
                
            }
        }
    }
    func addReview(id : Int , desc : String) {
        let parameters : [String : Any] = [
            "restaurant_id" : id ,
            "description" : desc ,
            "api_token" : userLoginToken!
        ]
        network.manager.request( AddReviewUrl ,method : .post, parameters :  parameters , headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self]response in
            guard let strongSelf = self else{return}
            let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].string! == StatusMsgs.sucess{

                    strongSelf.delegate?.ResturantDetail(Success: json["message"].string ?? "")

       
                } else {
                    strongSelf.delegate?.ResturantDetail(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.ResturantDetail(didFaild: error as NSError)
                break
                
            }
        }
    }
    
    
    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
