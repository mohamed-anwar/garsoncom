//
//  ResturantDetailTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/12/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit

class ResturantCategoryTableViewController: UITableViewController {
 var categories = [CategoryModel]()
    var id : Int!
    var backEndManger : ResturantCategoryBackEndManger!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(id)
        backEndManger = ResturantCategoryBackEndManger()
        backEndManger.delegate = self
        backEndManger.DownloadData(id: id)
        SwiftLoading().showLoading()

        let RestDetailCellNib = UINib(nibName: "ResturantDetailMenuCellTableViewCell", bundle: nil)
        tableView.register(RestDetailCellNib, forCellReuseIdentifier: ResturantDetailMenuCellID)
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return tableView.frame.height / 5
       
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:ResturantDetailMenuCellID , for: indexPath) as! ResturantDetailMenuCellTableViewCell
        cell.SetCell(name: categories[indexPath.row]._name, image: categories[indexPath.row]._image)
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: RestMenuID) as! ResturantMenuVC
        vc.restaurantId = id
        vc.categoryId = categories[indexPath.row]._id
        
       navigationController?.pushViewController(vc, animated: true)
    }
}
extension ResturantCategoryTableViewController : ResturantCategoryDelegate{
    func ResturantDetail(didFinishDownload Categories: [CategoryModel]?) {
        categories = Categories!
        tableView.reloadData()
        SwiftLoading().hideLoading()

    }
    
    func ResturantDetail(didFaild error: NSError?) {
        SwiftLoading().hideLoading()

        
    }
    
    func ResturantDetail(didGetUnexpectedResponse: String) {
        SwiftLoading().hideLoading()

        
    }
    
    
}
