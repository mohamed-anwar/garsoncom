//
//  ResturantDetailBackEndManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import SwiftyJSON
import ObjectMapper
class ResturantCategoryBackEndManger {
    
    private var network : Networking!
    weak var delegate  : ResturantCategoryDelegate?

    
    init() {
        network = Networking(requestTimeout: 10)
    }
    
    func DownloadData(id : Int) {
        network.manager.request( getRestaurantCategoryURL + "?id=" + "\(id)", headers : [ acceptLanguage :  setLang() ]).responseJSON {[weak self] response in
                var categoriesModels = [CategoryModel]()
                let result = response.result
            guard let strongSelf = self else{return}
                switch result {
                case .success(let value):
                    let json = JSON(value)
                    if let contentArray = json["data"].array {
                        for contentObject in contentArray {
                            let contentDictionary = contentObject.dictionaryObject
                            let model = CategoryModel()
                            model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                            categoriesModels.append(model)
                        }
                        strongSelf.delegate?.ResturantDetail(didFinishDownload: categoriesModels)
                        
                    } else {
                        strongSelf.delegate?.ResturantDetail(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                    }
                    break
                case .failure(let error) :
                    strongSelf.delegate?.ResturantDetail(didFaild: error as NSError)
                    break
                    
                }
            }
    }
    

    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
