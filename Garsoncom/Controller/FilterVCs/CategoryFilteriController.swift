//
//  CategoryFilteriController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/17/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

//
//  GeneralFilterTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/16/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages

class  CategoryFilteriController: UIViewController {
    var data:[CategoryModel] = []
    var selectedItems:[Int] = []
    var dataManger : HomeBackEndManger!
    var selectedCategoriesids = [Int]()

    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        dataManger = HomeBackEndManger()
        dataManger.delegate = self
        dataManger.DownloadCategoryData()
  
        // Do any additional setup after loading the view.
        let cellNib = UINib(nibName: "LocationCell", bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: LocationCellID)
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: 150, height: 50)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        collectionView.collectionViewLayout = layout
                collectionView.allowsMultipleSelection = true
      
        
     
        
        
    }
    
    
    /*override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
     
     if animated {
     let indexPath = collectionView.indexPathsForVisibleItems
     
     for index in indexPath {
     
     if let cell = collectionView.cellForItem(at: index) as? ColorCell {
     cell.animateCell()
     }
     }
     }
     
     }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension  CategoryFilteriController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LocationCellID,
                                                         for: indexPath) as? LocationCell {
            
            cell.kmLabel.text = data[indexPath.row]._name
            
            if data[indexPath.row]._selected{
                cell.setUpSelectedStyle()
            } else {
                cell.setUpUnselectedStyle()
            }
            
            return cell
        } else {
            
            return UICollectionViewCell()
        }
        
        
    }
}
extension  CategoryFilteriController: HomeDelegate{
    func Home(didFinishDownload Products: [ProductModel]?) {
        
        
    }
    
    func Home(didFinishDownload Categories: [CategoryModel]?) {
        data = Categories!
        data.first?._selected = false
        for index in selectedItems {
            data[index]._selected = true
        }
        collectionView.reloadData()
        for index in selectedItems {
            collectionView.selectItem(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        }

        
    }
    
    func Home(didFinishDownload Resturants: [ResturantModel]?, ResturantsRates: [ResturantModel]?, Products: [ProductModel]?) {
    
    }
    
    func Home(didFaild error: Error?) {
        
        SwiftMessages.showMessage(title: "", body:(error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
    
    func Home(didGetUnexpectedResponse: String) {
                SwiftMessages.showMessage(title: "", body:didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
        
    }
    
    
}

extension  CategoryFilteriController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? LocationCell {
            cell.setUpSelectedStyle()
            selectedItems.append(indexPath.row)
            selectedCategoriesids.append(data[indexPath.row]._id)
        }
    }
    
        func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
            if let cell = collectionView.cellForItem(at: indexPath) as? LocationCell {
                cell.setUpUnselectedStyle()
    
                if let index = selectedItems.index(of: indexPath.row) {
                    selectedItems.remove(at: index)
                    selectedCategoriesids.remove(at: index)
                }
            }
        }
    
}
