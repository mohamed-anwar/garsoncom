//
//  ColorFilterViewController.swift
//  D2PCurvedModal_Example
//
//  Created by Pradheep Rajendirane on 18/11/2017.
//  Copyright © 2017 CocoaPods. All rights reserved.
//
class LocationFilterModel {
    var selected  = false
    var km = 0
}
class GeneralFilterModel {
    var selected  = false
    var name = ""
}

import UIKit

class LocationFilterViewController: UIViewController {
    var data = [LocationFilterModel]()
    var selectedItem = Int()
    var indexSelected = Int()
    
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        for i in 1...5{
            let data1 = LocationFilterModel()
            data1.selected = false
            data1.km = i
            data.append(data1)
        }
        
        print(indexSelected)
        // Do any additional setup after loading the view.
        let cellNib = UINib(nibName: "LocationCell", bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: LocationCellID)
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: 150, height: 30)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        collectionView.collectionViewLayout = layout
//        collectionView.allowsMultipleSelection = true
        if indexSelected  != 10 {
        collectionView.selectItem(at: IndexPath(row: indexSelected, section: 0), animated: false, scrollPosition: .centeredHorizontally)
            data[indexSelected].selected = true
        }
        
        
    }

    
    /*override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if animated {
            let indexPath = collectionView.indexPathsForVisibleItems
            
            for index in indexPath {
                
                if let cell = collectionView.cellForItem(at: index) as? ColorCell {
                    cell.animateCell()
                }
            }
        }
     
    }*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LocationFilterViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LocationCellID,
                                                         for: indexPath) as? LocationCell {
            
            cell.kmLabel.text = "\(data[indexPath.row].km)" + NSLocalizedString("KM", comment: "")
            
            if data[indexPath.row].selected{
                cell.setUpSelectedStyle()
            } else {
                cell.setUpUnselectedStyle()
            }
            
            return cell
        } else {
            
            return UICollectionViewCell()
        }
        
        
    }
}


extension LocationFilterViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView.cellForItem(at: indexPath) as? LocationCell) != nil {
//            cell.setUpSelectedStyle()
            selectedItem = data[indexPath.row].km
            data.forEach({
                $0.selected = false
            })
            data[indexPath.row].selected = true
            collectionView.reloadData()
            indexSelected = indexPath.row
            
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//        if let cell = collectionView.cellForItem(at: indexPath) as? LocationCell {
//            cell.setUpUnselectedStyle()
//
//            if let index = selectedItems.index(of: indexPath.row) {
//                selectedItems.remove(at: index)
//            }
//        }
//    }
    
}
