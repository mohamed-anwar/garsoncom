//
//  GeneralFilterTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/16/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
var generalFilterArray = [
NSLocalizedString("Fastest Delivery", comment: "") ,
NSLocalizedString("Cheapest Delivery", comment: "") ,
NSLocalizedString("A to Z", comment: "") ,
NSLocalizedString("Home Cooking", comment: "") ,
NSLocalizedString("Most Requested Restraunts", comment: "")

]
class GeneralFilterViewController: UIViewController {
    var data = [GeneralFilterModel]()
    var selectedItem = ""
    var indexSelected = Int()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalFilterArray.forEach({
            let data1 = GeneralFilterModel()
            data1.selected = false
            data1.name = $0
            data.append(data1)
        })
        
        
        // Do any additional setup after loading the view.
        let cellNib = UINib(nibName: "LocationCell", bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: LocationCellID)
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: 250, height: 50)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        collectionView.collectionViewLayout = layout
        //        collectionView.allowsMultipleSelection = true
        if indexSelected  != 10 {
            collectionView.selectItem(at: IndexPath(row: indexSelected, section: 0), animated: false, scrollPosition: .centeredHorizontally)
            data[indexSelected].selected = true
        }
        
        
    }
    
    
    /*override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
     
     if animated {
     let indexPath = collectionView.indexPathsForVisibleItems
     
     for index in indexPath {
     
     if let cell = collectionView.cellForItem(at: index) as? ColorCell {
     cell.animateCell()
     }
     }
     }
     
     }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension GeneralFilterViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LocationCellID,
                                                         for: indexPath) as? LocationCell {
            
            cell.kmLabel.text = data[indexPath.row].name
            
            if data[indexPath.row].selected{
                cell.setUpSelectedStyle()
            } else {
                cell.setUpUnselectedStyle()
            }
            
            return cell
        } else {
            
            return UICollectionViewCell()
        }
        
        
    }
}


extension GeneralFilterViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView.cellForItem(at: indexPath) as? LocationCell) != nil {
            //            cell.setUpSelectedStyle()
            selectedItem = data[indexPath.row].name
            data.forEach({
                $0.selected = false
            })
            data[indexPath.row].selected = true
            collectionView.reloadData()
            indexSelected = indexPath.row
            
        }
    }
    
    //    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    //        if let cell = collectionView.cellForItem(at: indexPath) as? LocationCell {
    //            cell.setUpUnselectedStyle()
    //
    //            if let index = selectedItems.index(of: indexPath.row) {
    //                selectedItems.remove(at: index)
    //            }
    //        }
    //    }
    
}
