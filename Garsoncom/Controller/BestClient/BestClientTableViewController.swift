//
//  BestClientTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/16/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages

class BestClientTableViewController: UITableViewController {
    
    var bestClients = [BestClient]()
    
    var backend : BestClientBackEnd!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backend = BestClientBackEnd()
        backend.delegate  = self
        backend.DownloadData()
        SwiftLoading().showLoading()

        let celllNib = UINib(nibName: "BestClientTableViewCell", bundle: nil)
        tableView.register(celllNib, forCellReuseIdentifier: BestClientTableViewCellID)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bestClients.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BestClientTableViewCellID, for: indexPath) as! BestClientTableViewCell
        cell.ConFigure(name: bestClients[indexPath.row]._name, numberOfOrders: "\(bestClients[indexPath.row]._orders_count)", image: bestClients[indexPath.row]._image)
        return cell
    }

}
extension BestClientTableViewController : BestClientDelegate{
    func bestClient(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        SwiftLoading().hideLoading()


        
    }
    func bestClient(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        SwiftLoading().hideLoading()


        
    }
    func bestClient(DidFinshDownload Sender: [BestClient]) {
        bestClients = Sender
        tableView.reloadData()
        SwiftLoading().hideLoading()

    }
    
    
}

