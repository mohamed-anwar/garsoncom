//
//  BestClientBackEnd.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/17/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


import SwiftyJSON
import ObjectMapper
class  BestClientBackEnd {
    
    private var network : Networking!
    weak var delegate  : BestClientDelegate?
    
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    
    
    func DownloadData() {
        network.manager.request(BestClientUrl,headers : [ acceptLanguage :  setLang() ]).responseJSON {[weak self] response in
            guard let  strongSelf = self else{return}
            let result = response.result
            var models = [BestClient]()
            switch result {
            case .success(let value):
                let json = JSON(value)
                if let contentArray = json["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = BestClient()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        models.append(model)
                    }
                    strongSelf.delegate?.bestClient(DidFinshDownload: models)
                    
                } else {
                    strongSelf.delegate?.bestClient(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
              
                break
            case .failure(let error) :
                strongSelf.delegate?.bestClient(didFaild: error as NSError )
                break
                
            }
        }
    }
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
