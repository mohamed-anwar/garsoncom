//
//  ProfileBackEnd.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/15/18.
//  Copyright © 2018 Rivers. All rights reserved.
//




import SwiftyJSON
import ObjectMapper
class  ProfileBackEnd {
    
    private var network : Networking!
    weak var delegate  : ProfileDelegate?
    
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    
    
    func DownloadData(parameters : [String:Any] , image : Data?) {
        
      
        network.manager.upload(multipartFormData: { multipartFormData in
            if let imageData = image{
            multipartFormData.append(imageData, withName: "img", fileName: "image", mimeType: "image/jpeg")

            }
            for (key , value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },to : ProfileUrl, encodingCompletion: { [weak self] encodingResult in
            guard let  strongSelf = self else{return}

            switch encodingResult {
            case .success(let upload,_,_) :
                upload.debugLog().responseJSON {  response in

                    switch response.result {
                    case .success(let value) :
                        let json = JSON(value)
                        if json["message"].string! == StatusMsgs.dataUpdated{
                            let contentArray = json["data"].dictionaryValue
                            if let userData = contentArray["user"]?.dictionaryObject{
                                let userModel = UserModel(map: Map(mappingType: .fromJSON, JSON: userData))
                                userModel?.debugClass()
                                strongSelf.delegate?.Profile(DidFinshDownload: userModel! , msg : json["message"].string!)
                                
                            } else {
                                strongSelf.delegate?.Profile(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                            }
                        }else{
                            strongSelf.delegate?.Profile(didGetUnexpectedResponse: json["message"].string!)
                        }
                        break
                    case .failure(let error) :
                        strongSelf.delegate?.Profile(didFaild: error as NSError)
                    }
                }
            case .failure(let error) :
                strongSelf.delegate?.Profile(didFaild: error as NSError)
            }
        })
    }
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
