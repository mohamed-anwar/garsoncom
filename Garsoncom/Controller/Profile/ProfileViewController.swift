//
//  ProfileViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/15/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages
var profileTitles = [
    NSLocalizedString("Name : ", comment: ""),
    NSLocalizedString("Email : ", comment: ""),
    NSLocalizedString("Phone : ", comment: ""),
   NSLocalizedString("Orders : ", comment: "")
]
let profileIcons = [#imageLiteral(resourceName: "Profile-black"),#imageLiteral(resourceName: "mail-1"),#imageLiteral(resourceName: "phone-1"),#imageLiteral(resourceName: "order")]


class ProfileViewController: UIViewController {
    var user = UserModel()
    var imagePickerController: UIImagePickerController!
    var mainImage : Data!
    var pimage : UIImage!
    @IBOutlet weak var mainview : ProfileV!
    @objc func PickupImage(_ sender: UIButton) {
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        self.present(imagePickerController, animated: true, completion: nil)
    }
    weak var delegate : ProfileDelegate!
    var backEnd  : ProfileBackEnd!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(PickupImage))
          mainview.ProfileImageView.isUserInteractionEnabled = true

        mainview.ProfileImageView.addGestureRecognizer(tap)
        backEnd = ProfileBackEnd()
        backEnd.delegate = self
        print( ["api_token" : userLoginToken!])
        backEnd.DownloadData(parameters: ["api_token" : userLoginToken!], image: nil)
        makeBorderToView(mainview.ProfileImageView, boderWidth: 1, cornerRadius: mainview.ProfileImageView.frame.width / 2, borderColor: .clear, maskToBounds: true)
        let updateButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(updateProfile))
        navigationItem.rightBarButtonItem = updateButton
        let nibCell = UINib(nibName: "ProfileTableViewCell", bundle: nil)
        mainview.tabelView.register(nibCell, forCellReuseIdentifier: ProfileTableViewCellID)
        let nibCellLogOut = UINib(nibName: "LogOutTableViewCell", bundle: nil)
        mainview.tabelView.register(nibCellLogOut, forCellReuseIdentifier: LogOutTableViewCellID)
        
    }
   



}
extension ProfileViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
        switch indexPath.row {
        case 0...2:
            let vc = storyboard?.instantiateViewController(withIdentifier: EditProfileViewControllerID) as! EditProfileViewController
            vc.user = user
            vc.index = indexPath.row
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
        }else{
            userLoginToken = nil
            navigationController?.popViewController(animated: true)
        }
    }
}
extension ProfileViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 4
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCellID, for: indexPath) as! ProfileTableViewCell
            cell.configureCell(name: profileTitles[indexPath.row], icon: profileIcons[indexPath.row])
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: LogOutTableViewCellID, for: indexPath)
            
            return cell
            
            
        }
    }
    
    func upDateData(){
        profileTitles[0] = NSLocalizedString("Name : \(String(describing: user._name))", comment: "")
        profileTitles[1] = NSLocalizedString("Email : \(String(describing: user._email))", comment: "")
        profileTitles[2] = NSLocalizedString("Phone : \(String(describing: user._phone))", comment: "")
        profileTitles[3] = NSLocalizedString("Orders : \(String(describing: user._numberOfOrder))", comment: "")
        mainview.tabelView.reloadData()
    }
    @objc func updateProfile() {
//        print( ["api_token" : userLoginToken!])
//
        backEnd.DownloadData(parameters: ["api_token" : userLoginToken! ,
                                          "name" : user._name,
                                          "email" : user._email,
                                          "phone" : user._phone
            ], image: mainImage )
        
    }
    
}
extension ProfileViewController : ProfileDelegate{
    func Profile(DidFinshDownload Sender: UserModel, msg: String) {
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        user = Sender
        upDateData()
        mainview.Configure(image: Sender._image)
    }
    func Profile(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
    }
    func Profile(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
    }
}
extension ProfileViewController : EditeProfileDelegate{
    func edit(User: UserModel) {
        user = User
         upDateData()
        
    }
}
extension ProfileViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
          mainview.ProfileImageView.image = pickedImage
            pimage = pickedImage
        }else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            mainview.ProfileImageView.image = image

            pimage = image
            
        }
        mainImage =  pimage.ResizeImage(targetSize: CGSize(width: 800.0, height: 800.0))
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
