//
//  EditProfileViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/19/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class EditProfileViewController: UIViewController {
    var user = UserModel()
    var index = 0
    weak var delegate : EditeProfileDelegate!
    

    @IBOutlet weak var editTextF: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        switch index {
        case 0:
            editTextF.text = user._name
        case 1:
            editTextF.text = user._email
        case 2:
            editTextF.text = user._phone
        default:
            break
        }

    }

    @IBAction func changeButton(_ sender: UIButton) {
        switch index {
        case 0:
            user._name = editTextF.text!
        case 1:
            user._email = editTextF.text!
        case 2:
            user._phone = editTextF.text!
        default:
            break
        }
        delegate.edit(User: user)
        self.navigationController?.popViewController(animated: true)


        
    }
    
    @IBAction func CancelButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    

}
