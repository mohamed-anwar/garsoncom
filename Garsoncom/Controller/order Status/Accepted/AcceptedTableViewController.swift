//
//  AcceptedTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


import UIKit
import SwiftMessages

class AcceptedTableViewController: UITableViewController {
    var sections  = [OrderHeader]()
    var backEnd  : OrderStatusBackEndManger!
    var refreshTVControl: UIRefreshControl!
    
   
    @objc func refresh() {
        backEnd.DownloadData(status: "accepted")
        SwiftLoading().showLoading()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshTVControl = UIRefreshControl()
        refreshTVControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "Pull to refresh"))
        refreshTVControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.refreshControl = refreshTVControl
        backEnd = OrderStatusBackEndManger()
        backEnd.delegate = self
        backEnd.DownloadData(status: "accepted")
        SwiftLoading().showLoading()
        
        setupTableView()
        registerNibs()
    }
    
    func setupTableView() {
        tableView.estimatedRowHeight = 146
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func registerNibs() {
        var nib = UINib(nibName: "collapsibleTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "collapsibleTableViewCellID")
        nib = UINib(nibName: "RequestedOrderCollapibleHeader", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "RequestedOrderCollapibleHeaderID")
    }

  
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section]._collapsed ? 0 : sections[section]._orderDetails.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "collapsibleTableViewCellID") as! collapsibleTableViewCell
        
        
        cell.configure(image: sections[indexPath.section]._orderDetails[indexPath.row]._image, name: sections[indexPath.section]._orderDetails[indexPath.row]._name, cost: sections[indexPath.section]._orderDetails[indexPath.row]._price)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "RequestedOrderCollapibleHeaderID") as! RequestedOrderCollapibleHeader
        header.configure(id: String(sections[section]._id), deliveryCoust: sections[section]._deliveryCost, totalCoust: String(sections[section]._totalCost) , status: sections[section]._status, date: sections[section]._created_at, remaing:  sections[section]._remainingBySecond, image:  sections[section]._restaurantImage, restName:sections[section]._restName, orderNo: "\(sections[section]._orderNo)")

        
        header.setCollapsed(sections[section]._collapsed)
        header.section = section
        header.delegate = self
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 101
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.0
    }
    
    
    
}
extension AcceptedTableViewController : CollapsibleTableViewHeaderDelegate {
    func toggleCancel(section: Int) {
        backEnd.CancelOrderRequest(id: sections[section]._id)
        refreshTVControl.endRefreshing()
        SwiftLoading().showLoading()
        
    }
    
    
    func toggleSection(_ header: RequestedOrderCollapibleHeader, section: Int) {
        let collapsed = !sections[section]._collapsed
        sections[section]._collapsed = collapsed
        //        header.setCollapsed(collapsed)
        tableView.reloadSections(IndexSet(integer: section), with: .automatic)
    }
}
extension AcceptedTableViewController : OrderStatusDelegate {
    func OrderStatus(DidFinshDownload Sender: [OrderHeader]) {
        sections = Sender
        tableView.reloadData()
        refreshTVControl.endRefreshing()

        SwiftLoading().hideLoading()
        
    }
    
    func OrderStatus(didFaild error: Error?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        refreshTVControl.endRefreshing()

        SwiftLoading().hideLoading()
        
        
    }
    
    func OrderStatus(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        refreshTVControl.endRefreshing()

        SwiftLoading().hideLoading()
        
        
    }
    func OrderStatus(Success msg: String) {
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        backEnd.DownloadData(status: "accepted")
        refreshTVControl.endRefreshing()

        SwiftLoading().showLoading()
        
        
    }
    
    
    
}

