//
//  OrderStautsBackEndManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


import SwiftyJSON
import ObjectMapper
class OrderStatusBackEndManger {
    
    private var network : Networking!
    weak var delegate  : OrderStatusDelegate?
    
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    func CancelOrderRequest(id : Int) {
      
        network.manager.request( CancelOrderUrl ,method : .post, parameters : ["api_token" : userLoginToken! , "order_id" : id] , headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self]response in
            guard let strongSelf = self else{return}
            let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                if json["status"].string! == StatusMsgs.sucess{
                    strongSelf.delegate?.OrderStatus(Success: json["message"].string!)
                }else{
                    strongSelf.delegate?.OrderStatus(didGetUnexpectedResponse: json["message"].string!)
                }
                break
                
            case .failure(let error) :
                strongSelf.delegate?.OrderStatus(didFaild: error as NSError)
                break
                
            }
        }
    }

    func DownloadData(status : String) {
        network.manager.request( GetMyOrdersUrl , method : .post , parameters : ["api_token" : userLoginToken! , "status" : status], headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            var models = [OrderHeader]()
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                if let contentArray = json["data"]["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = OrderHeader()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        models.append(model)
                    }
                    strongSelf.delegate?.OrderStatus(DidFinshDownload: models)
                } else {
                    strongSelf.delegate?.OrderStatus(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.OrderStatus(didFaild: error)
                break
                
            }
        }
    }
    
    
    
    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
