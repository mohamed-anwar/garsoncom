//
//  ProcessOrderViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/25/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import  SkyFloatingLabelTextField
import SwiftMessages
import SideMenu
import SwiftyJSON
import DatePickerDialog


class ProcessOrderViewController: UIViewController {
    var mydate  = ""
    @IBAction func futureOrderBarButtonItem(_ sender: UIBarButtonItem) {
        DatePickerDialog().show(DatePickerTitleString, doneButtonTitle: OkayButtonTitle, cancelButtonTitle:CancelButtonTitle, datePickerMode: .dateAndTime) {[weak self]
            (date) -> Void in
            guard let strongSelf = self else{return}
            
            if let dt = date {
                strongSelf.mydate = convertDateTimeObject(date: dt)
            }
        }
    }
    
    var selectedIndex = IndexPath(item: 0, section: 0)
    var order = Order()
    var _view : ProcessOrder{
        return view as! ProcessOrder
    }
    @IBOutlet weak var tableView : UITableView!
    var paymentMethods = [PaymentMethod]()
    var backEnd : ProcessOrderBackEnd!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let paymentMethod = PaymentMethod()
        paymentMethod._psymentMethod_id = "1"
        paymentMethod._psymentMethod_name = "Cash"
        paymentMethods.append(paymentMethod)
//        let nibCell = UINib(nibName: "SelectPaymentTableViewCell", bundle: nil)
//        tableView.register(nibCell, forCellReuseIdentifier: SelectPaymentTableViewCellID)
        backEnd = ProcessOrderBackEnd()
        backEnd.delegate = self
        _view.configureView(deliveryTime: order.deliveryTime, restName: order.restName, subTotal:String(order.price), deliveryMin: order.deliverMin, deliveryMax: order.deliverMax)
        
    }
    
    @IBAction func procssOrderButton(sender : UIButton){
        if Validator.validateInputs(inputs: [_view.NameTextF , _view.addressTxtF ,_view.phoneTextF]) {
            if userLoginToken != nil {
                var parameter = [String :Any]()
                var products = [Any]()
                var myorder = [String : Any]()
                order.foods.enumerated().forEach({ (index,item) in
                    myorder = [
                        "id" : "\(item.productId)" ,
                        "price" : "\(item.productPrice)" ,
                        "quantity" : "\(item.quantity)" ,
                        "cov1" : "\(item.cov1)" ,
                        "cov2" : "\(item.cov2)" ,
                        "cov3" : "\(item.cov3)" ,
                        "cov4" : "\(item.cov4)" ,
                        "size_id" : "\(item.sizeId)"
                    ]
                    products.append(myorder)
                    
                })
                parameter["products"] = products
                parameter["api_token"] = userLoginToken!
                parameter["restaurant_id"] = order.rest_id
                parameter["address"] = _view.addressTxtF.text!
                parameter["payment_method_id"] = "1"
                parameter["phone"] = _view.phoneTextF.text!
                parameter["future_datetime"]  = mydate
                parameter["lat"] = lat!
                parameter["lng"] = lng!
                parameter["name"] = _view.NameTextF.text!
                
                backEnd.addOrder(parameters: parameter)
                SwiftLoading().showLoading()

            }else{
                presentViewController(RegisterNavID)
                
            }
        }
    }
    
    
   
    
}
    
    extension ProcessOrderViewController : UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return paymentMethods.count
        }
        
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: SelectPaymentTableViewCellID, for: indexPath) as! SelectPaymentTableViewCell
            cell.configureCell(PaymentType: paymentMethods[indexPath.row]._psymentMethod_name)
            
            if indexPath == selectedIndex{
                cell.configureCell(selected: true)
            }else{
                cell.configureCell(selected: false)
            }
            
            return cell
        }
    }
    extension ProcessOrderViewController : ProcessOrderDelegate{
        func order(addSucessfully msg: String) {
            SwiftLoading().hideLoading()

            SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
            
        }
        
        func paymanet(DidFinshDownload Sender: [PaymentMethod]) {
            paymentMethods = Sender
        }
        
        func paymanet(didFaild error: NSError?) {
            SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
            SwiftLoading().hideLoading()

            
        }
        
        func paymanet(didGetUnexpectedResponse: String) {
            SwiftLoading().hideLoading()

            SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
            
            
        }
        
        
    }
    extension ProcessOrderViewController : UITableViewDelegate{
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            selectedIndex = indexPath
            
            tableView.reloadData()
        }
        
        
}
