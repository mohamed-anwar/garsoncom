//
//  ProcessOrderBackEnd.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/26/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


import SwiftyJSON
import ObjectMapper
import Alamofire

class  ProcessOrderBackEnd {
    
    private var network : Networking!
    weak var delegate  : ProcessOrderDelegate?
    init() {
        network = Networking(requestTimeout: 10)
    }
    func DownloadPayment() {
        network.manager.request(PaymentMethodUrl,headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            guard let  strongSelf = self else{return}
         
            let result = response.result
            var models = [PaymentMethod]()

            switch result {
            case .success(let value):
                let json = JSON(value)
                if let contentArray = json["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = PaymentMethod()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        models.append(model)
                    }
                    strongSelf.delegate?.paymanet(DidFinshDownload: models)
                } else {
                    strongSelf.delegate?.paymanet(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.paymanet(didFaild: error as NSError )
                break
            }
        }
    }
    func addOrder(parameters : [String : Any]) {
        network.manager.request(AddOrderUrl,method : .post ,parameters : parameters,encoding : JSONEncoding.default ,headers : [ acceptLanguage :  setLang() ,"Content-Type" : "application/json"]).debugLog().responseJSON {[weak self] response in
            guard let  strongSelf = self else{return}
            let result = response.result
            
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].string! == StatusMsgs.sucess{
                        strongSelf.delegate?.order(addSucessfully: json["message"].string!)
                } else {
                    strongSelf.delegate?.paymanet(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.paymanet(didFaild: error as NSError )
                break
            }
        }
    }
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
