//
//  ResturantListBackEndManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/30/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation

import SwiftyJSON
import ObjectMapper
class ResturantListBackEndManger {
    
    private var network : Networking!
    weak var delegate  : RestutantListDelegate?
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    func addOrRemoveFavRequest(id : Int) {
        let parameters : [String : Any] = [
            "id" : id ,
            "type" : "restaurant" ,
            "api_token" : userLoginToken!
        ]
        network.manager.request( AddFavUrl ,method : .post, parameters :  parameters , headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self]response in
            guard let strongSelf = self else{return}
            let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                
                if json["status"].string! == StatusMsgs.sucess{
                    strongSelf.delegate?.RestutantList(Success: json["message"].string!)
                }else{
                    strongSelf.delegate?.RestutantList(didGetUnexpectedResponse: json["message"].string!)
                }
                break
                
            case .failure(let error) :
                strongSelf.delegate?.RestutantList(didFaild: error as NSError)
                break
                
            }
        }
    }
    func DownloadHomeData( orderBy : String? = nil , distance : Int? = nil , categories : [Int]? = nil , cityId : String? = nil ) {
    var parameters = [String : Any]()
        if cityId != nil{
            parameters["city_id"] =  cityId
        }else if cityID != 0{
        parameters["city_id"] =  cityID!
        }else{
        parameters["lat"] =  lat!
        parameters["lng"] =  lng!
        }
    if userLoginToken != nil{
           parameters["api_token"] = userLoginToken!
    }
        if orderBy != nil {
            parameters["order_by"] = orderBy!

        }
        if distance != nil {
            parameters["distance"] = distance!
            
        }
        if categories != nil  {
                parameters["categories"] = categories!

        }
        
    network.manager.request( ResturantsUrl ,method : .post ,parameters: parameters, headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
        var resturantModels = [ResturantModel]()

        let result = response.result
        guard let strongSelf = self else { return }
        
        switch result {
            
        case .success(let value):
            let json = JSON(value)
            print(json)
            if let contentArray = json["data"].array {
                for contentObject in contentArray {
                    let contentDictionary = contentObject.dictionaryObject
                    let model = ResturantModel()
                    model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                    resturantModels.append(model)
                }
                strongSelf.delegate?.RestutantList(DidFinshDownload: resturantModels)
                
            } else {
                strongSelf.delegate?.RestutantList(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
            }
            break
        case .failure(let error) :
            strongSelf.delegate?.RestutantList(didFaild: error as NSError)
            break
            
        }
    }
}
    func DownloadTables(type : Int , cityId : String? = nil) {
        var parameters = [String : Any]()
        if cityId != nil {
            parameters["city_id"] =  cityId
        }else if cityID != 0{
            parameters["city_id"] =  cityID!
        }else{
            parameters["lat"] =  lat!
            parameters["lng"] =  lng!
        }
        if userLoginToken != nil{
            parameters["api_token"] = userLoginToken!
        }
        parameters["table"] = 1
        parameters["type"] = type
        network.manager.request( ResturantsUrl ,method : .post ,parameters: parameters, headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            var resturantModels = [ResturantModel]()
            
            let result = response.result
            guard let strongSelf = self else { return }
            
            switch result {
                
            case .success(let value):
                let json = JSON(value)
                print(json)
                if let contentArray = json["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = ResturantModel()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        resturantModels.append(model)
                    }
                    strongSelf.delegate?.RestutantList(DidFinshDownload: resturantModels)
                    
                } else {
                    strongSelf.delegate?.RestutantList(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.RestutantList(didFaild: error as NSError)
                break
                
            }
        }
    }
        deinit {
            network.manager.session.invalidateAndCancel()
        }
}
