//
//  ResturantListTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/12/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages
import D2PCurvedModal
import PopupDialog
import  PMSuperButton
import DropDown


class ResturantListTableViewController: UIViewController , UITableViewDataSource ,UITableViewDelegate {
    var filteredData = [ResturantModel]()
    fileprivate let searchController = UISearchController(searchResultsController: nil)
    
    
    
    @IBOutlet weak var ChooseGovernmentButton: PMSuperButton!
    @IBOutlet weak var ChooseCityButton: PMSuperButton!
    var dataManger : ChooseGovernmentNetworkManger!
    var governments = [GovernmentModel]()
    var cities = [CityModel]()
    var downloaded = false
    var tableType : Int?
    let setGovernment = DropDown()
    let setCity = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.setGovernment,
            self.setCity
            
        ]
    }()
    
    
    
    
    var resturants = [ResturantModel]()
    var favIndex : IndexPath!
    let percentDrivenTransition = D2PCurvedModalPercentDrivenTransition()
    let transition = D2PCurvedModalTransition()
    var selectedLocation : Int!
    var selectedIndexLocation : Int!
    var selectedIndexGeneral : Int!
    var selectedIndexCegories = [Int]()
    var selectedCategoriesids = [Int]()


    


    @IBAction func filterBarButton(_ sender: UIBarButtonItem) {
        if let tabBarVC = storyboard?.instantiateViewController(withIdentifier:FilterTabBarID) as? UITabBarController {
 
        
            let modalVC = D2PCurvedModal(nibName: "D2PCurvedModal", bundle: Bundle(for: D2PCurvedModal.self))
            
            modalVC.setUpViewOf(viewController: tabBarVC)
            modalVC.modalTitle = "Filter"
            modalVC.containerHeight = 200
            modalVC.delegate = self
            modalVC.transitioningDelegate = self
            modalVC.closeBtnBgColor = greenColor
            modalVC.bgColor = greenColor
            modalVC.modalTitleColor = .white
            percentDrivenTransition.attachToViewController(viewController: modalVC)
            
            present(modalVC, animated: true, completion: nil)
            
        }
        
    
    }
    @IBOutlet weak var tableView : UITableView!
    var backEnd : ResturantListBackEndManger!
    var refreshTVControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        //============================================================
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Search"
        
        self.searchController.searchBar.barStyle = .default
        
        self.searchController.searchBar.delegate = self
        self.definesPresentationContext = true
        self.navigationItem.searchController = searchController
        
        
        
        dataManger = ChooseGovernmentNetworkManger()
        dataManger.delegate = self
        dataManger.DownloadGovernmentData()
        ChooseGovernmentButton.showLoader()
        setupSetGovernmentDropDown()
        setupSetCityDropDown()
        ChooseGovernmentButton.addTarget(self, action:#selector(showDropDownForgovernment) , for: .touchUpInside)
        ChooseCityButton.addTarget(self, action:#selector(showDropDownForCity) , for: .touchUpInside)
        //============================================================
        refreshTVControl = UIRefreshControl()
        refreshTVControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "Pull to refresh"))
        refreshTVControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.refreshControl = refreshTVControl

        backEnd = ResturantListBackEndManger()
        backEnd.delegate = self
        backEnd.DownloadHomeData()
        
        SwiftLoading().showLoading()
        let restCellNib = UINib(nibName: "ResturantListCellTableViewCell", bundle: nil)
        tableView.register(restCellNib, forCellReuseIdentifier: restListCellId)
    }
    @objc func showDropDownForgovernment() {
        if downloaded{
            setGovernment.show()
        }else{
            dataManger.DownloadGovernmentData()
        }
    }
    @objc func showDropDownForCity() {
        setCity.show()
    }
    func setupSetGovernmentDropDown() {
        setGovernment.anchorView =  ChooseGovernmentButton
        setGovernment.bottomOffset = CGPoint(x: 0, y:  ChooseCityButton.bounds.height)
        setGovernment.selectionAction = { [unowned self] (index, item) in
            self.ChooseGovernmentButton.setTitle(item, for: .normal)
            self.setCity.dataSource.removeAll()
            self.cities.removeAll()
            self.setCity.reloadAllComponents()
            self.ChooseCityButton.showLoader()
            self.dataManger.DownloadCityData(id: String(self.governments[index]._id))
        }
    }
    func setupSetCityDropDown() {
        setCity.anchorView =  ChooseCityButton
        setCity.bottomOffset = CGPoint(x: 0, y: ChooseCityButton.bounds.height)
        setCity.selectionAction = { [unowned self] (index, item) in
            self.ChooseCityButton.setTitle(item, for: .normal)
            self.backEnd.DownloadHomeData(orderBy: nil, distance: nil, categories: nil, cityId: String(self.cities[index]._id))
            SwiftLoading().showLoading()
        }
    }
    
    @objc func refresh(){
        backEnd.DownloadHomeData()
        SwiftLoading().showLoading()
        
    }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isFiltering(){
            return filteredData.count
        }else{
            return resturants.count
            
        }
    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: restListCellId, for: indexPath) as! ResturantListCellTableViewCell
        if isFiltering(){
            cell.SetCell(rest: filteredData[indexPath.row])
            cell.setCellState(rest: filteredData[indexPath.row])
        }else{
            cell.SetCell(rest: resturants[indexPath.row])
            cell.setCellState(rest: resturants[indexPath.row])

        }
        
        cell.heartButton.addTarget(self, action: #selector(favButtonAction), for: .touchUpInside)
        return cell
    }
    
    
    @objc func favButtonAction(sender : Any){
        let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView?.indexPathForRow(at: position)
        {
            favIndex = indexPath
            if isFiltering(){
                backEnd.addOrRemoveFavRequest(id:filteredData[indexPath.row]._id)

            }else{
                backEnd.addOrRemoveFavRequest(id:resturants[indexPath.row]._id)
                
            }
            
        }
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if resturants[indexPath.row]._active == false{
//        let tab = storyboard?.instantiateViewController(withIdentifier: RestTabBarId) as! UITabBarController
//        let resturantCategotyTB = tab.viewControllers![0] as! ResturantCategoryTableViewController
//        resturantCategotyTB.id = resturants[indexPath.row]._id
//        tab.navigationItem.title = resturants[indexPath.row]._name
//        let restDVC = tab.viewControllers![1] as! ResturantDetalsViewController
//        restDVC.resturant = resturants[indexPath.row]
//        let table = tab.viewControllers![2] as! TableTimePickerViewController
//        table.paramter["restaurant_id"] = resturants[indexPath.row]._id
//        if !resturants[indexPath.row]._haveTables{
//            tab.viewControllers?.remove(at: 2)
//        }
//            navigationController?.pushViewController(tab, animated: true)
        
        let restDVC = storyboard?.instantiateViewController(withIdentifier: RestTabBarId) as! ResturantDetalsViewController
        if isFiltering(){
            restDVC.resturant = filteredData[indexPath.row]
        }else{
            restDVC.resturant = resturants[indexPath.row]
        }
        navigationController?.pushViewController(restDVC, animated: true)
        
//        }
    }


}
extension ResturantListTableViewController : RestutantListDelegate{
    func RestutantList(DidFinshDownload Sender: [ResturantModel]) {
            resturants = Sender
            if favIndex != nil {
                tableView.reloadRows(at: [favIndex], with: .automatic)
            }else{
                tableView.reloadData()
            }
            SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()
        
    }
    
    func RestutantList(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()



    }
    
    func RestutantList(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()



    }
    
    func RestutantList(Success msg: String) {
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        resturants.removeAll()
        backEnd.DownloadHomeData()
        SwiftLoading().showLoading()
        
    }
    
    
}
    extension ResturantListTableViewController: UIViewControllerTransitioningDelegate {
        
        func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
            
            if presented is D2PCurvedModal {
                transition.opening = true
                return transition
            }
            
            return nil
            
        }
        
        func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
            
            if dismissed is D2PCurvedModal {
                transition.opening = false
                return transition
            }
            
            return nil
            
            
        }
        
        func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
            
            return percentDrivenTransition.transitionInProgress ? percentDrivenTransition : nil
            
        }
        
    }
    
    extension ResturantListTableViewController: D2PCurvedModalDelegate {
        
        func modalWillOpen(modalVC: D2PCurvedModal) {
            
            if let tabBarVC = modalVC.containerVC as? UITabBarController {
                
                if let locationVC = tabBarVC.viewControllers![0] as? LocationFilterViewController {
                    locationVC.selectedItem = selectedLocation ?? 0
                    locationVC.indexSelected = selectedIndexLocation ?? 10
                }
                if let categoryFliterVC = tabBarVC.viewControllers![1] as? CategoryFilteriController {
                    categoryFliterVC.selectedItems = selectedIndexCegories
                    
                     categoryFliterVC.selectedCategoriesids = selectedCategoriesids

                }
                if let generalVC = tabBarVC.viewControllers![2] as? GeneralFilterViewController {
                    generalVC.indexSelected = selectedIndexGeneral ?? 10
                }

          }
            
        }
        
        func modalWillClose(modalVC: D2PCurvedModal) {
            
            if let tabBarVC = modalVC.containerVC as? UITabBarController {
                if let locationVC = tabBarVC.viewControllers![0] as? LocationFilterViewController {
                    if locationVC.selectedItem == 0 {
                        selectedLocation  = nil
                        selectedIndexLocation = nil
                    }else{
                       selectedLocation = locationVC.selectedItem
                        selectedIndexLocation  = locationVC.indexSelected

                    }
                }
                if let categoryFliterVC = tabBarVC.viewControllers![1] as? CategoryFilteriController {
                   selectedIndexCegories = categoryFliterVC.selectedItems
                    selectedCategoriesids = categoryFliterVC.selectedCategoriesids
                }
                var keys = [
                "delivery_cost" ,
                "delivery_time" ,
                "name" ,
                "bety",
                "orders_count"]
                if let generalVC = tabBarVC.viewControllers![2] as? GeneralFilterViewController {
                    if generalVC.indexSelected == 10 {
                        selectedIndexGeneral  = nil
                    }else{
                        selectedIndexGeneral = generalVC.indexSelected
                    }
                }
         
                backEnd.DownloadHomeData(orderBy:  (selectedIndexGeneral != nil) ? keys[selectedIndexGeneral] : nil , distance:  (selectedLocation != nil) ? selectedLocation! : nil , categories: selectedCategoriesids)
                SwiftLoading().showLoading()
                
            }
            
        }
        
}
extension ResturantListTableViewController : ChooseGovernmentDelegate{
    func GetGovernmentCities(didFinishDownload Sender: [CityModel]?) {
        ChooseCityButton.hideLoader()
        self.setCity.dataSource.removeAll()
        self.cities.removeAll()
        self.setCity.reloadAllComponents()
        self.ChooseCityButton.setTitle(ChooseCityButtonTitle, for: .normal)
        cities = Sender!
        Sender?.forEach({
            setCity.dataSource.append($0._name)
        })
    }
    func GetGovernment(didFinishDownload Sender: [GovernmentModel]?) {
        ChooseGovernmentButton.hideLoader()
        downloaded = true
        governments = Sender!
        Sender?.forEach({
            setGovernment.dataSource.append($0._name)
        })
    }
    func GetGovernment(didFaild error: Error?) {
        ChooseCityButton.hideLoader()
        ChooseGovernmentButton.hideLoader()
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
    }
    func GetGovernment(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body:didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
    }
}
extension ResturantListTableViewController : UISearchBarDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        //Show Cancel
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.tintColor = .white
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        //Filter function
        self.filterFunction(searchText: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //Hide Cancel
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
        guard let term = searchBar.text , term.isEmpty == false else {
            
            //Notification "White spaces are not permitted"
            return
        }
        
        //Filter function
        self.filterFunction(searchText: term)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        //Hide Cancel
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = String()
        searchBar.resignFirstResponder()
        
        
        
        
        //Filter function
        self.filterFunction(searchText: searchBar.text!)
    }
    
    func filterFunction(searchText: String){
        filteredData = resturants.filter({( data : ResturantModel)
            -> Bool in
            return data._name.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()

    }
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
}



