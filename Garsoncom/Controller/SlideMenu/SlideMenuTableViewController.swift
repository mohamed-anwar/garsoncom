//
//  SlideMenuTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/13/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SideMenu
let slideMenuImages = [    #imageLiteral(resourceName: "home-1"),#imageLiteral(resourceName: "profile-1"),#imageLiteral(resourceName: "table list-1"),#imageLiteral(resourceName: "table list-1"),#imageLiteral(resourceName: "table list-1"),#imageLiteral(resourceName: "discount"),#imageLiteral(resourceName: "restaurant-1"),#imageLiteral(resourceName: "parties-1"),#imageLiteral(resourceName: "my order-1"),#imageLiteral(resourceName: "heartWhite"),#imageLiteral(resourceName: "best client"),#imageLiteral(resourceName: "language-1"),#imageLiteral(resourceName: "information-1"),#imageLiteral(resourceName: "logout-1")]
class SlideMenuTableViewController: UIViewController {
    
    @IBOutlet var mainView: SlideMenu!
    override func viewDidLoad() {
        super.viewDidLoad()
        let nibCell = UINib(nibName: "SlideMenuTableViewCell", bundle: nil)
        mainView.tableView.register(nibCell, forCellReuseIdentifier: "SlideMenuTableViewCellID")
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if userLoginToken != nil {
            titlesOfSlideMenu[13] = NSLocalizedString("Logout", comment: "")
            mainView.stackHightConstraint.constant = 182
            mainView.ConfigureView(name: userName!, image: userImage!, email: userEmail!)
            makeBorderToView(mainView.userImageView, boderWidth: 1, cornerRadius: mainView.userImageView.frame.width / 2, borderColor: .clear, maskToBounds: true)
            mainView.mainStacView.isHidden = false

            
        }else{
            titlesOfSlideMenu[13] = NSLocalizedString("Log in", comment: "")

            mainView.stackHightConstraint.constant = 0
            mainView.mainStacView.isHidden = true
        }
    }
    
}

extension SlideMenuTableViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0 :
//        
            let vc = storyboard?.instantiateViewController(withIdentifier: HomeTableViewControllerID)
            navigationController?.pushViewController(vc!, animated: true)
            print("home")
        case 1 :
            if userLoginToken != nil{
                let vc = storyboard?.instantiateViewController(withIdentifier: ProfileViewControllerID)
                navigationController?.pushViewController(vc!, animated: true)

               
                print("profile")
                
            }else{

                SideMenuManager.defaultManager.menuLeftNavigationController?.presentViewController(RegisterNavID)

                print("login")
            }
        case 2 :
            print("Resturant List")
            let vc = storyboard?.instantiateViewController(withIdentifier:ResturantListTableViewControllerID)
            navigationController?.pushViewController(vc!, animated: true)

        case 3 :
            print("Tabel List")
            let vc = storyboard?.instantiateViewController(withIdentifier:TableListViewControllerID)
            navigationController?.pushViewController(vc!, animated: true)
        case 4 :
            print(" My Tabel")
            if userLoginToken != nil{
            let vc = storyboard?.instantiateViewController(withIdentifier: RequestedTableTableViewControllerID)
            navigationController?.pushViewController(vc!, animated: true)
            }else{
                SideMenuManager.defaultManager.menuLeftNavigationController?.presentViewController(RegisterNavID)
            }
        case 5 :
            let vc = storyboard?.instantiateViewController(withIdentifier: OffersTableViewControllerID)
            vc?.title = titlesOfSlideMenu[indexPath.row]

            navigationController?.pushViewController(vc!, animated: true)
            
        case 6 :
            if userLoginToken != nil{
            let vc = storyboard?.instantiateViewController(withIdentifier: SuggestMealTableViewControllerID)
                vc?.title = titlesOfSlideMenu[indexPath.row]
            navigationController?.pushViewController(vc!, animated: true)
            }else{
                SideMenuManager.defaultManager.menuLeftNavigationController?.presentViewController(RegisterNavID)
            }
            
        case 7 :
            
            if userLoginToken != nil{
                let vc = storyboard?.instantiateViewController(withIdentifier: AddPartyViewControllerID)
                vc?.title = titlesOfSlideMenu[indexPath.row]

                navigationController?.pushViewController(vc!, animated: true)
            }else{
                SideMenuManager.defaultManager.menuLeftNavigationController?.presentViewController(RegisterNavID)
            }
            print("Parties")
        case 8 :
            if userLoginToken != nil{
                print("My Order")
                
                let vc = storyboard?.instantiateViewController(withIdentifier: MyOrderTabBarID)
                navigationController?.pushViewController(vc!, animated: true)
            }else{
                print("login")
            }
        case 9 :
            if userLoginToken != nil{
                print("Favourite")
                let vc = storyboard?.instantiateViewController(withIdentifier: FavTabBarControllerID)
                navigationController?.pushViewController(vc!, animated: true)
                
            }else{
                SideMenuManager.defaultManager.menuLeftNavigationController?.presentViewController(RegisterNavID)

                print("login")
            }
        case 10 :
            let vc = storyboard?.instantiateViewController(withIdentifier: BestClientTableViewCellID)
            navigationController?.pushViewController(vc!, animated: true)
            print("Best Cilet")
        case 11:
            changeAppLanguge()
        case 12:
            print("Info")
            
        case 13:
            if userLoginToken != nil {
            userLoginToken = nil
                dismiss(animated: true, completion: nil)
            }else{
                SideMenuManager.defaultManager.menuLeftNavigationController?.presentViewController(RegisterNavID)

            }
        default:
            break
        }
    }
    
}
extension SlideMenuTableViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesOfSlideMenu.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SlideMenuTableViewCellID", for: indexPath) as! SlideMenuTableViewCell
        cell.configureCell(image: slideMenuImages[indexPath.row], name: titlesOfSlideMenu[indexPath.row])
        return cell
    }
}
