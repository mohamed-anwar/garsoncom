//
//  LocationViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/9/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import AVFoundation
import MapKit
import CoreLocation

class LocationViewController: UIViewController,  CLLocationManagerDelegate  {
    var locationManager = CLLocationManager()
    var player : AVPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        locateMe()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if cityID != 0 {
            presentViewController(homeNavID)
            
        }else{
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                AskForLocation()
            case .authorizedAlways, .authorizedWhenInUse:
                //                locateMe()
                print("Accepted")
            }
        } else {
            presentViewController(ChooseLocationNavID)
            }
            
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     //   PlayVideo()
        
    }
    
  /*  func PlayVideo() {
        let videoURL = Bundle.main.url(forResource: "gif", withExtension: "mp4")
        player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view!.bounds
        view!.layer.addSublayer(playerLayer)
        player.play()
    }
 */
    
    
    
    
    
    
    
    
    func AskForLocation(){
        let actions : [((UIAlertAction) -> ())] = [
        { [weak self] _ in
            guard let strongSelf = self else { return }
            let actionsSub : [((UIAlertAction) -> ())] = [
            {_ in
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL, options: [:], completionHandler: ({ _ in
                }))
                },{ [weak self] _ in
                    guard let strongSelf = self else { return }
                    strongSelf.presentViewController(ChooseLocationNavID)
                }
            ]
            
            strongSelf.showAlert(alertTitle: aletAskString, alertMsg: askForLocationString, buttonTitles: [SettingsTitle,CancelButtonTitle], actions: actionsSub)
            
            
            },
        {  [weak self] _ in
            guard let strongSelf = self else { return }
            strongSelf.presentViewController(ChooseLocationNavID)
            }
        ]
        showAlert(alertTitle: aletAskString, alertMsg: askForLocationString, buttonTitles: buttonArray, actions: actions)
    }
    
    func locateMe() {
     
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        manager.stopUpdatingLocation()
        
        let coordinations = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        lng = coordinations.longitude
        lat = coordinations.latitude
        print(lng!,lat!)
        presentViewController(homeNavID)
    }
}

