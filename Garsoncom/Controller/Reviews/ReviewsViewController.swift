//
//  ReviewsViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/17/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages

class ReviewsViewController: UIViewController {
    var _view : CommentView{
        return view as! CommentView
    }
    var restaurant = ResturantModel()
    var responseState = ResponseState.loading

    var reviews = [ReviewModel]()
    var backEndManger : ResturantDetailBackENdManger?
    var id : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        backEndManger = ResturantDetailBackENdManger()
        backEndManger?.delegate = self
        backEndManger?.DownloadData(restaurantId: restaurant._id)
        let restDetailCellNib = UINib(nibName: "ResturantDetailTableViewCell", bundle: nil)
        _view.tableView.register(restDetailCellNib, forCellReuseIdentifier: RestDetailCellID)
        let loadingCellNib = UINib(nibName: TableViewCelldentifiers.loadingCell, bundle: nil)
        _view.tableView.register(loadingCellNib, forCellReuseIdentifier: TableViewCelldentifiers.loadingCell)
        _view.sendButton.addTarget(self, action:#selector(AddReview) , for: .touchUpInside)
        _view.commentTextF.delegate = _view
        if restaurant._isUser{
            _view.sendButton.isHidden = true
        }else{
            _view.sendButton.isHidden = false

        }
        

    }
    @objc func AddReview(){
        backEndManger?.addReview(id: restaurant._id, desc: _view.commentTextF.text!)
        SwiftLoading().showLoading()
    }


   

}
extension ReviewsViewController : ResturantDetaislDelegate{
    func ResturantDetail(didFinishDownload review: ReviewModel?, msg: String) {
        reviews.append(review!)
        _view.tableView.reloadData()
        _view.commentTextF.text = ""
        _view.sendButton.isEnabled = false
        SwiftLoading().hideLoading()
        
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
    }
    
  
    func ResturantDetail(didFinishDownload Reviews: [ReviewModel]?) {
        reviews = Reviews!
        if reviews.count > 0{
            responseState = ResponseState.result
        }else{
            responseState = ResponseState.defualt
        }
        SwiftLoading().hideLoading()

        _view.tableView.reloadData()
    }
    func ResturantDetail(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        SwiftLoading().hideLoading()
    }
    func ResturantDetail(didGetUnexpectedResponse: String) {
        SwiftLoading().hideLoading()
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
    }
    func ResturantDetail(Success msg: String) {
        _view.tableView.reloadData()
        _view.commentTextF.text = ""
        _view.sendButton.isEnabled = false
        backEndManger?.DownloadData(restaurantId: id!)
        SwiftLoading().showLoading()

        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
       
    }
    
}
extension ReviewsViewController : UITableViewDelegate{
    
}
extension ReviewsViewController  : UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch responseState {
        case ResponseState.defualt , ResponseState.loading:
            return 1
        case ResponseState.result :
            return (reviews.count)
        default :
            return 0
            
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch responseState {
    
        case ResponseState.result :
            let cell = tableView.dequeueReusableCell(withIdentifier: RestDetailCellID) as! ResturantDetailTableViewCell
            cell.ConfigureCell(name: reviews[indexPath.row]._client._name , image: reviews[indexPath.row]._client._image, desc: reviews[indexPath.row]._desc)
            return cell
        default :
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCelldentifiers.loadingCell,for: indexPath)
            let spinner = cell.viewWithTag(100) as! UIActivityIndicatorView
            spinner.startAnimating()
            return cell
        }
        
    }
}
