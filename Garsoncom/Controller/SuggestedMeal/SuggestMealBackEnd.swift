//
//  SuggestMealBackEnd.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation

import Foundation
import SwiftyJSON
import ObjectMapper
class SuggestMealBackEnd{
    private var network : Networking!
    weak var delegate : SuggestMealDelegate?
    init() {
        network = Networking(requestTimeout: 10)
    }
    func uploadPost(parameters : [String:Any] , image : Data?) {
        
        
        network.manager.upload(multipartFormData: { multipartFormData in
            if let imageData = image{
                multipartFormData.append(imageData, withName: "image", fileName: "img", mimeType: "image/jpeg")
                
            }
            for (key , value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },to : AddPostUrl, encodingCompletion: { [weak self] encodingResult in
            guard let  strongSelf = self else{return}
            
            switch encodingResult {
            case .success(let upload,_,_) :
                upload.debugLog().responseJSON {  response in
                    
                    switch response.result {
                    case .success(let value) :
                        let json = JSON(value)
                        if json["status"].string! == StatusMsgs.sucess{
                            let post = Post()
                            post.mapping(map: Map(mappingType: .fromJSON, JSON: json["data"].dictionaryObject!))
                            strongSelf.delegate?.SuggestMeal(didaAddPost: json["message"].string! , post : post)
                
                        }else{
                            strongSelf.delegate?.SuggestMeal(didGetUnexpectedResponse: json["message"].string!)
                        }
                        break
                    case .failure(let error) :
                        strongSelf.delegate?.SuggestMeal(didFaild: error)
                    }
                }
            case .failure(let error) :
                strongSelf.delegate?.SuggestMeal(didFaild: error)
            }
        })
    }
    func DownloadData() {
        network.manager.request( GetAllPostsUrl , method : .post , parameters : ["api_token" : userLoginToken!], headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            var models = [Post]()
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                if let contentArray = json["data"]["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = Post()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        models.append(model)
                    }
                    strongSelf.delegate?.SuggestMeal(DidFinshDownload: models)
                } else {
                    strongSelf.delegate?.SuggestMeal(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.SuggestMeal(didFaild: error)
                break
                
            }
        }
    }
    func VoteRequest(id: Int) {
        network.manager.request( VoteUrl , method : .post , parameters : ["api_token" : userLoginToken!,"post_id" : id], headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].string! == StatusMsgs.sucess{
                    strongSelf.delegate?.SuggestMeal(didVote: json["message"].string!)

                } else {
                    strongSelf.delegate?.SuggestMeal(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.SuggestMeal(didFaild: error)
                break
                
            }
        }
    }
    func DeletePostRequest(id: Int) {
        network.manager.request( DeletePostUrl , method : .post , parameters : ["api_token" : userLoginToken!,"post_id" : id], headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].string! == StatusMsgs.sucess{
                    strongSelf.delegate?.SuggestMeal(didDeletePost : json["message"].string!)
                    
                } else {
                    strongSelf.delegate?.SuggestMeal(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.SuggestMeal(didFaild: error)
                break
                
            }
        }
    }
    
    
    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
