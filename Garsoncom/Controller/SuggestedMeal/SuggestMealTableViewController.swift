//
//  SuggestMealTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/30/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages

class SuggestMealTableViewController: UITableViewController {
    var imagePickerController: UIImagePickerController!
    var mainImage : Data!
    var pimage : UIImage!
    var backend : SuggestMealBackEnd?
    var posts = [Post]()
    var favIndex : IndexPath!
    var deletedIndex : IndexPath!
    var refreshTVControl: UIRefreshControl!
    
    
    @objc func refresh() {
        backend?.DownloadData()
        SwiftLoading().showLoading()
        
    }
   
    
    @objc func favButtonAction(sender : Any){
        let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView?.indexPathForRow(at: position)
        {
            favIndex = indexPath
            backend?.VoteRequest(id: posts[indexPath.row]._id)
            SwiftLoading().showLoading()
        }
    }
    @objc func deleteButtonAction(sender : Any){
        let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView?.indexPathForRow(at: position)
        {
            let actions : [((UIAlertAction) -> ())] = [
            { [weak self] _ in
                self?.deletedIndex = indexPath
                self?.backend?.DeletePostRequest(id: (self?.posts[indexPath.row]._id)!)
                SwiftLoading().showLoading()
                
                },{ [weak self] _ in
                    self!.dismiss(animated: true, completion: nil)
                }
            ]
            
          showAlert(alertTitle: aletAskString, alertMsg: askForDeleteString, buttonTitles: [OkayButtonTitle,CancelButtonTitle], actions: actions)
            
        }
    }
    
    
    
    
    func registerNibs() {
        var nib = UINib(nibName: "SuggestMealTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "SuggestMealTableViewCellID")
        nib = UINib(nibName: "SuggestMealHeader", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "SuggestMealHeaderID")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshTVControl = UIRefreshControl()
        refreshTVControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "Pull to refresh"))
        refreshTVControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.refreshControl = refreshTVControl
        registerNibs()
        backend = SuggestMealBackEnd()
        backend?.delegate = self
        backend?.DownloadData()
        SwiftLoading().showLoading()
        
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 208
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SuggestMealHeaderID") as! SuggestMealHeader
        header.captureImageButton.addTarget(self, action: #selector(PickupImage(_:)), for: .touchUpInside)
        header.delegate = self
        return header
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return posts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestMealTableViewCellID", for: indexPath) as! SuggestMealTableViewCell
        cell.configure(post: posts[indexPath.row])
        cell.voteButton.addTarget(self, action: #selector(favButtonAction(sender:)), for: .touchUpInside)
        cell.deletePostButton.addTarget(self, action: #selector(deleteButtonAction(sender:)), for: .touchUpInside)

        
        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("any thing")
        let vc = storyboard?.instantiateViewController(withIdentifier: CommentTableViewControllerID) as! CommentTableViewController
        vc.post = posts[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
    }
    //    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
    //        if posts[indexPath.row]._is_my_post{
    //            return .delete
    //
    //        }else{
    //            return .none
    //        }
    //    }
    //    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    //        if posts[indexPath.row]._is_my_post{
    //        if editingStyle == .delete {
    //
    //            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
    //            }
    //
    //        }
    //    }
    
    
    
}
extension SuggestMealTableViewController:AddPostDelegate{
    func didAddPost(mealName: String, post: String) {
        if mainImage != nil{
            backend?.uploadPost(parameters: ["title" : mealName ,"description" : post , "api_token" : userLoginToken!], image: mainImage)
            SwiftLoading().showLoading()
            
        }else{
            SwiftMessages.showMessage(title: "", body: NSLocalizedString("plese select meal image", comment: ""), type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
            
        }
    }
}
extension SuggestMealTableViewController : SuggestMealDelegate{
    func SuggestMeal(DidFinshDownload Sender: [Post]) {
        posts = Sender
        tableView.reloadData()
        refreshControl?.endRefreshing()
        SwiftLoading().hideLoading()
        
        
    }
    
    func SuggestMeal(didaAddPost msg: String , post : Post) {
        tableView.reloadSections(IndexSet.init(integersIn: 0...0), with: .none)
        posts.insert(post, at: 0)
        tableView.reloadData()
        SwiftLoading().hideLoading()


        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
    
    func SuggestMeal(didFaild error: Error?) {
        refreshControl?.endRefreshing()

        SwiftLoading().hideLoading()
        
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
    
    func SuggestMeal(didGetUnexpectedResponse: String) {
        refreshControl?.endRefreshing()

        SwiftLoading().hideLoading()
        
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
    func SuggestMeal(didVote msg: String) {
        if favIndex != nil {
            posts[favIndex.row]._is_vote = !posts[favIndex.row]._is_vote
            if  posts[favIndex.row]._is_vote {
                posts[favIndex.row]._votes_count += 1
            }else{
                posts[favIndex.row]._votes_count -= 1
                
            }
            tableView.reloadRows(at: [favIndex], with: .none)
        }
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
        SwiftLoading().hideLoading()
        
        
    }
    func SuggestMeal(didDeletePost msg: String) {
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        posts.remove(at: deletedIndex.row)
        tableView.deleteRows(at: [deletedIndex], with: UITableViewRowAnimation.middle)
        
        SwiftLoading().hideLoading()
        
        
        
    }
    
    
}
extension SuggestMealTableViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @objc func PickupImage(_ sender: UIButton) {
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            pimage = pickedImage
        }else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            pimage = image
            
        }
        mainImage =  pimage.ResizeImage(targetSize: CGSize(width: 800.0, height: 800.0))
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
