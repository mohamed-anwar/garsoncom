//
//  FavResturantCollectionView.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/28/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages


class FavResturantCollectionView: UITableViewController {
    var resturants = [ResturantModel]()
    var backEnd : FavResturantBackEndManger!
    var refreshTVControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshTVControl = UIRefreshControl()
        refreshTVControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "Pull to refresh"))
        refreshTVControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView?.refreshControl = refreshTVControl

        backEnd = FavResturantBackEndManger()
        backEnd.delegate = self
        backEnd.DownloadData()
        SwiftLoading().showLoading()
        let restCellNib = UINib(nibName: "ResturantListCellTableViewCell", bundle: nil)
        tableView.register(restCellNib, forCellReuseIdentifier: restListCellId)
   
    }
    @objc  func refresh(){
        backEnd.DownloadData()
        SwiftLoading().showLoading()
        
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resturants.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: restListCellId, for: indexPath) as! ResturantListCellTableViewCell
        cell.SetCell(rest: resturants[indexPath.row])
        cell.heartButton.addTarget(self, action: #selector(minus), for: .touchUpInside)
        return cell
    }
    
    
    
    @objc func minus(sender : Any){
        let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView?.indexPathForRow(at: position)
        {
            backEnd.addOrRemoveFavRequest(id:resturants[indexPath.row]._id)
            
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        if resturants[indexPath.row]._active == false{
        //        let tab = storyboard?.instantiateViewController(withIdentifier: RestTabBarId) as! UITabBarController
        //        let resturantCategotyTB = tab.viewControllers![0] as! ResturantCategoryTableViewController
        //        resturantCategotyTB.id = resturants[indexPath.row]._id
        //        tab.navigationItem.title = resturants[indexPath.row]._name
        //        let restDVC = tab.viewControllers![1] as! ResturantDetalsViewController
        //        restDVC.resturant = resturants[indexPath.row]
        //        let table = tab.viewControllers![2] as! TableTimePickerViewController
        //        table.paramter["restaurant_id"] = resturants[indexPath.row]._id
        //        if !resturants[indexPath.row]._haveTables{
        //            tab.viewControllers?.remove(at: 2)
        //        }
        //            navigationController?.pushViewController(tab, animated: true)
        
        let restDVC = storyboard?.instantiateViewController(withIdentifier: RestTabBarId) as! ResturantDetalsViewController
        restDVC.resturant = resturants[indexPath.row]
        navigationController?.pushViewController(restDVC, animated: true)
        
        //        }
    }
   
  

}

extension FavResturantCollectionView : FavResturantDelegate {
    func FavResturant(DidFinshDownload Sender: [ResturantModel]) {
        resturants.removeAll()
        resturants = Sender
        tableView?.reloadData()
        SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()

    }
    
    func FavResturant(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        tableView?.reloadData()
        SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()




    }
    
    func FavResturant(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        tableView?.reloadData()
        SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()




    }
    
    func FavResturant(Success msg: String) {
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        resturants.removeAll()
        backEnd.DownloadData()
        tableView?.reloadData()
        SwiftLoading().showLoading()
        refreshTVControl.endRefreshing()


    }
    
    
    
}
