//
//  FavResturantBackEnd.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/28/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

//

import SwiftyJSON
import ObjectMapper
class FavResturantBackEndManger {
    
    private var network : Networking!
    weak var delegate  : FavResturantDelegate?
    
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    
   @objc func addOrRemoveFavRequest(id : Int) {
        let parameters : [String : Any] = [
            "id" : id ,
            "type" : "restaurant" ,
            "api_token" : userLoginToken!
            ]
        network.manager.request( AddFavUrl ,method : .post, parameters :  parameters , headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self]response in
            guard let strongSelf = self else{return}
                        let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                if json["status"].string! == StatusMsgs.sucess{
                        strongSelf.delegate?.FavResturant(Success: json["message"].string!)
                }else{
                    strongSelf.delegate?.FavResturant(didGetUnexpectedResponse:  json["message"].string!)
                }
                break
                
            case .failure(let error) :
                strongSelf.delegate?.FavResturant(didFaild: error as NSError)
                break
                
            }
        }
    }
    func DownloadData() {
        network.manager.request( GetFavouratRestirantsUrl , method : .post , parameters : ["api_token" : userLoginToken!], headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            var models = [ResturantModel]()
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if let contentArray = json["data"]["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = ResturantModel()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        models.append(model)
                    }
                    strongSelf.delegate?.FavResturant(DidFinshDownload: models)
                } else {
                    strongSelf.delegate?.FavResturant(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.FavResturant(didFaild: error as NSError)
                break
                
            }
        }
    }
    
    
    
    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
