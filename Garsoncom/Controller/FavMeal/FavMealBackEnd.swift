//
//  FavMealBackEnd.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/28/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


import SwiftyJSON
import ObjectMapper
class FavMealBackEndManger {
    
    private var network : Networking!
    weak var delegate  : FavMealDelegate?
    
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    
     func addOrRemoveFavRequest(id : Int) {
        let parameters : [String : Any] = [
            "id" : id ,
            "type" : "product" ,
            "api_token" : userLoginToken!
        ]
        network.manager.request( AddFavUrl ,method : .post, parameters :  parameters , headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self]response in
            guard let strongSelf = self else{return}
            let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                if json["status"].string! == StatusMsgs.sucess{
                    strongSelf.delegate?.FavMeal(Success: json["message"].string!)
                }else{
                    strongSelf.delegate?.FavMeal(didGetUnexpectedResponse: json["message"].string!)
                }
                break
                
            case .failure(let error) :
                strongSelf.delegate?.FavMeal(didFaild: error as NSError)
                break
                
            }
        }
    }
    func DownloadData() {
        network.manager.request( GetFavMealURL , method : .post , parameters : ["api_token" : userLoginToken!], headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            var models = [ProductModel]()
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if let contentArray = json["data"]["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = ProductModel()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        models.append(model)
                    }
                    strongSelf.delegate?.FavMeal(DidFinshDownload: models)
                } else {
                    strongSelf.delegate?.FavMeal(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.FavMeal(didFaild: error as NSError)
                break
                
            }
        }
    }
    
    
    
    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
