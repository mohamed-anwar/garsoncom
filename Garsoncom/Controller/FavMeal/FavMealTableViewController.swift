//
//  FavMealTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/28/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages
class FavMealTableViewController: UITableViewController {
    var products = [ProductModel]()
    var backEnd : FavMealBackEndManger!
    var refreshTVControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshTVControl = UIRefreshControl()
        refreshTVControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "Pull to refresh"))
        refreshTVControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.refreshControl = refreshTVControl
        backEnd = FavMealBackEndManger()
        backEnd.delegate = self
        backEnd.DownloadData()
        SwiftLoading().showLoading()
        let menuCellNib = UINib(nibName: "ResturantMenuTableViewCell", bundle: nil)
        tableView.register(menuCellNib, forCellReuseIdentifier: RestProductMenuCellID)
    }
    @objc func refresh(){
        backEnd.DownloadData()
        SwiftLoading().showLoading()
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return products.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RestProductMenuCellID, for: indexPath) as! ResturantMenuTableViewCell
        
        cell.configure(name: products[indexPath.row]._name, desc: products[indexPath.row]._description, rate: products[indexPath.row]._rate, sizes: products[indexPath.row]._sizes, fav: true, image: products[indexPath.row]._image, isLogin: userLoginToken == nil ? false : true)
        cell.favBut.addTarget(self, action: #selector(minus(sender:)), for: .touchUpInside)
        return cell
    }
    
    
    @objc func minus(sender : Any){
        let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView?.indexPathForRow(at: position)
        {
            backEnd.addOrRemoveFavRequest(id:products[indexPath.row]._id)
            
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: MealDetailsViewControllerID) as! MealDetailsViewController
        vc.product = products[indexPath.row]
      navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}

extension FavMealTableViewController : FavMealDelegate {
    func FavMeal(DidFinshDownload Sender: [ProductModel]) {
        products.removeAll()
        products = Sender
        tableView?.reloadData()
        SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()
    }
    
    func FavMeal(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        tableView?.reloadData()
        SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()

        
        
        
    }
    
    func FavMeal(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        tableView?.reloadData()
        SwiftLoading().hideLoading()
        refreshTVControl.endRefreshing()

        
    }
    
    func FavMeal(Success msg: String) {
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        products.removeAll()
        backEnd.DownloadData()
        tableView?.reloadData()
        SwiftLoading().showLoading()
        refreshTVControl.endRefreshing()

        
    }
    
    
    
}
