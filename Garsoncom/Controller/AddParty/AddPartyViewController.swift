//  AddPartyViewController.swift
//  Garsoncom
//  Created by Mohamed anwar on 6/8/18.
//  Copyright © 2018 Rivers. All rights reserved.

import UIKit
import  SkyFloatingLabelTextField
import DatePickerDialog
import  SwiftMessages
import PMSuperButton
import DropDown

class AddPartyViewController: UIViewController {
    var _view : Parties{
        return view as! Parties
    }
    @IBOutlet weak var markAllRestaurant: UIButton!
    @IBOutlet weak var markAllCategries: UIButton!
    @IBOutlet weak var ChooseGovernmentButton: PMSuperButton!
    @IBOutlet weak var ChooseCityButton: PMSuperButton!
    var dataManger : ChooseGovernmentNetworkManger!
    var governments = [GovernmentModel]()
    var cities = [CityModel]()
    var downloaded = false
    var tableType : Int?
    let setGovernment = DropDown()
    let setCity = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.setGovernment,
            self.setCity
            
        ]
    }()
    var catigoriesMarked = false
    var restauntsMarked = false
    var parameters = [String : Any]()
    var partytypes = [PartyType]()
    var backEndManger : AddPartyBackEndManger!
    var inputArray : [SkyFloatingLabelTextField]!
    var categories  = [CategoryModel]()
    var restaurants = [ResturantModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataManger = ChooseGovernmentNetworkManger()
        dataManger.delegate = self
        dataManger.DownloadGovernmentData()
        ChooseGovernmentButton.showLoader()
        setupSetGovernmentDropDown()
        setupSetCityDropDown()
        ChooseGovernmentButton.addTarget(self, action:#selector(showDropDownForgovernment) , for: .touchUpInside)
        ChooseCityButton.addTarget(self, action:#selector(showDropDownForCity) , for: .touchUpInside)
        
        backEndManger = AddPartyBackEndManger()
        backEndManger.delegate = self
        backEndManger.DownloadPartyType()
        backEndManger.DownloadCategoryData()
//        backEndManger.DownloadRestaurantsData(categoriesId: [])
        _view.delegete = self
        _view.setupSetCityDropDown()
        _view.collectionView.allowsSelection = true
        _view.collectionView.allowsMultipleSelection = true
        if let layout = _view.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.itemSize = CGSize(width: _view.collectionView.frame.width / 4 , height: _view.collectionView.frame.height - 20)
        }
        inputArray = [_view.addressTextF , _view.contentTextF ,_view.numberOfGuestsTextF]
        let categoryNib = UINib(nibName: "CategoryCollectionViewCell", bundle: nil)
        _view.collectionView.register(categoryNib, forCellWithReuseIdentifier: categoryCellId)
        let restaurantNib = UINib(nibName: "PartiesTableViewCell", bundle: nil)
        _view.tableView.register(restaurantNib, forCellReuseIdentifier: PartiesTableViewCellID)
        parameters["in_out"] = 1

        
    }
    @objc func showDropDownForgovernment() {
        if downloaded{
            setGovernment.show()
        }else{
            dataManger.DownloadGovernmentData()
        }
    }
    @objc func showDropDownForCity() {
        setCity.show()
    }
    func setupSetGovernmentDropDown() {
        setGovernment.anchorView =  ChooseGovernmentButton
        setGovernment.bottomOffset = CGPoint(x: 0, y:  ChooseCityButton.bounds.height)
        setGovernment.selectionAction = { [unowned self] (index, item) in
            self.ChooseGovernmentButton.setTitle(item, for: .normal)
            self.setCity.dataSource.removeAll()
            self.cities.removeAll()
            self.setCity.reloadAllComponents()
            self.ChooseCityButton.showLoader()
            self.dataManger.DownloadCityData(id: String(self.governments[index]._id))
        }
    }
    var cityId : Int?
    func setupSetCityDropDown() {
        setCity.anchorView =  ChooseCityButton
        setCity.bottomOffset = CGPoint(x: 0, y: ChooseCityButton.bounds.height)
        setCity.selectionAction = { [unowned self] (index, item) in
            self.ChooseCityButton.setTitle(item, for: .normal)
            self.cityId = self.cities[index]._id
          
        }
    }
    
    @IBAction func ChooseDate(_ sender: UIButton) {
        DatePickerDialog().show(DatePickerTitleString, doneButtonTitle: OkayButtonTitle, cancelButtonTitle:CancelButtonTitle, datePickerMode: .date) {[weak self]
            (date) -> Void in
            guard let strongSelf = self else{return}
            if let dt = date {
                strongSelf.parameters["date"] = convertDateObjectToDate(date: dt)
                strongSelf._view.chooseDate.setTitle(convertDateObjectToDate(date: dt), for: .normal)
                
            }
        }
    }
    @IBAction func markAllCatefories(_ sender : UIButton){
        var ids = [Int]()
        categories.forEach({
            $0._selected = !catigoriesMarked
            ids.append($0._id)
        })
        backEndManger.DownloadRestaurantsData(categoriesId: ids ,cityId: cityId)
            
        SwiftLoading().showLoading()
        _view.collectionView.reloadData()
        _view.markAllCategoriesButton.setImage(catigoriesMarked ? #imageLiteral(resourceName: "uncheckbox") :#imageLiteral(resourceName: "checkBox"), for: .normal)

        catigoriesMarked = !catigoriesMarked
        _view.markAllRestaurantsButton.setImage( #imageLiteral(resourceName: "uncheckbox") , for: .normal)
        restauntsMarked = false
        
    }
    @IBAction func selectPartyPlace(_ sender : UIButton){
        if sender == _view.intsideRestButton{
            _view.outsideRestButton.setImage(#imageLiteral(resourceName: "unselectedHumperger"), for: .normal)
            _view.intsideRestButton.setImage(#imageLiteral(resourceName: "selectedHumperger"), for: .normal)
            parameters["in_out"] = 1
        }
        if sender == _view.outsideRestButton{
            _view.intsideRestButton.setImage(#imageLiteral(resourceName: "unselectedHumperger"), for: .normal)
            _view.outsideRestButton.setImage(#imageLiteral(resourceName: "selectedHumperger"), for: .normal)
            parameters["in_out"] = 0


        }
        
        
    }
    @IBAction func markAllResturants(_ sender : UIButton){
        var ids = [Int]()
        restaurants.forEach({
            $0._selected = !restauntsMarked
            ids.append($0._id)
        })
        _view.tableView.reloadData()
        _view.markAllRestaurantsButton.setImage(restauntsMarked ? #imageLiteral(resourceName: "uncheckbox") :#imageLiteral(resourceName: "checkBox"), for: .normal)

        restauntsMarked = !restauntsMarked
    }
    @IBAction func SendNotification(_ sender : UIButton){
        if Validator.validateInputs(inputs: inputArray) {
            if parameters.count > 2 {
            parameters["api_token"] = userLoginToken
            parameters["description"] = _view.contentTextF.text
            parameters["numbers"] = _view.numberOfGuestsTextF.text
            parameters["location"] = _view.addressTextF.text
            if restaurants.filter({
                $0._selected == true
            }).count > 0 {
                restaurants.enumerated().forEach({ index , value in
                    parameters["restaurants[\(index)]"] = value._id
                })
                backEndManger.AddPartyRequest(parameters: parameters)
                SwiftLoading().showLoading()
            }else{
                SwiftMessages.showMessage(title: "", body: "Choose Restaurant", type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

                
                }
                
            }else{
                SwiftMessages.showMessage(title: "", body: PleaseFillInMsgString, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
                
            }
       
    
        }
        
    }


  
}

extension AddPartyViewController : ChoosePartyDelegate{
    func ChooseParty(DidFinshDownload Sender: [PartyType]) {
        partytypes = Sender
        Sender.forEach({
            _view.setPartyType.dataSource.append($0._name)
        })
        SwiftLoading().hideLoading()

    }
    
    func ChooseParty(DidFinshDownload Sender: [CategoryModel]) {
        categories = Sender
        if categories.count > 0{
            markAllCategries.isHidden = false
        
        }else{
            markAllCategries.isHidden = true

            
        }
        _view.collectionView.reloadData()
        SwiftLoading().hideLoading()

        
    }
    func ChooseParty(DidFinshDownload Sender: [ResturantModel]) {
        restaurants = Sender
        if Sender.count > 0 {
            markAllRestaurant.isHidden = false
        }else{
            markAllRestaurant.isHidden = true

        }
        _view.tableView.reloadData()
        SwiftLoading().hideLoading()

        
    }
    
    func ChooseParty(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        SwiftLoading().hideLoading()

    }
    
    func ChooseParty(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        SwiftLoading().hideLoading()

    }
    
    func ChooseParty(Success msg: String) {
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        SwiftLoading().hideLoading()

    }
    func didTapPartyType(Sender: Int) {
        parameters["type"] = partytypes[Sender]._id

    }
    
    func didTapPartyPlace(Sender: Bool) {

    }
  
}
extension AddPartyViewController :ChooseGovernmentDelegate{
    func GetGovernmentCities(didFinishDownload Sender: [CityModel]?) {
        ChooseCityButton.hideLoader()
        self.setCity.dataSource.removeAll()
        self.cities.removeAll()
        self.setCity.reloadAllComponents()
        self.ChooseCityButton.setTitle(ChooseCityButtonTitle, for: .normal)
        cities = Sender!
        Sender?.forEach({
            setCity.dataSource.append($0._name)
        })
    }
    func GetGovernment(didFinishDownload Sender: [GovernmentModel]?) {
        ChooseGovernmentButton.hideLoader()
        downloaded = true
        governments = Sender!
        Sender?.forEach({
            setGovernment.dataSource.append($0._name)
        })
    }
    func GetGovernment(didFaild error: Error?) {
        ChooseCityButton.hideLoader()
        ChooseGovernmentButton.hideLoader()
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
    }
    func GetGovernment(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body:didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
    }
}
