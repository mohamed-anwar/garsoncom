//
//  AddParty+CollectionViewEX.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/12/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
extension AddPartyViewController : UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          categories[indexPath.row]._selected = !categories[indexPath.row]._selected
        var ids = [Int]()
        categories.filter({
            $0._selected == true
        }).forEach({
            ids.append($0._id)
        })
        backEndManger.DownloadRestaurantsData(categoriesId: ids ,cityId: cityId)
        SwiftLoading().showLoading()
            _view.markAllRestaurantsButton.setImage( #imageLiteral(resourceName: "uncheckbox") , for: .normal)
            _view.markAllCategoriesButton.setImage( #imageLiteral(resourceName: "uncheckbox") , for: .normal)
        
            restauntsMarked = false
            catigoriesMarked = false
     
        _view.collectionView.reloadData()
        
    }
    
}
extension AddPartyViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoryCellId, for:indexPath ) as! CategoryCollectionViewCell
        cell.SetCell(name: categories[indexPath.row]._name)
       cell.SetSelectedCell(sender: categories[indexPath.row]._selected)
        
        return cell
        
    }
}
