//
//  AddParty+TableViewEX.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/12/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
extension AddPartyViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        restaurants[indexPath.row]._selected = !restaurants[indexPath.row]._selected
        _view.tableView.reloadRows(at: [indexPath], with: .automatic)
        _view.markAllRestaurantsButton.setImage( #imageLiteral(resourceName: "uncheckbox") , for: .normal)
        restauntsMarked = false
    }
}
extension AddPartyViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PartiesTableViewCellID, for: indexPath) as! PartiesTableViewCell
        cell.configureCell(image: restaurants[indexPath.row]._image, name: restaurants[indexPath.row]._name, branshNum: "\(restaurants[indexPath.row]._numBranches)", type: restaurants[indexPath.row]._type, selected: restaurants[indexPath.row]._selected)
        return cell
    }
    
    
    
}
