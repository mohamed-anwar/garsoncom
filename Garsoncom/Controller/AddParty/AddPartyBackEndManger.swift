//
//  BackEndManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 6/11/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
class AddPartyBackEndManger {
    private var network : Networking!
    weak var delegate  : ChoosePartyDelegate?
    init() {
        network = Networking(requestTimeout: 10)
}
    func DownloadCategoryData() {
        var categoriesModels = [CategoryModel]()
        network.manager.request( categoriesUrl , headers : [ acceptLanguage : setLang() ]).responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else { return }
            switch result {
            case .success(let value):
                let json = JSON(value)
                if let contentArray = json["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = CategoryModel()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        categoriesModels.append(model)
                    }
                    strongSelf.delegate?.ChooseParty(DidFinshDownload: categoriesModels)
                } else {
                    strongSelf.delegate?.ChooseParty(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.ChooseParty(didFaild: error as NSError)
                break
            }
        }
    }
    func DownloadPartyType() {
        var partyTypes = [PartyType]()
        network.manager.request( GetPartyTypeUrl , headers : [ acceptLanguage : setLang() ]).responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else { return }
            switch result {
            case .success(let value):
                let json = JSON(value)
                if let contentArray = json["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = PartyType()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        partyTypes.append(model)
                    }
                    strongSelf.delegate?.ChooseParty(DidFinshDownload: partyTypes)
                } else {
                    strongSelf.delegate?.ChooseParty(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.ChooseParty(didFaild: error as NSError)
                break
            }
        }
    }
    func DownloadRestaurantsData(categoriesId : [Int], cityId : Int? = nil) {
        var parameters = [String : Any]()
        if cityId != nil {
            parameters["city_id"] =  cityId
        }else if cityID != 0{
            parameters["city_id"] =  cityID!
        }else{
            parameters["lat"] =  lat!
            parameters["lng"] =  lng!
        }
//        categoriesId.enumerated().forEach({ index , value in
//            parameters["categories[\(index)]"] = value
//        })
         parameters["categories"] = categoriesId
        if userLoginToken != nil{
            parameters["api_token"] = userLoginToken!
        }
        print(parameters)
        network.manager.request( ResturantsUrl ,method : .post ,parameters: parameters, headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            var resturantModels = [ResturantModel]()
            let result = response.result
            guard let strongSelf = self else { return }
            switch result {
            case .success(let value):
                let json = JSON(value)
                if let contentArray = json["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = ResturantModel()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        resturantModels.append(model)
                    }
                    strongSelf.delegate?.ChooseParty(DidFinshDownload: resturantModels)
                } else {
                    strongSelf.delegate?.ChooseParty(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.ChooseParty(didFaild: error as NSError)
                break
            }
        }
    }
    func AddPartyRequest(parameters : [String : Any]) {
   
        network.manager.request( AddPartyUrl ,method : .post ,parameters: parameters, headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else { return }
            switch result {
            case .success(let value):
                let json = JSON(value)
                if let msg = json["message"].string{
                    strongSelf.delegate?.ChooseParty(Success: msg)
                } else {
                    strongSelf.delegate?.ChooseParty(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.ChooseParty(didFaild: error as NSError)
                break
            }
        }
    }
}
