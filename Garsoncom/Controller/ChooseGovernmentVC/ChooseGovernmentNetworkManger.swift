import Foundation
import SwiftyJSON
import ObjectMapper

class ChooseGovernmentNetworkManger {
    
    private var network : Networking!
    weak var delegate : ChooseGovernmentDelegate?

    init() {
        network = Networking(requestTimeout: 10)
    }
    
    func DownloadGovernmentData() {
        var governments = [GovernmentModel]()

        network.manager.request(governoratesUrl, headers : [ acceptLanguage :  setLang() ]).responseJSON { [weak self]response in
            let result = response.result
            guard let strongSelf = self else { return }

            switch result {
            case .success(let value):
                let json = JSON(value)
                if let contentArray = json["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let government = GovernmentModel()
                        government.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        governments.append(government)

                        }
                    strongSelf.delegate?.GetGovernment(didFinishDownload: governments)
                } else {
                    strongSelf.delegate?.GetGovernment(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.GetGovernment(didFaild: error)
                break
                
            }
        }
}
    func DownloadCityData(id : String) {
        var cities = [CityModel]()

        network.manager.request(citiesUrl + "?id=" + id, headers : [ acceptLanguage :  setLang() ]).responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else { return }

            switch result {
            case .success(let value):
                let json = JSON(value)

                if let contentArray = json["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let city = CityModel()
                        city.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        cities.append(city)
                    }
                    strongSelf.delegate?.GetGovernmentCities(didFinishDownload: cities)
                } else {
                    strongSelf.delegate?.GetGovernment(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.GetGovernment(didFaild: error )
                break
                
            }
        }
    }

    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
