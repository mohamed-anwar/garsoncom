//
//  ChooseGovernmentViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/10/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import PMSuperButton
import DropDown
import SwiftMessages


class ChooseGovernmentViewController: UIViewController  {
    var dataManger : ChooseGovernmentNetworkManger!
    @IBOutlet var mainView: ChooseGovernmentV!
    var governments = [GovernmentModel]()
    var cities = [CityModel]()
    var downloaded = false

    override func viewDidLoad() {
        super.viewDidLoad()
        dataManger = ChooseGovernmentNetworkManger()
        dataManger.delegate = self
        dataManger.DownloadGovernmentData()
        mainView.ChooseGovernmentButton.showLoader()
        setupSetGovernmentDropDown()
         setupSetCityDropDown()
        mainView.ChooseGovernmentButton.addTarget(self, action:#selector(showDropDownForgovernment) , for: .touchUpInside)
        mainView.ChooseCityButton.addTarget(self, action:#selector(showDropDownForCity) , for: .touchUpInside)
        mainView.SubmitButton.addTarget(self, action:#selector(SubmitButton) , for: .touchUpInside)

        

    }

    @objc func SubmitButton() {
        if cityID != 0{
        presentViewController(homeNavID)
        }else{
            SwiftMessages.showMessage(title: "", body:PleaseSelectCityMsgString, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        }
        
    }
    
    @objc func showDropDownForgovernment() {
        if downloaded{
            mainView.setGovernment.show()
        }else{
            dataManger.DownloadGovernmentData()
        }
    }
    @objc func showDropDownForCity() {
        mainView.setCity.show()
    }
    func setupSetGovernmentDropDown() {
         mainView.setGovernment.anchorView =  mainView.ChooseGovernmentButton
         mainView.setGovernment.bottomOffset = CGPoint(x: 0, y:  mainView.ChooseCityButton.bounds.height)
         mainView.setGovernment.selectionAction = { [unowned self] (index, item) in
            self.mainView.ChooseGovernmentButton.setTitle(item, for: .normal)
            self.mainView.setCity.dataSource.removeAll()
            self.cities.removeAll()
            self.mainView.setCity.reloadAllComponents()
            self.mainView.ChooseCityButton.showLoader()
            self.dataManger.DownloadCityData(id: String(self.governments[index]._id))
        }
    }
    func setupSetCityDropDown() {
         mainView.setCity.anchorView =  mainView.ChooseCityButton
         mainView.setCity.bottomOffset = CGPoint(x: 0, y:  mainView.ChooseCityButton.bounds.height)
         mainView.setCity.selectionAction = { [unowned self] (index, item) in
            self.mainView.ChooseCityButton.setTitle(item, for: .normal)
            cityID = self.cities[index]._id

        }
    }
 

}
extension ChooseGovernmentViewController : ChooseGovernmentDelegate{
    
    func GetGovernmentCities(didFinishDownload Sender: [CityModel]?) {
        mainView.ChooseCityButton.hideLoader()
        self.mainView.setCity.dataSource.removeAll()
        self.cities.removeAll()
        self.mainView.setCity.reloadAllComponents()
        self.mainView.ChooseCityButton.setTitle(ChooseCityButtonTitle, for: .normal)
        cities = Sender!
        Sender?.forEach({
            mainView.setCity.dataSource.append($0._name)
        })

        
    }
    
    func GetGovernment(didFinishDownload Sender: [GovernmentModel]?) {
        mainView.ChooseGovernmentButton.hideLoader()
        downloaded = true
        governments = Sender!
        Sender?.forEach({
            mainView.setGovernment.dataSource.append($0._name)
        })
    }
    
    func GetGovernment(didFaild error: Error?) {
        mainView.ChooseCityButton.hideLoader()
        mainView.ChooseGovernmentButton.hideLoader()
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)


        
    }
    
    func GetGovernment(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body:didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

    }
    
    
}
