//
//  OffersTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/17/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages
class OffersTableViewController: UITableViewController {
          
        var offers = [ProductModel]()
        var backend : OffersBackEnd!
        var refreshTVControl: UIRefreshControl!

        override func viewDidLoad() {
            super.viewDidLoad()
            refreshTVControl = UIRefreshControl()
            refreshTVControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "Pull to refresh"))
            refreshTVControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
            tableView.refreshControl = refreshTVControl

            backend = OffersBackEnd()
            backend.delegate  = self
            backend.DownloadData()
            SwiftLoading().showLoading()

            let celllNib = UINib(nibName: "OffersTableViewCell", bundle: nil)
            tableView.register(celllNib, forCellReuseIdentifier: OffersTableViewCellID)
        }
    @objc func refresh(){
        backend.DownloadData()
        SwiftLoading().showLoading()
        
    }
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return offers.count
        }
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: OffersTableViewCellID, for: indexPath) as! OffersTableViewCell
            cell.configureCell(image: offers[indexPath.row]._image, restName: offers[indexPath.row]._resturant._name, foodName: offers[indexPath.row]._name, offer: "\(offers[indexPath.row]._sizes[0]._offer)",offerPrice: "\(offers[indexPath.row]._sizes[0]._offer_price)" )
           
            return cell
        }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: MealDetailsViewControllerID) as! MealDetailsViewController
        vc.product = offers[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
        
    }
    extension OffersTableViewController : OffersDelegate{
        func Offers(didFaild error: NSError?) {
            SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
            SwiftLoading().hideLoading()
            refreshTVControl.endRefreshing()

            
            
        }
        func Offers(didGetUnexpectedResponse: String) {
            SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
            SwiftLoading().hideLoading()
            refreshTVControl.endRefreshing()


            
            
        }
        func Offers(DidFinshDownload Sender: [ProductModel]) {
            offers = Sender
            tableView.reloadData()
            SwiftLoading().hideLoading()
            refreshTVControl.endRefreshing()


        
        }
        
        
}

