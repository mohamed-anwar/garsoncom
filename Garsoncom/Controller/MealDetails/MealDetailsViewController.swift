//
//  MealDetailsViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/21/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import SwiftMessages
import RealmSwift
import PopupDialog


class MealDetailsViewController: UIViewController {
    var product  = ProductModel()
    var selectedIndex = IndexPath(item: 0, section: 0)
    var _view : MealDetails {
        return view as! MealDetails
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = product._name
        let nibCell = UINib(nibName: "SelectSizeTableViewCell", bundle: nil)
        _view.tableView.register(nibCell, forCellReuseIdentifier: SelectSizeTableViewCellID)
        _view.configure(image: product._image , desc : product._description)
        _view.addRateButton.addTarget(self, action: #selector(showCustomDialog(animated:)), for: .touchUpInside)
        
    }
    @IBAction func AddToCartButton(_ sender: UIButton) {
        let order = Order()
        let food = Food()
        food.productName = product._name
        food.cov1 = product._cov1
        food.cov2 = product._cov2
        food.cov3 = product._cov3
        food.cov4 = product._cov4
        food.productId = "\(product._id)"
        if product._sizes.count > 0{
        food.sizeId = "\(product._sizes[selectedIndex.row]._id)"
        food.productPrice = product._sizes[selectedIndex.row]._offer_price

        }else{
            food.sizeId = "0"
            food.productPrice = "0"
        }
        food.productImage = product._image
        food.quantity = "\(_view.counter)"
        food.total = "\(_view.counter * Int(_view.price))"
        food.deliveryCost = product._resturant._deliveryCost
        food.deliveryTime = "\( product._resturant._deliveryTime)"
        order.foods.append(food)
        order.rest_id = "\(product._restaurantId)"
        order.deliveryCost = product._resturant._deliveryCost
        order.deliveryTime = "\(product._resturant._deliveryTime)"
        order.restName = product._resturant._name
        order.deliverMin = product._resturant._min_delivery_cost
        order.deliverMax = product._resturant._max_delivery_cost

        let realmOrder = Array(realm.objects(Order.self))
        try! realm.write {
            if realmOrder.count > 0 {
            realmOrder.first?.deliveryCost =  product._resturant._deliveryCost
            realmOrder.first?.deliveryTime = "\(product._resturant._deliveryTime)"
            realmOrder.first?.foods.append(food)
            realmOrder.first?.rest_id = "\(product._restaurantId)"
            }else{
            realm.add(order)

            }
//            realm.add(food)
//            realm.add(order)

            SwiftMessages.showMessage(title: "", body: NSLocalizedString("Your Item add Successfully", comment: ""), type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        }
        
    }
    
    
    
    
    @objc func showCustomDialog(animated: Bool = true) {
    
        // Create a custom view controller
        let RateDilaog = RateView(nibName: "RateView", bundle: nil)
        RateDilaog.id = product._id
        // Create thec  dialog
        let popup = PopupDialog(viewController: RateDilaog, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false)
    
        // Present dialog
            present(popup, animated: animated, completion: nil)
            
    }
    
    
    
    @IBAction func editMealButton(_ sender: UIButton) {
        if product._co1 != "" || product._co2 != "" || product._co3 != "" || product._co4 != "" {
            let vc = storyboard?.instantiateViewController(withIdentifier: EditDetailMealViewControllerID) as! EditDetailMealViewController
            vc.product = product
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        }else{
            showAlert(alertTitle: ErrorAlertString, alertMsg: NoAddationString, buttonTitle: OkayButtonTitle)
            
        }
    }
    
}
extension MealDetailsViewController : EditMealDelegate{
    func didEditMeal(Product: ProductModel) {
        product = Product
        print(product._cov1 , product._cov2 ,product._cov3,product._cov4)
    }
}
extension MealDetailsViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return product._sizes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SelectSizeTableViewCellID, for: indexPath) as! SelectSizeTableViewCell
        cell.configureCell(size: product._sizes[indexPath.row]._name, discount: product._sizes[indexPath.row]._offer_price, price: product._sizes[indexPath.row]._price , offer : product._sizes[indexPath.row]._offer)
        if indexPath == selectedIndex{
            cell.configureCell(selected: true)
        }else{
            cell.configureCell(selected: false)
        }
        if product._sizes[indexPath.row]._offer == 0{
            _view.configure(Totlat: Float(product._sizes[selectedIndex.row]._price)!)
        }else{
            _view.configure(Totlat: Float(product._sizes[selectedIndex.row]._offer_price)!)
        }
        return cell
    }
}

extension MealDetailsViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath
        
        if product._sizes[indexPath.row]._offer == 0{
            _view.configure(Totlat: Float(product._sizes[indexPath.row]._price)!)
        }else{
            _view.configure(Totlat: Float(product._sizes[indexPath.row]._offer_price)!)
        }
        _view.tableView.reloadData()
    }
    
    
}
