//
//  ChooseCollectionViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/2/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import AMPopTip
import SwiftMessages

class ChooseTableCollectionViewController: UIViewController {
    var tabels = [TablesModel]()
    @IBOutlet weak var collectionView : UICollectionView!
    weak var delegate  : ChooseTabelDelegate!
    var backEnd : ChooseTablesBackEndManger!
    var paramter = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        backEnd = ChooseTablesBackEndManger()
        backEnd.delegate = self
        let tableNib = UINib(nibName: "TableCollectionViewCell", bundle: nil)
        collectionView.register(tableNib, forCellWithReuseIdentifier: TableCellID)
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.itemSize = CGSize(width: collectionView.frame.width / 2.4 , height: collectionView.frame.height / 3.5)
        }
        collectionView.allowsSelection = true
        collectionView.allowsMultipleSelection = true
    }
    @IBAction func submit(sender  : UIButton){
        tabels.enumerated().filter({ index,table in
            table._isSelected == true
        }).forEach({ (index,table) in
            paramter["tables[\(index)]"] = table._id
        })
        if userLoginToken != nil{
            paramter["api_token"] = userLoginToken!
            if paramter.count > 4{
                backEnd.DownloadData(parameters: paramter)
            }else{
                showAlert(alertTitle: ErrorAlertString, alertMsg: PleaseSelectTableMsgString, buttonTitle: OkayButtonTitle)
            }
            
        }else{
            presentViewController(RegisterNavID)
        }

       
    }
    
}
extension ChooseTableCollectionViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TableCellID, for: indexPath) as! TableCollectionViewCell
        cell.Configre(image: tabels[indexPath.row]._image, tableNum: tabels[indexPath.row]._num, gusetsAllowes: tabels[indexPath.row]._num_guests, disc: tabels[indexPath.row]._description, selected: tabels[indexPath.row]._isSelected)
        cell.InfoButton.addTarget(self, action: #selector(showTip(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func showTip(sender : Any) {
        let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: self.collectionView)
        if let indexPath = self.collectionView.indexPathForItem(at: position)
        {
            let cell = collectionView.cellForItem(at: indexPath)  as! TableCollectionViewCell
            cell.popTip.show(text: tabels[indexPath.row]._description, direction: .down, maxWidth: CGFloat(200), in: cell.InfoButton, from: cell.InfoButton.frame, duration: 3)
        }
    }
}
extension ChooseTableCollectionViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        tabels[indexPath.row]._isSelected = !tabels[indexPath.row]._isSelected
        collectionView.reloadData()
    }
}
extension ChooseTableCollectionViewController : ChooseTabelDelegate{
    func ChooseTabelDelegate(didFinishDownload msg: String?) {
        SwiftMessages.showMessage(title: "", body: msg!, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        navigationController?.popViewController(animated: true)
    }
    
    func ChooseTabelDelegate(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        
    }
    
    func ChooseTabelDelegate(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        
    }
    
    
    
}
