//
//  ChooseTablesBackEndManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

//


import SwiftyJSON
import ObjectMapper
class  ChooseTablesBackEndManger {
    
    private var network : Networking!
    weak var delegate  : ChooseTabelDelegate?
    
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    
    
    func DownloadData(parameters : [String:Any]) {
        network.manager.request( AddTableReservationURL,method : .post ,parameters: parameters,headers : [ acceptLanguage :  setLang() ]).responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                if let msg = json["message"].string{
                
                    strongSelf.delegate?.ChooseTabelDelegate(didFinishDownload: msg)
                } else {
                    strongSelf.delegate?.ChooseTabelDelegate(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.ChooseTabelDelegate(didFaild: error as NSError)
                break
            }
        }
    }
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
