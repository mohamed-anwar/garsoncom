//
//  CartViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import RealmSwift

class CartViewController: UIViewController {
    var _view : EditMeal{
        return view as! EditMeal
    }
    var order = [Order]()
    
    var resturant = ResturantModel()

    @IBOutlet weak var tableView : UITableView!
    
    @IBAction func submitButton(sender : UIButton){
        let foods  = Array(realm.objects(Food.self))
        if foods.count > 0{
            reloadData()
        let vc = storyboard?.instantiateViewController(withIdentifier: ProcessOrderViewControllerID) as! ProcessOrderViewController
            vc.order = order.first!
            navigationController?.pushViewController(vc, animated: true)
        }else{
            showAlert(alertTitle: ErrorAlertString, alertMsg: NSLocalizedString("Your Cart Is Empty ! ", comment: ""), buttonTitle: OkayButtonTitle)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nibCell = UINib(nibName: "CartTableViewCell", bundle: nil)
        tableView.register(nibCell, forCellReuseIdentifier: CartTableViewCellID)
         reloadData()
        
    }
    @objc func plus(sender : Any){
        let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: position)
        {
            let cell = tableView.cellForRow(at: indexPath)  as! CartTableViewCell
            try! realm.write {
                order.first?.foods[indexPath.row].quantity = "\(cell.counter)"
                order.first?.foods[indexPath.row].total = "\(Double(cell.counter) * Double(cell.price))"

            }
            reloadData()
            
        }
    }
    @objc func minus(sender : Any){
        let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: position)
        {
            let cell = tableView.cellForRow(at: indexPath)  as! CartTableViewCell

            try! realm.write {
                order.first?.foods[indexPath.row].quantity = "\(cell.counter)"
                 order.first?.foods[indexPath.row].total = "\(Double(cell.counter) * Double(cell.price))"
            }
            reloadData()
            
        }
    }
    func reloadData() {
        print(Realm.Configuration.defaultConfiguration.fileURL!)

        order = Array(realm.objects(Order.self))
        
        try! realm.write {
                    order.first?.price = 0

                order.first?.foods.forEach({
                order.first?.price += Double($0.total)!
            })
//            if let foods = order.first?.foods{
//                order.first?.deliveryCost = (foods.first?.deliveryCost)!
//                order.first?.deliveryTime = (foods.first?.deliveryTime)!
                order.first?.total = "\( (order.first?.price)! + Double( (order.first?.deliveryCost) ?? "0")!)"
//            }
//            if  (order.first?.foods.count)! > 0 {
//                 order.first?.deliveryCost = ( order.first?.foods.first?.deliveryCost)!
//                 order.first?.deliveryTime = ( order.first?.foods.first?.deliveryTime)!
//                order.first?.total = "\( (order.first?.price)! + Double( (order.first?.deliveryCost)!)!)"
//            }
//        }
        
        }
        
    }
//    func reloadData() {
//        order.foods.removeAll()
//        order.price = 0
//        let foods  = Array(realm.objects(Food.self))
//        try! realm.write {
//        foods.forEach({
//            order.foods.append($0)
//            order.price += Double($0.total)!
//        })
//            if foods.count > 0 {
//                order.deliveryCost = (foods.first?.deliveryCost)!
//                order.deliveryTime = (foods.first?.deliveryTime)!
//                order.total = "\(order.price + Double(order.deliveryCost)!)"
//            }
//        }
//
//    }
    
    
    
}
extension CartViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            try! realm.write {
                realm.delete((order.first?.foods[indexPath.row])!)
            }
            reloadData()
//            tableView.reloadData()
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }
}
extension CartViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order.first?.foods.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CartTableViewCellID, for: indexPath) as! CartTableViewCell
        cell.configure(image:(order.first?.foods[indexPath.row].productImage)!, name: (order.first?.foods[indexPath.row].productName)!, price: (order.first?.foods[indexPath.row].productPrice)! , Counter : Int((order.first?.foods[indexPath.row].quantity)!)!)
        cell.plusButton.addTarget(self, action: #selector(plus(sender:)), for: .touchUpInside)
        cell.minusButton.addTarget(self, action: #selector(minus(sender:)), for: .touchUpInside)
        
        
        return cell
    }
}
