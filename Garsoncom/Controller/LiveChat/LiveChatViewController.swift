//
//  LiveChatViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/24/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import JSQMessagesViewController

class LiveChatViewController: JSQMessagesViewController {
    var messages = [JSQMessage]()


    override func viewDidLoad() {
        super.viewDidLoad()

    }



}
extension LiveChatViewController {
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
 
        
        finishSendingMessage(animated: true)
        
        
    }
   
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.row]
        let messageUsername = message.senderDisplayName
        
        
        return NSAttributedString(string: messageUsername!)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        let message = messages[indexPath.row]
        
        
//        if currentUser.id == message.senderId {
//            return bubbleFactory?.outgoingMessagesBubbleImage(with: UIColor(red: 176/255, green: 232/255, blue: 247/255, alpha: 1))
//        } else {
            return bubbleFactory?.incomingMessagesBubbleImage(with: UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1))
//
//        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.row]
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : JSQMessagesCollectionViewCell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.row]
        
        
        if (!message.isMediaMessage) {
            
            if(message.senderId == self.senderId){
                cell.textView?.textColor = UIColor.black //change this color for your messages
                
            }else{
                cell.textView?.textColor = UIColor.blue //change this color for other people message
            }
            
//            cell.textView?.linkTextAttributes = [NSForegroundColorAttributeName : UIColor.blue] //this is the color of the links
            
        }
        
        return cell
    }
}
