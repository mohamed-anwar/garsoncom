//
//  StartUpViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/4/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import PopupDialog
class StartUpViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    var startupBackEndManager : FaceBookBackEndManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        startupBackEndManager = FaceBookBackEndManager()
        startupBackEndManager.delegate = self
        
        makeBorderToView(loginButton, boderWidth: 1, cornerRadius: 15, borderColor: .clear, maskToBounds: true)
        makeBorderToView(signUpButton, boderWidth: 1, cornerRadius: 15, borderColor: .clear, maskToBounds: true)
    }
    
    let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
    
    
    @IBAction func faceBookLogin(_ sender: UIButton) {
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) in
            if (error == nil){
                print(result?.description)
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.fetchProfile()
                    }
                }
            }else{
                print(error?.localizedDescription)

            }
        }
        
    }
    
    


}

