//
//  LoginBackEndMannger.swift
//  Foreera
//
//  Created by Mohamed on 9/27/17.
//  Copyright © 2017 Foreera. All rights reserved.
//

import Foundation
import Foundation
import SwiftyJSON
import RealmSwift

class FaceBookBackEndManager {
    private var network : Networking!
    weak var delegate : FaceBookLoginDelegate?
    init() {
        network = Networking(requestTimeout: 10)
    }
    func SocialLoginRequest(deviceId : String , deviceName :String , userid : String , authToken : String) {
        
        let parameters : [ String: String ] = [
            SocialLoginKeys.authToken : authToken
        ]
        network.manager.request(facebookLoginUrl , parameters: parameters, headers :[ acceptLanguage :  setLang() ]).responseJSON {[weak self] response in
            guard let  strongSelf = self else{return}
            let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].string != StatusMsgs.error {

                    strongSelf.delegate?.StartUpDidFail(error: nil, Messages: json["message"].string)
                } else {
                  strongSelf.delegate?.StartUp(DidSucess: true)
                        }
                break
            case .failure(let error) :
        strongSelf.delegate?.StartUpDidFail(error: error as NSError, Messages:nil)
                break
            }
        }
        
    }
        
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
