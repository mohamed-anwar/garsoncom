//
//  LoginFacebookEx.swift
//  Foreera
//
//  Created by Mohamed on 9/29/17.
//  Copyright © 2017 Foreera. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import Firebase
import FirebaseMessaging
extension StartUpViewController : FBSDKLoginButtonDelegate{
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        return false
    }
    
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if(error == nil){
            print("Facebook logged in")
            print(result)
            fetchProfile()
            
        }
    }
    func fetchProfile(){
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields" : "id, name, first_name, last_name, picture.type(large), email"])
            .start(completionHandler:  { (connection, result, error) in
                print(error?.localizedDescription,result)

                let fbAccessToken = FBSDKAccessToken.current().tokenString
                print(fbAccessToken)
                guard let fbResult = result as? [String : AnyObject],
                    let email = fbResult["email"] as? String,
                    let user_name = fbResult["name"] as? String,
                    let frist_name = fbResult["first_name"] as? String,
                    let last_name = fbResult["last_name"] as? String,
                    let user_id_fb = fbResult["id"]  as? String else {return}

                if user_id_fb.count != 0 {
                    
                    print("here is accses token ",fbAccessToken ?? "no token" , "user id " ,user_id_fb , email , user_name )
                }
                guard let userInfo = fbResult as? [String: Any] else { return }
                if let imageURL = ((userInfo["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                    print(imageURL)
                }
                print("here is user data email : \(email) , name : \(user_name) ,  token :\(fbAccessToken)")
//                self.startupBackEndManager.SocialLoginRequest(deviceId: refesheToken!, deviceName: "ios", userid:user_id_fb , authToken: fbAccessToken!)

            })

        
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!){
        print("loged out")
    }
    
    
    
    
}
