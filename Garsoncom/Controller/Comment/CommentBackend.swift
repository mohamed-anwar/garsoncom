//
//  CommentBackend.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


import Foundation
import SwiftyJSON
import ObjectMapper
class CommentBackEnd{
    private var network : Networking!
    weak var delegate : CommentDelelgate?
    init() {
        network = Networking(requestTimeout: 10)
    }
   
    func DownloadData(id : Int) {
        network.manager.request( ShowPostURL , method : .post , parameters : ["api_token" : userLoginToken!,"post_id":id], headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            var models = [CommentModel]()
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                if let contentArray = json["data"]["comments"]["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = CommentModel()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        models.append(model)
                    }
                    strongSelf.delegate?.Comment(didDownloadData: models)
                } else {
                    strongSelf.delegate?.Comment(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.Comment(didFaild: error)
                break
                
            }
        }
    }
 
    func DeletePostRequest(id: Int) {
        network.manager.request( DeletePostUrl , method : .post , parameters : ["api_token" : userLoginToken!,"post_id" : id], headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].string! == StatusMsgs.sucess{
                    strongSelf.delegate?.Comment(didDeletePost : json["message"].string!)
                    
                } else {
                    strongSelf.delegate?.Comment(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.Comment(didFaild: error)
                break
                
            }
        }
    }
    func DeleteCommentRequest(id: Int) {
        network.manager.request( DeleteCommentURL , method : .post , parameters : ["api_token" : userLoginToken!,"comment_id" : id], headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].string! == StatusMsgs.sucess{
                    strongSelf.delegate?.Comment(didDeleteComment: json["message"].string!)
                    
                } else {
                    strongSelf.delegate?.Comment(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.Comment(didFaild: error)
                break
                
            }
        }
    }
    func AddCommentRequest(id: Int,comment : String) {
        network.manager.request( AddCommentUrl , method : .post , parameters : ["api_token" : userLoginToken!,"post_id" : id , "comment" : comment], headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].string! == StatusMsgs.sucess{
                    let model = CommentModel()
                    model.mapping(map: Map(mappingType: .fromJSON, JSON: json["data"].dictionaryObject!))
                    strongSelf.delegate?.Comment(didAddComment: json["message"].string!, comment: model)
                    
                } else {
                    strongSelf.delegate?.Comment(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.Comment(didFaild: error)
                break
                
            }
        }
    }

    
    
    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
