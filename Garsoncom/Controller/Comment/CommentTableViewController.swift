//
//  CommentTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 7/3/18.
//  Copyright © 2018 Rivers. All rights reserved.
//



import UIKit
import SwiftMessages

class CommentTableViewController: UIViewController {
    var _view : CommentView!{
        return view as! CommentView
    }
    var refreshTVControl: UIRefreshControl!
    
    
    @objc func refresh() {
        backend?.DownloadData(id: post._id)
        SwiftLoading().showLoading()
        
    }
    
    @IBAction func deletePostButton(_ sender: UIBarButtonItem) {
        let actions : [((UIAlertAction) -> ())] = [
        { [weak self] _ in
            self?.backend?.DeletePostRequest(id: (self?.post._id)!)
            SwiftLoading().showLoading()
            
            },{ [weak self] _ in
                self!.dismiss(animated: true, completion: nil)
            }
        ]
        
        showAlert(alertTitle: aletAskString, alertMsg: askForDeleteString, buttonTitles: [OkayButtonTitle,CancelButtonTitle], actions: actions)
        
    }
        
    @IBOutlet weak var deletePostButton: UIBarButtonItem!
    var backend : CommentBackEnd?
    var comments = [CommentModel]()
    var post = Post()
    var deletedIndex : IndexPath?

    @objc func deleteButtonAction(sender : Any){
        let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: _view.tableView)
        if let indexPath = _view.tableView?.indexPathForRow(at: position)
        {
            let actions : [((UIAlertAction) -> ())] = [
            { [weak self] _ in
                self?.deletedIndex = indexPath
                self?.backend?.DeleteCommentRequest(id: (self?.comments[indexPath.row]._id)!)
                SwiftLoading().showLoading()
                
                },{ [weak self] _ in
                }
            ]
            
            showAlert(alertTitle: aletAskString, alertMsg: askForDeleteString, buttonTitles: [OkayButtonTitle,CancelButtonTitle], actions: actions)
            
        }
    }
    
    @objc func AddComment(){
        backend?.AddCommentRequest(id: post._id, comment: _view.commentTextF.text!)
        SwiftLoading().showLoading()
    }
    
    
    func registerNibs() {
        var nib = UINib(nibName: "CommentTableViewCell", bundle: nil)
        _view.tableView.register(nib, forCellReuseIdentifier: "CommentTableViewCellID")
        nib = UINib(nibName: "CommentTableViewHeader", bundle: nil)
        _view.tableView.register(nib, forHeaderFooterViewReuseIdentifier: "CommentTableViewHeaderID")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshTVControl = UIRefreshControl()
        refreshTVControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "Pull to refresh"))
        refreshTVControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        _view.tableView.refreshControl = refreshTVControl
        registerNibs()
        backend = CommentBackEnd()
        backend?.delegate = self
        backend?.DownloadData(id: post._id)
        if !post._is_my_post{
            navigationItem.rightBarButtonItems?.removeAll()
        }
        _view.sendButton.addTarget(self, action: #selector(AddComment), for: .touchUpInside)
    }
 
    
    
}
extension CommentTableViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 306
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentTableViewHeaderID") as! CommentTableViewHeader
        header.configure(post: post)
        return header
    }
    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCellID", for: indexPath) as! CommentTableViewCell
        cell.configure(comment: comments[indexPath.row])
        cell.deleteCommentButton.addTarget(self, action: #selector(deleteButtonAction(sender:)), for: .touchUpInside)
        
        
        
        return cell
    }

}

extension CommentTableViewController : CommentDelelgate{
    func Comment(didDeletePost msg: String) {
        SwiftLoading().hideLoading()
        navigationController?.popViewController(animated: true)
        
        
    }
    func Comment(didDownloadData Sender: [CommentModel]) {
        comments = Sender
        refreshTVControl?.endRefreshing()

        _view.tableView.reloadData()
        SwiftLoading().hideLoading()
        
        
    }
    
    func Comment(didAddComment msg: String , comment : CommentModel) {
        comments.append(comment)
        _view.tableView.reloadData()
        _view.commentTextF.text = ""
        _view.sendButton.isEnabled = false
        SwiftLoading().hideLoading()

        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
    
    func Comment(didFaild error: Error?) {
        refreshTVControl?.endRefreshing()

        SwiftLoading().hideLoading()
        
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
    
    func Comment(didGetUnexpectedResponse: String) {
        refreshTVControl?.endRefreshing()

        SwiftLoading().hideLoading()
        
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        
    }
   
    func Comment(didDeleteComment msg: String) {
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        comments.remove(at: (deletedIndex?.row)!)
        _view.tableView.deleteRows(at: [deletedIndex!], with: UITableViewRowAnimation.automatic)
        
        SwiftLoading().hideLoading()
        
        
        
    }
    
    
}
