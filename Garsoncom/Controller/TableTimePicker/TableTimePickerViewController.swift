//
//  TableTimePickerViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/2/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import DatePickerDialog
import SwiftMessages


class TableTimePickerViewController: UIViewController {

    weak var delegate : TableTimePickerDelegate!
    var backEnd  : TableTimeBackENdManger!
    var paramter = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paramter["from"] = convertDateObjectToTime(date: mainView.FromTimePicker.date)
        paramter["to"] = convertDateObjectToTime(date: mainView.FromTimePicker.date)
        backEnd = TableTimeBackENdManger()
        backEnd.delegate = self
        mainView.ChooseDateButton.addTarget(self, action: #selector(datePickerTapped), for: .touchUpInside)
        mainView.SubmitButton.addTarget(self, action: #selector(submit), for: .touchUpInside)
        mainView.ChooseDateButton.layer.cornerRadius = 10
        mainView.SubmitButton.layer.cornerRadius = 10


    }

    @IBOutlet var mainView: TableTimePickerView!
    
    @IBAction func FromDatePicker(_ sender: UIDatePicker) {
        paramter["from"] = convertDateObjectToTime(date: sender.date)

    }
    
    @IBAction func ToDatePicker(_ sender: UIDatePicker) {
        paramter["to"] = convertDateObjectToTime(date: sender.date)
    }

    
    @objc func datePickerTapped() {
        DatePickerDialog().show(DatePickerTitleString, doneButtonTitle: OkayButtonTitle, cancelButtonTitle:CancelButtonTitle, datePickerMode: .date) {[weak self]
            (date) -> Void in
            guard let strongSelf = self else{return}

            if let dt = date {
                strongSelf.paramter["date"] = convertDateObjectToDate(date: dt)
                strongSelf.mainView.ChooseDateButton.setTitle(convertDateObjectToDate(date: dt), for: .normal)
            }
        }
    }
    @objc func submit(){
        if paramter.count == 4{
            backEnd.DownloadData(parameters: paramter)
        }else{
            showAlert(alertTitle: ErrorAlertString, alertMsg: PleaseSelectDateMsgString, buttonTitle: OkayButtonTitle)
        }
        
    }
  
    
    
}
extension TableTimePickerViewController : TableTimePickerDelegate {
    
    func TableTimePicker(didFinishDownload Tables: [TablesModel]?) {
        let vc = storyboard?.instantiateViewController(withIdentifier: ChooseTableID) as! ChooseTableCollectionViewController
            vc.tabels = Tables!
        vc.paramter["from"] = paramter["from"]
        vc.paramter["to"] = paramter["to"]
        vc.paramter["date"] = paramter["date"]


        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func TableTimePicker(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        
    }
    
    func TableTimePicker(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)

        
    }
    
    
    
}
