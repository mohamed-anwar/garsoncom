//
//  TableTimeBackEnd.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/2/18.
//  Copyright © 2018 Rivers. All rights reserved.
//


import SwiftyJSON
import ObjectMapper
class  TableTimeBackENdManger {
    
    private var network : Networking!
    weak var delegate  : TableTimePickerDelegate?
    
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    
    
    func DownloadData(parameters : [String:Any]) {
        network.manager.request( GetTablesURL,method : .post ,parameters: parameters,headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            var tablesModels = [TablesModel]()
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if let contentArray = json["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = TablesModel()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        tablesModels.append(model)
                    }
                    strongSelf.delegate?.TableTimePicker(didFinishDownload: tablesModels)
                } else {
                    strongSelf.delegate?.TableTimePicker(didGetUnexpectedResponse: json["message"].string!)
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.TableTimePicker(didFaild: error as NSError)
                break
            }
        }
    }
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
