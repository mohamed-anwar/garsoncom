//
//  TableListViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/31/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import PopupDialog
import  SwiftMessages
import  PMSuperButton
import DropDown

class TableListViewController: UIViewController {
    var filteredData = [ResturantModel]()
    fileprivate let searchController = UISearchController(searchResultsController: nil)
    
   
    
    @IBOutlet weak var ChooseGovernmentButton: PMSuperButton!
    @IBOutlet weak var ChooseCityButton: PMSuperButton!
    var dataManger : ChooseGovernmentNetworkManger!
    var governments = [GovernmentModel]()
    var cities = [CityModel]()
    var downloaded = false
    var tableType : Int?
    let setGovernment = DropDown()
    let setCity = DropDown()
        lazy var dropDowns: [DropDown] = {
        return [
            self.setGovernment,
            self.setCity
            
        ]
    }()
    var resturants = [ResturantModel]()
    var favIndex : IndexPath!
    var didShow = false
    
    @IBOutlet weak var tableView : UITableView!
    var backEnd : ResturantListBackEndManger!
    override func viewDidLoad() {
        super.viewDidLoad()

        //Setup Search Controller
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Search"

        self.searchController.searchBar.barStyle = .default
        
        self.searchController.searchBar.delegate = self
        self.definesPresentationContext = true
        self.navigationItem.searchController = searchController
       
        
        
        dataManger = ChooseGovernmentNetworkManger()
        dataManger.delegate = self
        dataManger.DownloadGovernmentData()
        ChooseGovernmentButton.showLoader()
        setupSetGovernmentDropDown()
        setupSetCityDropDown()
        ChooseGovernmentButton.addTarget(self, action:#selector(showDropDownForgovernment) , for: .touchUpInside)
        ChooseCityButton.addTarget(self, action:#selector(showDropDownForCity) , for: .touchUpInside)
        
        backEnd = ResturantListBackEndManger()
        backEnd.delegate = self
        
        let restCellNib = UINib(nibName: "ResturantListCellTableViewCell", bundle: nil)
        tableView.register(restCellNib, forCellReuseIdentifier: restListCellId)
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !didShow{
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.showCustomDialog()
        })
            didShow = true
        }
    }
    @objc func showDropDownForgovernment() {
        if downloaded{
            setGovernment.show()
        }else{
            dataManger.DownloadGovernmentData()
        }
    }
    @objc func showDropDownForCity() {
       setCity.show()
    }
    func setupSetGovernmentDropDown() {
        setGovernment.anchorView =  ChooseGovernmentButton
      setGovernment.bottomOffset = CGPoint(x: 0, y:  ChooseCityButton.bounds.height)
        setGovernment.selectionAction = { [unowned self] (index, item) in
            self.ChooseGovernmentButton.setTitle(item, for: .normal)
            self.setCity.dataSource.removeAll()
            self.cities.removeAll()
            self.setCity.reloadAllComponents()
            self.ChooseCityButton.showLoader()
            self.dataManger.DownloadCityData(id: String(self.governments[index]._id))
        }
    }
    func setupSetCityDropDown() {
        setCity.anchorView =  ChooseCityButton
        setCity.bottomOffset = CGPoint(x: 0, y: ChooseCityButton.bounds.height)
        setCity.selectionAction = { [unowned self] (index, item) in
            self.ChooseCityButton.setTitle(item, for: .normal)
            self.backEnd.DownloadTables(type:self.tableType!, cityId: String(self.cities[index]._id))
            SwiftLoading().showLoading()
        }
    }
 
    func showCustomDialog(animated: Bool = true) {
        
        // Create a custom view controller
        let ChooseTableTypeNib = ChooseTableTypePopUp(nibName: "ChooseTableTypePopUp", bundle: nil)
        ChooseTableTypeNib.delegate = self
        // Create thec  dialog
        let popup = PopupDialog(viewController: ChooseTableTypeNib, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false)
        
        // Present dialog
        present(popup, animated: animated, completion: nil)
    }
    
}
extension TableListViewController : ChooseTableTypeDelegate{
    func ChooseTableType(type: Bool) {
        if type == false{
            backEnd.DownloadTables(type: 0)
            tableType = 0
            
            
        }else{
            backEnd.DownloadTables(type: 1)
            tableType = 1

        }
        SwiftLoading().showLoading()
        
        
        
        
    }
    
    
}
extension TableListViewController: UITableViewDataSource ,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isFiltering(){
            return filteredData.count
        }else{
            return resturants.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: restListCellId, for: indexPath) as! ResturantListCellTableViewCell
        if isFiltering(){
            cell.SetCell(rest: filteredData[indexPath.row])
        }else{
            cell.SetCell(rest: resturants[indexPath.row])
        }
        cell.heartButton.addTarget(self, action: #selector(favButtonAction), for: .touchUpInside)
        return cell
    }
    
    
    @objc func favButtonAction(sender : Any){
        let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView?.indexPathForRow(at: position)
        {
            favIndex = indexPath
            if isFiltering(){
                backEnd.addOrRemoveFavRequest(id:filteredData[indexPath.row]._id)

            }else{
                backEnd.addOrRemoveFavRequest(id:resturants[indexPath.row]._id)
                
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        if resturants[indexPath.row]._active == false{
        
//        let vc = storyboard?.instantiateViewController(withIdentifier: TableTimePickerViewControllerID) as! TableTimePickerViewController
//        if isFiltering(){
//            vc.paramter["restaurant_id"] = filteredData[indexPath.row]._id
//
//        }else{
//            vc.paramter["restaurant_id"] = resturants[indexPath.row]._id
//
//        }
//        navigationController?.pushViewController(vc, animated: true)
        
        let restDVC = storyboard?.instantiateViewController(withIdentifier: RestTabBarId) as! ResturantDetalsViewController
        if isFiltering(){
            restDVC.resturant = filteredData[indexPath.row]
        }else{
            restDVC.resturant = resturants[indexPath.row]
        }
        navigationController?.pushViewController(restDVC, animated: true)
        
        //        }
    }
    
    
}
extension TableListViewController : RestutantListDelegate{
    func RestutantList(DidFinshDownload Sender: [ResturantModel]) {
        
        resturants = Sender
        if favIndex != nil {
            tableView.reloadRows(at: [favIndex], with: .automatic)
        }else{
            tableView.reloadData()
        }
        SwiftLoading().hideLoading()
        
    }
    
    func RestutantList(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        SwiftLoading().hideLoading()
        
        
    }
    
    func RestutantList(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        SwiftLoading().hideLoading()
        
        
    }
    
    func RestutantList(Success msg: String) {
        SwiftMessages.showMessage(title: "", body: msg, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        resturants.removeAll()
        backEnd.DownloadHomeData()
        SwiftLoading().showLoading()
    }
}
extension TableListViewController : ChooseGovernmentDelegate{
    func GetGovernmentCities(didFinishDownload Sender: [CityModel]?) {
        ChooseCityButton.hideLoader()
        self.setCity.dataSource.removeAll()
        self.cities.removeAll()
        self.setCity.reloadAllComponents()
        self.ChooseCityButton.setTitle(ChooseCityButtonTitle, for: .normal)
        cities = Sender!
        Sender?.forEach({
            setCity.dataSource.append($0._name)
        })
    }
    func GetGovernment(didFinishDownload Sender: [GovernmentModel]?) {
        ChooseGovernmentButton.hideLoader()
        downloaded = true
        governments = Sender!
        Sender?.forEach({
            setGovernment.dataSource.append($0._name)
        })
    }
    func GetGovernment(didFaild error: Error?) {
        ChooseCityButton.hideLoader()
        ChooseGovernmentButton.hideLoader()
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
    }
    func GetGovernment(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body:didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
            }
}
extension TableListViewController : UISearchBarDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        //Show Cancel
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.tintColor = .white
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        //Filter function
        self.filterFunction(searchText: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //Hide Cancel
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
        guard let term = searchBar.text , term.isEmpty == false else {

            //Notification "White spaces are not permitted"
            return
        }
        
        //Filter function
        self.filterFunction(searchText: term)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        //Hide Cancel
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = String()
        searchBar.resignFirstResponder()
   


        
        //Filter function
        self.filterFunction(searchText: searchBar.text!)
    }
    
    func filterFunction(searchText: String){
        filteredData = resturants.filter({( data : ResturantModel)
            -> Bool in
            return data._name.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
        
    }
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }}



