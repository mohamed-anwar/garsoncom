//
//  MyTablesBackEndManger.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/30/18.
//  Copyright © 2018 Rivers. All rights reserved.

import SwiftyJSON
import ObjectMapper
class MyTablesBackEndManger {
    
    private var network : Networking!
    weak var delegate  : MyTablesDelegate?
    
    
    init() {
        network = Networking(requestTimeout: 10)
    }
    
    func RemoveTableRequest(reservation_id : Int) {
        let parameters : [String : Any] = [
            "reservation_id" : reservation_id ,
            "api_token" : userLoginToken!
        ]
        network.manager.request( cancelTableUrl ,method : .post, parameters :  parameters , headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self]response in
            guard let strongSelf = self else{return}
            let result = response.result
            switch result {
            case .success(let value):
                let json = JSON(value)
                if json["status"].string! == StatusMsgs.sucess{
                    strongSelf.delegate?.MyTables(Success: json["message"].string!)
                }else{
                    strongSelf.delegate?.MyTables(didGetUnexpectedResponse: json["message"].string!)
                }
                break
                
            case .failure(let error) :
                strongSelf.delegate?.MyTables(didFaild: error as NSError)
                break
                
            }
        }
    }
    func DownloadData(status : String) {
        network.manager.request( MyTablesUrl , method : .post , parameters : ["api_token" : userLoginToken!,  "status" : status ,], headers : [ acceptLanguage :  setLang() ]).debugLog().responseJSON {[weak self] response in
            var models = [Reservation]()
            let result = response.result
            guard let strongSelf = self else{return}
            switch result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if let contentArray = json["data"]["data"].array {
                    for contentObject in contentArray {
                        let contentDictionary = contentObject.dictionaryObject
                        let model = Reservation()
                        model.mapping(map: Map(mappingType: .fromJSON, JSON: contentDictionary!))
                        models.append(model)
                    }
                    strongSelf.delegate?.MyTables(DidFinshDownload: models)
                } else {
                    strongSelf.delegate?.MyTables(didGetUnexpectedResponse: "The server returned an unexpected error please try again later.")
                }
                break
            case .failure(let error) :
                strongSelf.delegate?.MyTables(didFaild: error as NSError)
                break
                
            }
        }
    }
    
    
    
    
    deinit {
        network.manager.session.invalidateAndCancel()
    }
}
