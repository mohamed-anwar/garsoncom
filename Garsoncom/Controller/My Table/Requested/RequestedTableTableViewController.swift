//
//  RequestedTableTableViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/30/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit
import  SwiftMessages
import AMPopTip

class RequestedTableTableViewController: UITableViewController {
    var reservationTables = [Reservation]()
    var backEnd : MyTablesBackEndManger!
    var segmentedController: UISegmentedControl!
    var refreshTVControl: UIRefreshControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshTVControl = UIRefreshControl()
        refreshTVControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: "Pull to refresh"))
        refreshTVControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.refreshControl = refreshTVControl
        let items = [NSLocalizedString("Requested", comment: ""), NSLocalizedString("Accepted", comment: "")]
        segmentedController = UISegmentedControl(items: items)
        navigationItem.titleView = segmentedController
        backEnd = MyTablesBackEndManger()
        backEnd.delegate = self
        backEnd.DownloadData(status: "new")
        SwiftLoading().showLoading()
        let nibCell = UINib(nibName: "MyTableTableViewCell", bundle: nil)
        tableView.register(nibCell, forCellReuseIdentifier: MyTableTableViewCellID)
        segmentedController.selectedSegmentIndex = 0
        segmentedController.addTarget(self, action: #selector(segmentedControlValueChanged), for:.valueChanged)

    }
    @objc func refresh() {
        backEnd.DownloadData(status: "new")
        SwiftLoading().showLoading()
        
    }
    @objc func segmentedControlValueChanged(segment: UISegmentedControl) {
        if segment.selectedSegmentIndex == 0 {
            backEnd.DownloadData(status: "new")
            SwiftLoading().showLoading()

        }else{
            backEnd.DownloadData(status: "accepted")
            SwiftLoading().showLoading()


        }
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return reservationTables.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyTableTableViewCellID", for: indexPath) as! MyTableTableViewCell
        cell.configureCell(image: reservationTables[indexPath.row]._table._image, numberOfGust: "\(reservationTables[indexPath.row]._table._num_guests)", tableNum: "\(reservationTables[indexPath.row]._table._num)", from: reservationTables[indexPath.row]._from, to: reservationTables[indexPath.row]._to, desc: reservationTables[indexPath.row]._table._description , status :  reservationTables[indexPath.row]._status)
        cell.canselButton.addTarget(self, action: #selector(CancelTable), for: .touchUpInside)
        return cell
    }
    @objc func CancelTable(sender : Any) {
        let position: CGPoint = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        if let indexPath = tableView.indexPathForRow(at: position)
        {
            backEnd.RemoveTableRequest(reservation_id: reservationTables[indexPath.row]._id)
            SwiftLoading().showLoading()

        }
    }




}
extension RequestedTableTableViewController : MyTablesDelegate{
    func MyTables(DidFinshDownload Sender: [Reservation]) {
        reservationTables.removeAll()
        reservationTables = Sender
        tableView.reloadData()
        refreshControl?.endRefreshing()
        SwiftLoading().hideLoading()
        
    }
    
    func MyTables(didFaild error: NSError?) {
        SwiftMessages.showMessage(title: "", body: (error?.localizedDescription)!, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        refreshTVControl.endRefreshing()

        SwiftLoading().hideLoading()

        
    }
    
    func MyTables(didGetUnexpectedResponse: String) {
        SwiftMessages.showMessage(title: "", body: didGetUnexpectedResponse, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        refreshTVControl.endRefreshing()

        SwiftLoading().hideLoading()

        
    }
    
    func MyTables(Success: String) {
        SwiftMessages.showMessage(title: "", body: Success, type: .success, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 3)
        reservationTables.removeAll()
        backEnd.DownloadData(status: "new")
        refreshTVControl.endRefreshing()

        tableView?.reloadData()
        SwiftLoading().showLoading()

        
    }
    
    
}
