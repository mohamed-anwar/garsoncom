//
//  EditDetailMealViewController.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 5/23/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import UIKit

class EditDetailMealViewController: UIViewController {
    var product = ProductModel()
    weak var delegate : EditMealDelegate!
    var _view : EditMeal {
        return view as! EditMeal
    }
    @IBAction func saveChangesButton(_ sender: UIButton) {
        delegate.didEditMeal(Product: product)
        navigationController?.popViewController(animated: true)

    }
    override func viewDidLoad() {
        super.viewDidLoad()

        _view.configureView(cov1: product._cov1, co1 : product._co1 , cov2: product._cov2 , co2 : product._co2 ,  cov3: product._cov3 , co3 : product._co3, cov4: product._cov4 , co4 : product._co4, image: product._image)
    }

    @IBAction func switchSegmented(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            sender.setImage(#imageLiteral(resourceName: "unselectedHumperger"), forSegmentAt: 1)
            sender.setImage(#imageLiteral(resourceName: "unselectedHumperger"), forSegmentAt: 2)
            sender.setImage(#imageLiteral(resourceName: "selectedHumperger"), forSegmentAt: 0)
        } else if sender.selectedSegmentIndex == 1 {
            sender.setImage(#imageLiteral(resourceName: "unselectedHumperger"), forSegmentAt: 0)
            sender.setImage(#imageLiteral(resourceName: "unselectedHumperger"), forSegmentAt: 2)
             sender.setImage(#imageLiteral(resourceName: "selectedHumperger"), forSegmentAt: 1)
        } else if sender.selectedSegmentIndex == 2 {
            sender.setImage(#imageLiteral(resourceName: "unselectedHumperger"), forSegmentAt: 0)
            sender.setImage(#imageLiteral(resourceName: "unselectedHumperger"), forSegmentAt: 1)
             sender.setImage(#imageLiteral(resourceName: "selectedHumperger"), forSegmentAt: 2)
        }

        
        
    }
    @IBAction func firstSegmented(_ sender: UISegmentedControl) {
        product._cov1 = _view.getCov(selected: _view.co1Segmented.selectedSegmentIndex)
    }
    @IBAction func secondSegmented(_ sender: UISegmentedControl) {
        product._cov2 = _view.getCov(selected: _view.co2Segmented.selectedSegmentIndex)
    }
    @IBAction func thirdSegmented(_ sender: UISegmentedControl) {
        product._cov3 = _view.getCov(selected: _view.co3Segmented.selectedSegmentIndex)
       
    }
    @IBAction func fourthSegmented(_ sender: UISegmentedControl) {
        product._cov4 = _view.getCov(selected: _view.co1Segmented.selectedSegmentIndex)
    }
    
}
