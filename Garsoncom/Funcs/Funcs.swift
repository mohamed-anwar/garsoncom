//
//  Funcs.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/10/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import UIKit
import SideMenu
func convertDateformate_24_to12_andReverse (str_date : String , strdateFormat: String, inputFormat: String) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = inputFormat
    dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    
    let date = dateFormatter.date(from: str_date)
    dateFormatter.dateFormat = strdateFormat
    let datestr = dateFormatter.string(from: date!)
    return datestr
}


//func showCustomDialog(animated: Bool = true) {
//    
//    // Create a custom view controller
//    let splashDilaog = AddGroup(nibName: "AddGroup", bundle: nil)
//    
//    // Create thec  dialog
//    let popup = PopupDialog(viewController: splashDilaog, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false)
//    
//    // Present dialog
//    present(popup, animated: animated, completion: nil)
//}

let ArrayofLang = UserDefaults.standard.object(forKey: "AppleLanguages") as! [String]
let favLang = ArrayofLang.first
func setLang()->String{
    if favLang?.lowercased().range(of: "ar") != nil{
        return "ar"
    }else if favLang?.lowercased().range(of: "en") != nil{
        return "en"
    }else{
        return "en"
    }
}

func convertDateObjectToTime(date : Date) -> String {
     let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm:ss"
    return dateFormatter.string(from: date)
}
func convertDateTimeObject(date : Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
    return dateFormatter.string(from: date)
}
func convertDateObjectToDate(date : Date) -> String {
    let dateFormatter = DateFormatter()
//    dateFormatter.dateFormat = "MM-dd-yyyy"
        dateFormatter.dateFormat = "yyyy-MM-dd"

    return dateFormatter.string(from: date)
}
func setSlideMenu(sender : UIViewController){
    let menuLeftNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController
    menuLeftNavigationController.leftSide = true
    SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
    SideMenuManager.default.menuAddPanGestureToPresent(toView: sender.navigationController!.navigationBar)
    SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: sender.navigationController!.view)
    SideMenuManager.default.menuFadeStatusBar = false
    SideMenuManager.default.menuPresentMode = .menuSlideIn
    SideMenuManager.default.menuWidth = UIScreen.main.bounds.width - 70
}
