//
//  SharedPreferences.swift
//  Foreera
//
//  Created by Mohamed on 9/12/17.
//  Copyright © 2017 Foreera. All rights reserved.
//

import UIKit
import RealmSwift
let realm = try! Realm()

var userLoginToken  : String? {
    get{
        if let token =  UserDefaults.standard.string(forKey: loginTokenKey){
            return token
        }else{
            return nil
        }
    }set{
        UserDefaults.standard.set(newValue, forKey: loginTokenKey)
    }
}
var userName  : String? {
    get{
        if let _userName =  UserDefaults.standard.string(forKey: userNameKey){
            return _userName
        }else{
            return nil
        }
    }set{
        UserDefaults.standard.set(newValue, forKey: userNameKey)
    }
}
var userEmail  : String? {
    get{
        if let _userEmail =  UserDefaults.standard.string(forKey: userEmailKey){
            return _userEmail
        }else{
            return nil
        }
    }set{
        UserDefaults.standard.set(newValue, forKey: userEmailKey)
    }
}
var userImage  : String? {
    get{
        if let _userImage =  UserDefaults.standard.string(forKey: userImageKey){
            return _userImage
        }else{
            return nil
        }
    }set{
        UserDefaults.standard.set(newValue, forKey: userImageKey)
    }
}
var cityID  : Int? {
    get{
        return  UserDefaults.standard.integer(forKey: cityIDKey)
    }set{
        UserDefaults.standard.set(newValue, forKey: cityIDKey)
    }
}
var lng  : Double? {
    get{
        return   UserDefaults.standard.double(forKey: lngKey)
    }set{
        UserDefaults.standard.set(newValue, forKey: lngKey)
    }
}
var lat  : Double? {
    get{
        return UserDefaults.standard.double(forKey: latKey)
        }set{
            UserDefaults.standard.set(newValue, forKey: latKey)
        }
}

var userPhone  : String? {
    get{
        if let _userPhone =  UserDefaults.standard.string(forKey: userPhoneKey){
            return _userPhone
        }else{
            return nil
        }
    }set{
        UserDefaults.standard.set(newValue, forKey: userPhoneKey)
    }
}
var userAddress  : String? {
    get{
        if let _userAddress =  UserDefaults.standard.string(forKey: userAddressKey){
            return _userAddress
        }else{
            return nil
        }
    }set{
        UserDefaults.standard.set(newValue, forKey: userAddressKey)
    }
}



