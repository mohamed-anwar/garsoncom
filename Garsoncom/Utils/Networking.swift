//
//  Networking.swift
//  HomeCare
//
//  Created by Mahmoud Fathy on 7/28/17.
//  Copyright © 2017 Mahmoud Fathy. All rights reserved.
//

import UIKit
import Alamofire

class Networking {
    
    var manager: SessionManager!
    init(requestTimeout : Double) {
        manager = self.getAlamofireManager(timeout: requestTimeout)
    }
    
    func getAlamofireManager(timeout : Double) -> SessionManager  {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = timeout
        configuration.timeoutIntervalForRequest = timeout
        let alamofireManager = Alamofire.SessionManager(configuration: configuration)
        return alamofireManager
    }

}
