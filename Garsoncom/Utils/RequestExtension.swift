//
//  RequestExtension.swift
//  Garsoncom
//
//  Created by Mohamed anwar on 4/22/18.
//  Copyright © 2018 Rivers. All rights reserved.
//

import Foundation
import Alamofire

extension Request {
    public func debugLog() -> Self {
        #if DEBUG
        debugPrint("=======================================")
        debugPrint(self)
        debugPrint("=======================================")
        #endif
        return self
    }
}
