//
//  SwiftMessages.swift
//  Foreera
//
//  Created by Mohamed on 9/8/17.
//  Copyright © 2017 Foreera. All rights reserved.
//

import UIKit
import SwiftMessages
extension SwiftMessages{
    public static func showMessage(title: String, body : String, type : Theme,layout : MessageView.Layout, buttonText : String,
                                   buttonTapHandler : ((UIButton) -> Swift.Void)? = nil,
                                   viewTapHandler : ((BaseView) -> Swift.Void)? = nil,
                                   presentationStyle : PresentationStyle,
                                   duration : TimeInterval) {
        let view = MessageView.viewFromNib(layout: layout)
        view.configureTheme(type)
        view.configureContent(title: title, body: body)
        buttonText == "" ? view.button?.isHidden = true : view.button?.setTitle(buttonText, for: .normal)
        view.buttonTapHandler = buttonTapHandler
        view.tapHandler = viewTapHandler
        view.configureDropShadow()
        var viewConfig = SwiftMessages.defaultConfig
        viewConfig.presentationStyle = presentationStyle
        viewConfig.duration = .seconds(seconds: duration)
        SwiftMessages.show(config: viewConfig, view: view)
        
    }
}
